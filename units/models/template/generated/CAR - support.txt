attack_helicopter_brigade.0 = {
	gunship_armament = 0
	gunship_fuselage = 0
	gunship_engines = 0
	gunship_reliability = 0
}
attack_helicopter_brigade.1 = {
	gunship_armament = 1
	gunship_fuselage = 1
	gunship_engines = 1
	gunship_reliability = 1
}
attack_helicopter_brigade.2 = {
	gunship_armament = 2
	gunship_fuselage = 2
	gunship_engines = 2
	gunship_reliability = 2
}
attack_helicopter_brigade.3 = {
	gunship_armament = 4
	gunship_fuselage = 4
	gunship_engines = 4
	gunship_reliability = 4
}
attack_helicopter_brigade.4 = {
	gunship_armament = 6
	gunship_fuselage = 6
	gunship_engines = 6
	gunship_reliability = 6
}
attack_helicopter_brigade.5 = {
	gunship_armament = 8
	gunship_fuselage = 8
	gunship_engines = 8
	gunship_reliability = 8
}
attack_helicopter_brigade.6 = {
	gunship_armament = 10
	gunship_fuselage = 10
	gunship_engines = 10
	gunship_reliability = 10
}
attack_helicopter_brigade.7 = {
	gunship_armament = 12
	gunship_fuselage = 12
	gunship_engines = 12
	gunship_reliability = 12
}
artillery_brigade.0 = {
	artillery_gun = 0
	artillery_carriage = 0
	artillery_increase = 2
}
artillery_brigade.1 = {
	artillery_gun = 1
	artillery_carriage = 1
	artillery_increase = 2
}
artillery_brigade.2 = {
	artillery_gun = 2
	artillery_carriage = 2
	artillery_increase = 2
}
artillery_brigade.3 = {
	artillery_gun = 4
	artillery_carriage = 4
	artillery_increase = 2
}
artillery_brigade.4 = {
	artillery_gun = 6
	artillery_carriage = 6
	artillery_increase = 2
}
artillery_brigade.5 = {
	artillery_gun = 8
	artillery_carriage = 8
	artillery_increase = 2
}
artillery_brigade.6 = {
	artillery_gun = 10
	artillery_carriage = 10
	artillery_increase = 2
}
artillery_brigade.7 = {
	artillery_gun = 12
	artillery_carriage = 12
	artillery_increase = 2
}
pack_artillery_brigade.0 = {
	artillery_gun = 0
	artillery_carriage = 0
	artillery_increase = 2
}
pack_artillery_brigade.1 = {
	artillery_gun = 1
	artillery_carriage = 1
	artillery_increase = 2
}
pack_artillery_brigade.2 = {
	artillery_gun = 2
	artillery_carriage = 2
	artillery_increase = 2
}
pack_artillery_brigade.3 = {
	artillery_gun = 4
	artillery_carriage = 4
	artillery_increase = 2
}
pack_artillery_brigade.4 = {
	artillery_gun = 6
	artillery_carriage = 6
	artillery_increase = 2
}
pack_artillery_brigade.5 = {
	artillery_gun = 8
	artillery_carriage = 8
	artillery_increase = 2
}
pack_artillery_brigade.6 = {
	artillery_gun = 10
	artillery_carriage = 10
	artillery_increase = 2
}
pack_artillery_brigade.7 = {
	artillery_gun = 12
	artillery_carriage = 12
	artillery_increase = 2
}
anti_tank_brigade.0 = {
	anti_tank_ordnance = 0
	anti_tank_carriage = 0
	aa_at_increase = 2
}
anti_tank_brigade.1 = {
	anti_tank_ordnance = 1
	anti_tank_carriage = 1
	aa_at_increase = 2
}
anti_tank_brigade.2 = {
	anti_tank_ordnance = 2
	anti_tank_carriage = 2
	aa_at_increase = 2
}
anti_tank_brigade.3 = {
	anti_tank_ordnance = 4
	anti_tank_carriage = 4
	aa_at_increase = 2
}
anti_tank_brigade.4 = {
	anti_tank_ordnance = 6
	anti_tank_carriage = 6
	aa_at_increase = 2
}
anti_tank_brigade.5 = {
	anti_tank_ordnance = 8
	anti_tank_carriage = 8
	aa_at_increase = 2
}
anti_tank_brigade.6 = {
	anti_tank_ordnance = 10
	anti_tank_carriage = 10
	aa_at_increase = 2
}
anti_tank_brigade.7 = {
	anti_tank_ordnance = 12
	anti_tank_carriage = 12
	aa_at_increase = 2
}
heavy_anti_tank_brigade.0 = {
	anti_tank_ordnance = 0
	anti_tank_carriage = 0
	aa_at_increase = 2
}
heavy_anti_tank_brigade.1 = {
	anti_tank_ordnance = 1
	anti_tank_carriage = 1
	aa_at_increase = 2
}
heavy_anti_tank_brigade.2 = {
	anti_tank_ordnance = 2
	anti_tank_carriage = 2
	aa_at_increase = 2
}
heavy_anti_tank_brigade.3 = {
	anti_tank_ordnance = 4
	anti_tank_carriage = 4
	aa_at_increase = 2
}
heavy_anti_tank_brigade.4 = {
	anti_tank_ordnance = 6
	anti_tank_carriage = 6
	aa_at_increase = 2
}
heavy_anti_tank_brigade.5 = {
	anti_tank_ordnance = 8
	anti_tank_carriage = 8
	aa_at_increase = 2
}
heavy_anti_tank_brigade.6 = {
	anti_tank_ordnance = 10
	anti_tank_carriage = 10
	aa_at_increase = 2
}
heavy_anti_tank_brigade.7 = {
	anti_tank_ordnance = 12
	anti_tank_carriage = 12
	aa_at_increase = 2
}
anti_air_brigade.0 = {
	anti_air_gun = 0
	anti_air_carriage = 0
	aa_at_increase = 2
}
anti_air_brigade.1 = {
	anti_air_gun = 1
	anti_air_carriage = 1
	aa_at_increase = 2
}
anti_air_brigade.2 = {
	anti_air_gun = 2
	anti_air_carriage = 2
	aa_at_increase = 2
}
anti_air_brigade.3 = {
	anti_air_gun = 4
	anti_air_carriage = 4
	aa_at_increase = 4
}
anti_air_brigade.4 = {
	anti_air_gun = 6
	anti_air_carriage = 6
	aa_at_increase = 2
}
anti_air_brigade.5 = {
	anti_air_gun = 8
	anti_air_carriage = 8
	aa_at_increase = 2
}
anti_air_brigade.6 = {
	anti_air_gun = 10
	anti_air_carriage = 10
	aa_at_increase = 2
}
anti_air_brigade.7 = {
	anti_air_gun = 12
	anti_air_carriage = 12
	aa_at_increase = 2
}
sp_artillery_brigade.0 = {
	artillery_gun = 0
	sp_artillery_chassis = 0
	artillery_increase = 2
}
sp_artillery_brigade.1 = {
	artillery_gun = 1
	sp_artillery_chassis = 1
	artillery_increase = 2
}
sp_artillery_brigade.2 = {
	artillery_gun = 2
	sp_artillery_chassis = 2
	artillery_increase = 2
}
sp_artillery_brigade.3 = {
	artillery_gun = 4
	sp_artillery_chassis = 4
	artillery_increase = 2
}
sp_artillery_brigade.4 = {
	artillery_gun = 6
	sp_artillery_chassis = 6
	artillery_increase = 2
}
sp_artillery_brigade.5 = {
	artillery_gun = 8
	sp_artillery_chassis = 8
	artillery_increase = 2
}
sp_artillery_brigade.6 = {
	artillery_gun = 10
	sp_artillery_chassis = 10
	artillery_increase = 2
}
sp_artillery_brigade.7 = {
	artillery_gun = 12
	sp_artillery_chassis = 12
	artillery_increase = 2
}
tank_destroyer_brigade.0 = {
	High_velocity_gun = 0
	td_chassis = 0
	armorsupport_increase = 2
}
tank_destroyer_brigade.1 = {
	High_velocity_gun = 1
	td_chassis = 1
	armorsupport_increase = 2
}
tank_destroyer_brigade.2 = {
	High_velocity_gun = 2
	td_chassis = 2
	armorsupport_increase = 2
}
tank_destroyer_brigade.3 = {
	High_velocity_gun = 4
	td_chassis = 4
	armorsupport_increase = 2
}
tank_destroyer_brigade.4 = {
	High_velocity_gun = 6
	td_chassis = 6
	armorsupport_increase = 2
}
tank_destroyer_brigade.5 = {
	High_velocity_gun = 8
	td_chassis = 8
	armorsupport_increase = 2
}
tank_destroyer_brigade.6 = {
	High_velocity_gun = 10
	td_chassis = 10
	armorsupport_increase = 2
}
tank_destroyer_brigade.7 = {
	High_velocity_gun = 12
	td_chassis = 12
	armorsupport_increase = 2
}
sp_anti_air_brigade.0 = {
	sp_anti_air_armament = 0
	sp_anti_air_chassis = 0
	aa_at_increase = 2
}
sp_anti_air_brigade.1 = {
	sp_anti_air_armament = 1
	sp_anti_air_chassis = 1
	aa_at_increase = 2
}
sp_anti_air_brigade.2 = {
	sp_anti_air_armament = 2
	sp_anti_air_chassis = 2
	aa_at_increase = 2
}
sp_anti_air_brigade.3 = {
	sp_anti_air_armament = 4
	sp_anti_air_chassis = 4
	aa_at_increase = 2
}
sp_anti_air_brigade.4 = {
	sp_anti_air_armament = 6
	sp_anti_air_chassis = 6
	aa_at_increase = 2
}
sp_anti_air_brigade.5 = {
	sp_anti_air_armament = 8
	sp_anti_air_chassis = 8
	aa_at_increase = 2
}
sp_anti_air_brigade.6 = {
	sp_anti_air_armament = 10
	sp_anti_air_chassis = 10
	aa_at_increase = 2
}
sp_anti_air_brigade.7 = {
	sp_anti_air_armament = 12
	sp_anti_air_chassis = 12
	aa_at_increase = 2
}