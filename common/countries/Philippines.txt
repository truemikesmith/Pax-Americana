color = { 144  143  181 }
graphical_culture = Japanese
last_election = 2001.5.14
duration = 36

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"11th Philipine Infantry Div." "21st Philipine Infantry Div." "31st Philipine Infantry Div." "41st Philipine Infantry Div." "51st Philipine Infantry Div." "61st Philipine Infantry Div." "71st Philipine Infantry Div." "81st Philipine Infantry Div." "91st Philipine Infantry Div." "101st Philipine Infantry Div." "111th Philipine Infantry Div." 
		"121st Philipine Infantry Div." "131st Philipine Infantry Div." "141st Philipine Infantry Div." "151st Philipine Infantry Div." "161st Philipine Infantry Div." "171st Philipine Infantry Div." "181st Philipine Infantry Div." "191st Philipine Infantry Div." "201st Philipine Infantry Div." 
	}
	cavalry_brigade = {
		"1st Philipine Cavalry Div." "2nd Philipine Cavalry Div." "3rd Philipine Cavalry Div." "4th Philipine Cavalry Div." "5th Philipine Cavalry Div." "6th Philipine Cavalry Div." "7th Philipine Cavalry Div." "8th Philipine Cavalry Div." "9th Philipine Cavalry Div." "10th Philipine Cavalry Div." 
	}
	motorized_brigade = {
		"1st Philipine Motorized Div." "2nd Philipine Motorized Div." "3rd Philipine Motorized Div." "4th Philipine Motorized Div." "5th Philipine Motorized Div." "6th Philipine Motorized Div." "7th Philipine Motorized Div." "8th Philipine Motorized Div." "9th Philipine Motorized Div." "10th Philipine Motorized Div." 
	}
	mechanized_brigade = {
		"1st Philipine Mechanized Div." "2nd Philipine Mechanized Div." "3rd Philipine Mechanized Div." "4th Philipine Mechanized Div." "5th Philipine Mechanized Div." "6th Philipine Mechanized Div." "7th Philipine Mechanized Div." "8th Philipine Mechanized Div." "9th Philipine Mechanized Div." "10th Philipine Mechanized Div." 
	}
	light_armor_brigade = {
		"1st Philipine Armoured Div." "2nd Philipine Armoured Div." "3rd Philipine Armoured Div." "4th Philipine Armoured Div." "5th Philipine Armoured Div." "6th Philipine Armoured Div." "7th Philipine Armoured Div." "8th Philipine Armoured Div." "9th Philipine Armoured Div." "10th Philipine Armoured Div." "11th Philipine Armoured Div." 
		"12th Philipine Armoured Div." "14th Philipine Armoured Div." "15th Philipine Armoured Div." 
	}
	armor_brigade = {
		"1st Philipine Armoured Div." "2nd Philipine Armoured Div." "3rd Philipine Armoured Div." "4th Philipine Armoured Div." "5th Philipine Armoured Div." "6th Philipine Armoured Div." "7th Philipine Armoured Div." "8th Philipine Armoured Div." "9th Philipine Armoured Div." "10th Philipine Armoured Div." "11th Philipine Armoured Div." 
		"12th Philipine Armoured Div." "14th Philipine Armoured Div." "15th Philipine Armoured Div." 
	}
	paratrooper_brigade = {
		"1st Philipine Paracommando 'Falcons'" "2nd Philipine Paracommando Div." "3rd Philipine Paracommando Div." "4th Philipine Paracommando Div." "5th Philipine Paracommando Div." "6th Philipine Paracommando Div." "7th Philipine Paracommando Div." "8th Philipine Paracommando Div." "9th Philipine Paracommando Div." "10th Philipine Paracommando Div." 
	}
	marine_brigade = {
		"1st Philipine Marinecommando Div." "2nd Philipine Marinecommando Div." "3rd Philipine Marinecommando Div." "4th Philipine Marinecommando Div." "5th Philipine Marinecommando Div." "6th Philipine Marinecommando Div." "7th Philipine Marinecommando Div." "8th Philipine Marinecommando Div." "9th Philipine Marinecommando Div." "10th Philipine Marinecommando Div." 
	}
	bergsjaeger_brigade = {
		"1st Commando Rangers Div." "2nd Commando Rangers Div." "3rd Commando Rangers Div." "4th Commando Rangers Div." "5th Commando Rangers Div." "8th Commando Rangers Div." "10th Commando Rangers Div." "11th Commando Rangers Div." "12th Commando Rangers Div." 
	}
	garrison_brigade = {
		"11th Philipine Infantry Div." "21st Philipine Infantry Div." "31st Philipine Infantry Div." "41st Philipine Infantry Div." "51st Philipine Infantry Div." "61st Philipine Infantry Div." "71st Philipine Infantry Div." "81st Philipine Infantry Div." "91st Philipine Infantry Div." "101st Philipine Infantry Div." "111th Philipine Infantry Div." 
		"121st Philipine Infantry Div." "131st Philipine Infantry Div." "141st Philipine Infantry Div." "151st Philipine Infantry Div." "161st Philipine Infantry Div." "171st Philipine Infantry Div." "181st Philipine Infantry Div." "191st Philipine Infantry Div." "201st Philipine Infantry Div." 
	}
	hq_brigade = {
		"1st Phillipine Army" "2nd Phillipine Army" "3rd Phillipine Army" "4th Phillipine Army" "5th Phillipine Army" 
	}
	militia_brigade = {
		"11th Special Services Division" "21st Special Services Division" "31st Special Services Division" "41st Special Services Division" "51st Special Services Division" "61st Special Services Division" "71st Special Services Division" "81st Special Services Division" "91st Special Services Division" "101st Special Services Division" "111th Special Services Division" 
		"121st Special Services Division" "131st Special Services Division" "141st Special Services Division" "151st Special Services Division" "161st Special Services Division" "171st Special Services Division" "181st Special Services Division" "191st Special Services Division" "201st Special Services Division" "12th Special Services Division" 
	}
	multi_role = {
		"1st PAF Fighter Wing" "2nd PAF Fighter Wing" "3rd PAF Fighter Wing" "4th PAF Fighter Wing" "5th PAF Fighter Wing" "6th PAF Fighter Wing" "7th PAF Fighter Wing" "8th PAF Fighter Wing" "9th PAF Fighter Wing" "10th PAF Fighter Wing" 
	}
	interceptor = {
		"1st PAF Fighter Wing" "2nd PAF Fighter Wing" "3rd PAF Fighter Wing" "4th PAF Fighter Wing" "5th PAF Fighter Wing" "6th PAF Fighter Wing" "7th PAF Fighter Wing" "8th PAF Fighter Wing" "9th PAF Fighter Wing" "10th PAF Fighter Wing" 
	}
	strategic_bomber = {
		"1st PAF Strategic Wing" "2nd PAF Strategic Wing" "3rd PAF Strategic Wing" "4th PAF Strategic Wing" "5th PAF Strategic Wing" "6th PAF Strategic Wing" "7th PAF Strategic Wing" "8th PAF Strategic Wing" "9th PAF Strategic Wing" "10th PAF Strategic Wing" 
	}
	tactical_bomber = {
		"1st PAF Tactical Wing" "2nd PAF Tactical Wing" "3rd PAF Tactical Wing" "4th PAF Tactical Wing" "5th PAF Tactical Wing" "6th PAF Tactical Wing" "7th PAF Tactical Wing" "8th PAF Tactical Wing" "9th PAF Tactical Wing" "10th PAF Tactical Wing" 
	}
	naval_bomber = {
		"1st PAF Navy Bomber Wing" "2nd PAF Navy Bomber Wing" "3rd PAF Navy Bomber Wing" "4th PAF Navy Bomber Wing" "5th PAF Navy Bomber Wing" "6th PAF Navy Bomber Wing" "7th PAF Navy Bomber Wing" "8th PAF Navy Bomber Wing" "9th PAF Navy Bomber Wing" "10th PAF Navy Bomber Wing" "1st PAF Torpedo Wing" 
		"2nd PAF Torpedo Wing" "3rd PAF Torpedo Wing" "4th PAF Torpedo Wing" "5th PAF Torpedo Wing" "6th PAF Torpedo Wing" "7th PAF Torpedo Wing" "8th PAF Torpedo Wing" "9th PAF Torpedo Wing" "10th PAF Torpedo Wing" 
	}
	cas = {
		"1st PAF Dive Bomb Wing" "2nd PAF Dive Bomb Wing" "3rd PAF Dive Bomb Wing" "4th PAF Dive Bomb Wing" "5th PAF Dive Bomb Wing" "6th PAF Dive Bomb Wing" "7th PAF Dive Bomb Wing" "8th PAF Dive Bomb Wing" "9th PAF Dive Bomb Wing" "10th PAF Dive Bomb Wing" 
	}
	transport_plane = {
		"1st PAF Army Transport Wing" "2nd PAF Army Transport Wing" "3rd PAF Army Transport Wing" "4th PAF Army Transport Wing" "5th PAF Army Transport Wing" 
	}
	battleship = {
		"PRNS Manila" "PRNS Luzon" "PRNS Pilipinas Republic" "PRNS General Manchatas" "PRNS Cabanatuan" "PRNS San Pablo" "PRNS Batangas" "PRNS Bacolod" "PRNS Cadiz" "PRNS San Carlos" "PRNS Davao" 
			}
	destroyer = {
		"Flotille Tuguegarao" "Flotille Dagupan" "Flotille San Fernando" "Flotille Tarlac" "Flotille Angeles" "Flotille Olongapo" "Flotille Boac" "Flotille Mindora" "Flotille Legazpi" "Flotille Naga" "Flotille Surigao" 
		"Flotille Butuan" "Flotille Cebu" "Flotille Zamboanga" "Flotille Isabela" 
	}
	submarine = {
		"1st Submarine Flotille" "2nd Submarine Flotille" "3rd Submarine Flotille" "4th Submarine Flotille" "5th Submarine Flotille" "6th Submarine Flotille" "7th Submarine Flotille" "8th Submarine Flotille" "9th Submarine Flotille" "10th Submarine Flotille" "11th Submarine Flotille" 
		"12th Submarine Flotille" "13th Submarine Flotille" "14th Submarine Flotille" "15th Submarine Flotille" 
	}
	transport_ship = {
		"1st Army Transport Flotille" "2nd Army Transport Flotille" "3rd Army Transport Flotille" "4th Army Transport Flotille" "5th Army Transport Flotille" "6th Army Transport Flotille" "7th Army Transport Flotille" "8th Army Transport Flotille" "9th Army Transport Flotille" "10th Army Transport Flotille" "11th Army Transport Flotille" 
		"12th Army Transport Flotille" "13th Army Transport Flotille" "14th Army Transport Flotille" "15th Army Transport Flotille" 
	}

}

ministers = {
	
}
