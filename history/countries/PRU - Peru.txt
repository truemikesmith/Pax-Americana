capital = 10079
government = republic
ideology = social_liberal
alignment = { x = 0 y = 50 }
neutrality = 100
national_unity = 70
oob = "PRU_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 24
	market_liberal = 1
	social_liberal = 37
	social_democrat = 26
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 72
	market_liberal = 3
	social_liberal = 111
	social_democrat = 78
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
service_length = six_months
economic_policy = lais_fair

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
military_police_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
nuclear_reactor_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
special_forces_regiment = 1
special_small_arms = 1
special_anti_tank = 1
special_heavy_weapons = 1
special_support_weapons = 1
cas_doctrine = 1
superior_firepower = 1
green_water_doctrine = 1
submarine_warfare_doctrine = 1

1936.1.1 = { decision = allow_research }