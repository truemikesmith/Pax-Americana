color = { 145  103  129 }
graphical_culture = Generic
last_election = 1999.4.18
duration = 60

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}




ministers = {
	
	78516 = {
		name = "Ikililou Dhoinine"
		ideology = national_socialist
		loyalty = 1.00
		picture = M_COM1
		head_of_state = happy_amateur
		start_date = 2000.1.1
	}
	
}
