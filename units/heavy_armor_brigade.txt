heavy_armor_brigade = {
	type = land
	sprite = Tank
	active = no
	unit_group = armor_unit_type
	is_armor = yes
	is_mobile = yes

	#Size Definitions
	max_strength = 8
	default_organisation = 30
	default_morale = 0.4
	officers = 110

	#Building Costs
	build_cost_ic = 20.00
	build_cost_manpower = 1.5
	build_time = 147

	#Misc Abilities
	maximum_speed = 3.00
	transport_weight = 55.00
	supply_consumption = 1.2
	fuel_consumption = 3.30
	radio_strength = 1
	repair_cost_multiplier = 0.2

	#Defensive Abilities
	defensiveness = 4.25		# was 7
	toughness = 2.85
	softness = 0.05
	air_defence = 5.30
	armor_value = 13

	#Offensive Abilities
	suppression = 0.00
	soft_attack = 5.75
	hard_attack = 3.75
	air_attack = 0.165
	ap_attack = 9

	plains = {
		defence = 0.2
	}
    river = { 
		attack = -0.3
		defence = 0.25 
		movement = -0.45
	}
	amphibious = {
		attack = -2.00 
	}
	urban = {
		attack = 0.05
		defence = 0.05
		movement = -0.25
	}
	arctic = {
		attack = -1
		attrition = 2
	}
	desert = {
		defence = 0.4 
	}
	woods = {
		attack = -0.4 
		defence = -0.3
		movement = -0.6
	}
	forest = {
		attack = -0.60
		defence = -0.5
		movement = -1.0
	}
	jungle = {
		attack = -1.80 
		defence = -1.4
		movement = -1.5
	}
	hills = {
		attack = -0.4 
		defence = -0.3  
		movement = -0.6 
	}
	mountain = {
		attack = -0.60 
		defence = -0.4
		movement = -1.0
	}
   	marsh = {
		attack = -1.80 
		defence = -1.4
		movement = -1.5
	}
	night = {	
	}  		
	fort = {
		attack = 0.95
	}

   
	
	combat_width = 0.2

	completion_size = 2.6
	on_completion = armour_practical
	
	priority = 32

}
