color = { 76  145  63 }
graphical_culture = Generic
last_election = 2002.10.6
duration = 48
major = yes

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"I Divis�o de Infantaria" "II Divis�o de Infantaria" "III Divis�o de Infantaria" "IV Divis�o de Infantaria" "V Divis�o de Infantaria" "VI Divis�o de Infantaria" "VII Divis�o de Infantaria" "VIII Divis�o de Infantaria" "IX Divis�o de Infantaria" "X Divis�o de Infantaria" "XI Divis�o de Infantaria" 
		"XII Divis�o de Infantaria" "XIII Divis�o de Infantaria" "XIV Divis�o de Infantaria" "XV Divis�o de Infantaria" "XVI Divis�o de Infantaria" "XVII Divis�o de Infantaria" "XVIII Divis�o de Infantaria" "XIX Divis�o de Infantaria" "XX Divis�o de Infantaria" "XXI Divis�o de Infantaria" "XXII Divis�o de Infantaria" 
		"XXIII Divis�o de Infantaria" "XXIV Divis�o de Infantaria" "XXV Divis�o de Infantaria" "XXVI Divis�o de Infantaria" "XXVII Divis�o de Infantaria" "XXVIII Divis�o de Infantaria" "XXIX Divis�o de Infantaria" "XXX Divis�o de Infantaria" "XXXI Divis�o de Infantaria" "XXXII Divis�o de Infantaria" "XXXIII Divis�o de Infantaria" 
		"XXXIV Divis�o de Infantaria" "XXXV Divis�o de Infantaria" "XXXVI Divis�o de Infantaria" "XXXVII Divis�o de Infantaria" "XXXVIII Divis�o de Infantaria" "XXXIX Divis�o de Infantaria" "XL Divis�o de Infantaria" "XLI Divis�o de Infantaria" "XLII Divis�o de Infantaria" "XLIII Divis�o de Infantaria" "XLIV Divis�o de Infantaria" 
		"XLV Divis�o de Infantaria" "XLVI Divis�o de Infantaria" "XLVII Divis�o de Infantaria" "XLVIII Divis�o de Infantaria" "XLIX Divis�o de Infantaria" "L Divis�o de Infantaria" "LI Divis�o de Infantaria" "LII Divis�o de Infantaria" "LIII Divis�o de Infantaria" "LIV Divis�o de Infantaria" "LV Divis�o de Infantaria" 
		"LVI Divis�o de Infantaria" "LVII Divis�o de Infantaria" "LVIII Divis�o de Infantaria" "LVIX Divis�o de Infantaria" "LX Divis�o de Infantaria" "LXI Divis�o de Infantaria" "LXII Divis�o de Infantaria" "LXIII Divis�o de Infantaria" "LXIV Divis�o de Infantaria" "LXV Divis�o de Infantaria" "LXVI Divis�o de Infantaria" 
		"LXVII Divis�o de Infantaria" "LXVIII Divis�o de Infantaria" "LXIX Divis�o de Infantaria" "LXX Divis�o de Infantaria" "LXXI Divis�o de Infantaria" "LXXII Divis�o de Infantaria" "LXXIII Divis�o de Infantaria" "LXXIV Divis�o de Infantaria" "LXXV Divis�o de Infantaria" "LXXVI Divis�o de Infantaria" "LXXVII Divis�o de Infantaria" 
		"LXXVIII Divis�o de Infantaria" "LXXIX Divis�o de Infantaria" "LXXX Divis�o de Infantaria" "LXXXI Divis�o de Infantaria" "LXXXII Divis�o de Infantaria" "LXXXIII Divis�o de Infantaria" "LXXXIV Divis�o de Infantaria" "LXXXV Divis�o de Infantaria" "LXXXVI Divis�o de Infantaria" "LXXXVII Divis�o de Infantaria" "LXXXVIII Divis�o de Infantaria" 
		"LXXXIX Divis�o de Infantaria" "XC Divis�o de Infantaria" "XCI Divis�o de Infantaria" 
	}
	cavalry_brigade = {
		"XCII Divis�o de Cavalaria" "XCIII Divis�o de Cavalaria" "XCIV Divis�o de Cavalaria" "XCV Divis�o de Cavalaria" "XCVI Divis�o de Cavalaria" "XCVII Divis�o de Cavalaria" "XCVIII Divis�o de Cavalaria" "XCIX Divis�o de Cavalaria" "C Divis�o de Cavalaria" "CI Divis�o de Cavalaria" "CII Divis�o de Cavalaria" 
		"CIII Divis�o de Cavalaria" "CIV Divis�o de Cavalaria" "CV Divis�o de Cavalaria" "CVI Divis�o de Cavalaria" "CVII Divis�o de Cavalaria" "CVIII Divis�o de Cavalaria" "CIX Divis�o de Cavalaria" "CX Divis�o de Cavalaria" "CXI Divis�o de Cavalaria" 
	}
	motorized_brigade = {
		"CXII Divis�o Motorizada" "CXIII Divis�o Motorizada" "CXIV Divis�o Motorizada" "CXV Divis�o Motorizada" "CXVI Divis�o Motorizada" "CXVII Divis�o Motorizada" "CXVIII Divis�o Motorizada" "CXIX Divis�o Motorizada" "CXX Divis�o Motorizada" "CXXI Divis�o Motorizada" "CXXII Divis�o Motorizada" 
		"CXXIII Divis�o Motorizada" "CXXIV Divis�o Motorizada" "CXXV Divis�o Motorizada" "CXXVI Divis�o Motorizada" "CXXVII Divis�o Motorizada" "CXXVIII Divis�o Motorizada" "CXXIX Divis�o Motorizada" "CXXX Divis�o Motorizada" "CXXXI Divis�o Motorizada" "CXXXII Divis�o Motorizada" "CXXXIII Divis�o Motorizada" 
		"CXXXIV Divis�o Motorizada" "CXXXV Divis�o Motorizada" "CXXXVI Divis�o Motorizada" "CXXXVII Divis�o Motorizada" "CXXXIX Divis�o Motorizada" "CXL Divis�o Motorizada" "CXLI Divis�o Motorizada" "CXLII Divis�o Motorizada" "CXLIII Divis�o Motorizada" "CXLIV Divis�o Motorizada" "CXLV Divis�o Motorizada" 
		"CXLVI Divis�o Motorizada" "CXLVII Divis�o Motorizada" "CXLVIII Divis�o Motorizada" "CXLIX Divis�o Motorizada" "CL Divis�o Motorizada" "CLI Divis�o Motorizada" "CLII Divis�o Motorizada" "CLIII Divis�o Motorizada" 
	}
	mechanized_brigade = {
		"CLIV Infantaria Mecanizada" "CLV Infantaria Mecanizada" "CLVI Infantaria Mecanizada" "CLVII Infantaria Mecanizada" "CLVIII Infantaria Mecanizada" "CLIX Infantaria Mecanizada" "CLX Infantaria Mecanizada" "CLXI Infantaria Mecanizada" "CLXII Infantaria Mecanizada" "CLXIII Infantaria Mecanizada" "CLXIV Infantaria Mecanizada" 
		"CLXV Infantaria Mecanizada" "CLXVI Infantaria Mecanizada" "CLXVII Infantaria Mecanizada" "CLXVIII Infantaria Mecanizada" "CLXIX Infantaria Mecanizada" "CLXX Infantaria Mecanizada" "CLXXI Infantaria Mecanizada" "CLXXII Infantaria Mecanizada" "CLXXIII Infantaria Mecanizada" "CLXXIV Infantaria Mecanizada" "CLXXV Infantaria Mecanizada" 
		"CLXXVI Infantaria Mecanizada" "CLXXVII Infantaria Mecanizada" "CLXXVIII Infantaria Mecanizada" "CLXXIX Infantaria Mecanizada" "CLXXX Infantaria Mecanizada" "CLXXXI Infantaria Mecanizada" "CLXXXII Infantaria Mecanizada" "CLXXXIII Infantaria Mecanizada" "CLXXXIV Infantaria Mecanizada" "CLXXXV Infantaria Mecanizada" "CLXXXVI Infantaria Mecanizada" 
		"CLXXXVII Infantaria Mecanizada" "CLXXXIX Infantaria Mecanizada" "CXCI Infantaria Mecanizada" "CXCII Infantaria Mecanizada" "CXCIII Infantaria Mecanizada" "CXCIV Infantaria Mecanizada" "CXCV Infantaria Mecanizada" "CXCVI Infantaria Mecanizada" 
	}
	light_armor_brigade = {
		"CXCVII Divis�o do Tanque" "CXCIX Divis�o do Tanque" "CC Divis�o do Tanque" "CCI Divis�o do Tanque" "CCII Divis�o do Tanque" "CCIII Divis�o do Tanque" "CCIV Divis�o do Tanque" "CCV Divis�o do Tanque" "CCVI Divis�o do Tanque" "CCVII Divis�o do Tanque" "CCVIII Divis�o do Tanque" 
		"CCIX Divis�o do Tanque" "CCX Divis�o do Tanque" "CCXI Divis�o do Tanque" "CCXII Divis�o do Tanque" "CCXIII Divis�o do Tanque" "CCXIV Divis�o do Tanque" "CCXV Divis�o do Tanque" "CCXVI Divis�o do Tanque" "CCXVII Divis�o do Tanque" "CCXVIII Divis�o do Tanque" "CCXIX Divis�o do Tanque" 
		"CCXX Divis�o do Tanque" "CCXXI Divis�o do Tanque" "CCXXII Divis�o do Tanque" 
	}
	armor_brigade = {
		"CXCVII Divis�o do Tanque" "CXCIX Divis�o do Tanque" "CC Divis�o do Tanque" "CCI Divis�o do Tanque" "CCII Divis�o do Tanque" "CCIII Divis�o do Tanque" "CCIV Divis�o do Tanque" "CCV Divis�o do Tanque" "CCVI Divis�o do Tanque" "CCVII Divis�o do Tanque" "CCVIII Divis�o do Tanque" 
		"CCIX Divis�o do Tanque" "CCX Divis�o do Tanque" "CCXI Divis�o do Tanque" "CCXII Divis�o do Tanque" "CCXIII Divis�o do Tanque" "CCXIV Divis�o do Tanque" "CCXV Divis�o do Tanque" "CCXVI Divis�o do Tanque" "CCXVII Divis�o do Tanque" "CCXVIII Divis�o do Tanque" "CCXIX Divis�o do Tanque" 
		"CCXX Divis�o do Tanque" "CCXXI Divis�o do Tanque" "CCXXII Divis�o do Tanque" 
	}
	paratrooper_brigade = {
		"CCXXIII Divis�o de Paraquedistas" "CCXXIV Divis�o de Paraquedistas" "CCXXV Divis�o de Paraquedistas" "CCXXVI Divis�o de Paraquedistas" "CCXXVII Divis�o de Paraquedistas" "CCXXVIII Divis�o de Paraquedistas" "CCXXIX Divis�o de Paraquedistas" "CCXXX Divis�o de Paraquedistas" "CCXXXI Divis�o de Paraquedistas" "CCXXXII Divis�o de Paraquedistas" "CCXXXIII Divis�o de Paraquedistas" 
		"CCXXXIV Divis�o de Paraquedistas" "CCXXXV Divis�o de Paraquedistas" "CCXXXVI Divis�o de Paraquedistas" "CCXXXVII Divis�o de Paraquedistas" 
	}
	marine_brigade = {
		"CCXXXVIII Divis�o de Fuzileiros Navais" "CCXXXIX Divis�o de Fuzileiros Navais" "CCXL Divis�o de Fuzileiros Navais" "CCXLI Divis�o de Fuzileiros Navais" "CCXLII Divis�o de Fuzileiros Navais" "CCXLIII Divis�o de Fuzileiros Navais" "CCXLIV Divis�o de Fuzileiros Navais" "CCXLV Divis�o de Fuzileiros Navais" "CCXLVI Divis�o de Fuzileiros Navais" "CCXLVII Divis�o de Fuzileiros Navais" "CCXLVIII Divis�o de Fuzileiros Navais" 
		"CCXLIX Divis�o de Fuzileiros Navais" "CCL Divis�o de Fuzileiros Navais" "CCLI Divis�o de Fuzileiros Navais" "CCLII Divis�o de Fuzileiros Navais" 
	}
	bergsjaeger_brigade = {
		"CCLIII Divis�o de Montanha" "CCLIV Divis�o de Montanha" "CCLV Divis�o de Montanha" "CCLVI Divis�o de Montanha" "CCLVII Divis�o de Montanha" "CCLVIII Divis�o de Montanha" "CCLIX Divis�o de Montanha" "CCLX Divis�o de Montanha" "CCLXI Divis�o de Montanha" "CCLXII Divis�o de Montanha" "CCLXIII Divis�o de Montanha" 
		"CCLXIV Divis�o de Montanha" "CCLXV Divis�o de Montanha" "CCLXVI Divis�o de Montanha" "CCLXVII Divis�o de Montanha" 
	}
	garrison_brigade = {
		"L Divis�o de Infantaria" "LI Divis�o de Infantaria" "LII Divis�o de Infantaria" "LIII Divis�o de Infantaria" "LIV Divis�o de Infantaria" "LV Divis�o de Infantaria" "LVI Divis�o de Infantaria" "LVII Divis�o de Infantaria" "LVIII Divis�o de Infantaria" "LVIX Divis�o de Infantaria" "LX Divis�o de Infantaria" 
		"LXI Divis�o de Infantaria" "LXII Divis�o de Infantaria" "LXIII Divis�o de Infantaria" "LXIV Divis�o de Infantaria" "LXV Divis�o de Infantaria" "LXVI Divis�o de Infantaria" "LXVII Divis�o de Infantaria" "LXVIII Divis�o de Infantaria" "LXIX Divis�o de Infantaria" "LXX Divis�o de Infantaria" "LXXI Divis�o de Infantaria" 
		"LXXII Divis�o de Infantaria" "LXXIII Divis�o de Infantaria" "LXXIV Divis�o de Infantaria" "LXXV Divis�o de Infantaria" "LXXVI Divis�o de Infantaria" "LXXVII Divis�o de Infantaria" "LXXVIII Divis�o de Infantaria" "LXXIX Divis�o de Infantaria" "LXXX Divis�o de Infantaria" "LXXXI Divis�o de Infantaria" "LXXXII Divis�o de Infantaria" 
		"LXXXIII Divis�o de Infantaria" "LXXXIV Divis�o de Infantaria" "LXXXV Divis�o de Infantaria" "LXXXVI Divis�o de Infantaria" "LXXXVII Divis�o de Infantaria" "LXXXVIII Divis�o de Infantaria" "LXXXIX Divis�o de Infantaria" "XC Divis�o de Infantaria" "XCI Divis�o de Infantaria" 
	}
	hq_brigade = {
		"1o Ex�rcito do Brasil" "2o Ex�rcito do Brasil" "3o Ex�rcito do Brasil" "4o Ex�rcito do Brasil" "5o Ex�rcito do Brasil" "6o Ex�rcito do Brasil" "7o Ex�rcito do Brasil" "8o Ex�rcito do Brasil" "9o Ex�rcito do Brasil" "10o Ex�rcito do Brasil" 
	}
	militia_brigade = {
		"CCLXVIII Divis�o do Guarda Civ�l" "CCLXIX Divis�o do Guarda Civ�l" "CCLXX Divis�o do Guarda Civ�l" "CCLXXI Divis�o do Guarda Civ�l" "CCLXXII Divis�o do Guarda Civ�l" "CCLXXIII Divis�o do Guarda Civ�l" "CCLXXIV Divis�o do Guarda Civ�l" "CCLXXV Divis�o do Guarda Civ�l" "CCLXXVI Divis�o do Guarda Civ�l" "CCLXXVII Divis�o do Guarda Civ�l" "CCLXXIX Divis�o do Guarda Civ�l" 
		"CCLXXX Divis�o do Guarda Civ�l" "CCLXXXI Divis�o do Guarda Civ�l" "CCLXXXII Divis�o do Guarda Civ�l" "CCLXXXIII Divis�o do Guarda Civ�l" "CCLXXXIV Divis�o do Guarda Civ�l" "CCLXXXV Divis�o do Guarda Civ�l" "CCLXXXVI Divis�o do Guarda Civ�l" "CCLXXXVII Divis�o do Guarda Civ�l" "CCLXXXVIII Divis�o do Guarda Civ�l" "CCLXXXIX Divis�o do Guarda Civ�l" "CCXC Divis�o do Guarda Civ�l" 
		"CCXCI Divis�o do Guarda Civ�l" "CCXCII Divis�o do Guarda Civ�l" "CCXCIII Divis�o do Guarda Civ�l" "CCXCIV Divis�o do Guarda Civ�l" "CCXCV Divis�o do Guarda Civ�l" "CCXCVI Divis�o do Guarda Civ�l" "CCXCVII Divis�o do Guarda Civ�l" "CCXCVIII Divis�o do Guarda Civ�l" "CCXCIX Divis�o do Guarda Civ�l" "CCC Divis�o do Guarda Civ�l" 
	}
	multi_role = {
		"1. Grupo de Ca�as" "2. Grupo de Ca�as" "3. Grupo de Ca�as" "4. Grupo de Ca�as" "5. Grupo de Ca�as" "6. Grupo de Ca�as" "7. Grupo de Ca�as" "8. Grupo de Ca�as" "9. Grupo de Ca�as" "10. Grupo de Ca�as" "11. Grupo de Ca�as" 
		"12. Grupo de Ca�as" "13. Grupo de Ca�as" "14. Grupo de Ca�as" "15. Grupo de Ca�as" "16. Grupo de Ca�as" "17. Grupo de Ca�as" "18. Grupo de Ca�as" "19. Grupo de Ca�as" "20. Grupo de Ca�as" 
	}
	interceptor = {
		"1. Grupo de Ca�as" "2. Grupo de Ca�as" "3. Grupo de Ca�as" "4. Grupo de Ca�as" "5. Grupo de Ca�as" "6. Grupo de Ca�as" "7. Grupo de Ca�as" "8. Grupo de Ca�as" "9. Grupo de Ca�as" "10. Grupo de Ca�as" "11. Grupo de Ca�as" 
		"12. Grupo de Ca�as" "13. Grupo de Ca�as" "14. Grupo de Ca�as" "15. Grupo de Ca�as" "16. Grupo de Ca�as" "17. Grupo de Ca�as" "18. Grupo de Ca�as" "19. Grupo de Ca�as" "20. Grupo de Ca�as" 
	}
	strategic_bomber = {
		"1. Grupo de Bombardeiros" "2. Grupo de Bombardeiros" "3. Grupo de Bombardeiros" "4. Grupo de Bombardeiros" "5. Grupo de Bombardeiros" "6. Grupo de Bombardeiros" "7. Grupo de Bombardeiros" "8. Grupo de Bombardeiros" "9. Grupo de Bombardeiros" "10. Grupo de Bombardeiros" "11. Grupo de Bombardeiros" 
		"12. Grupo de Bombardeiros" "13. Grupo de Bombardeiros" "14. Grupo de Bombardeiros" "15. Grupo de Bombardeiros" "16. Grupo de Bombardeiros" "17. Grupo de Bombardeiros" "18. Grupo de Bombardeiros" "19. Grupo de Bombardeiros" "20. Grupo de Bombardeiros" 
	}
	tactical_bomber = {
		"21. Grupo de Bombardeiros" "22. Grupo de Bombardeiros" "23. Grupo de Bombardeiros" "24. Grupo de Bombardeiros" "25. Grupo de Bombardeiros" "26. Grupo de Bombardeiros" "27. Grupo de Bombardeiros" "28. Grupo de Bombardeiros" "29. Grupo de Bombardeiros" "30. Grupo de Bombardeiros" "31. Grupo de Bombardeiros" 
		"32. Grupo de Bombardeiros" "33. Grupo de Bombardeiros" "34. Grupo de Bombardeiros" "35. Grupo de Bombardeiros" "36. Grupo de Bombardeiros" "37. Grupo de Bombardeiros" "38. Grupo de Bombardeiros" "39. Grupo de Bombardeiros" "40. Grupo de Bombardeiros" 
	}
	naval_bomber = {
		"1. Grupo de Bomb. do Navais" "2. Grupo de Bomb. do Navais" "3. Grupo de Bomb. do Navais" "4. Grupo de Bomb. do Navais" "5. Grupo de Bomb. do Navais" "6. Grupo de Bomb. do Navais" "7. Grupo de Bomb. do Navais" "8. Grupo de Bomb. do Navais" "9. Grupo de Bomb. do Navais" "10. Grupo de Bomb. do Navais" "11. Grupo de Bomb. do Navais" 
		"12. Grupo de Bomb. do Navais" "13. Grupo de Bomb. do Navais" "14. Grupo de Bomb. do Navais" "15. Grupo de Bomb. do Navais" "16. Grupo de Bomb. do Navais" "17. Grupo de Bomb. do Navais" "18. Grupo de Bomb. do Navais" "19. Grupo de Bomb. do Navais" "20. Grupo de Bomb. do Navais" "1. Grupo de Avi�es Torpedeiros" "2. Grupo de Avi�es Torpedeiros" 
		"3. Grupo de Avi�es Torpedeiros" "4. Grupo de Avi�es Torpedeiros" "5. Grupo de Avi�es Torpedeiros" "6. Grupo de Avi�es Torpedeiros" "7. Grupo de Avi�es Torpedeiros" "8. Grupo de Avi�es Torpedeiros" "9. Grupo de Avi�es Torpedeiros" "10. Grupo de Avi�es Torpedeiros" "11. Grupo de Avi�es Torpedeiros" "12. Grupo de Avi�es Torpedeiros" "13. Grupo de Avi�es Torpedeiros" 
		"14. Grupo de Avi�es Torpedeiros" "15. Grupo de Avi�es Torpedeiros" 
	}
	cas = {
		"1. Grupo de Bomb. de Mergulho" "2. Grupo de Bomb. de Mergulho" "3. Grupo de Bomb. de Mergulho" "4. Grupo de Bomb. de Mergulho" "5. Grupo de Bomb. de Mergulho" "6. Grupo de Bomb. de Mergulho" "7. Grupo de Bomb. de Mergulho" "8. Grupo de Bomb. de Mergulho" "9. Grupo de Bomb. de Mergulho" "10. Grupo de Bomb. de Mergulho" "11. Grupo de Bomb. de Mergulho" 
		"12. Grupo de Bomb. de Mergulho" "13. Grupo de Bomb. de Mergulho" "14. Grupo de Bomb. de Mergulho" "15. Grupo de Bomb. de Mergulho" 
	}
	transport_plane = {
		"1. Grupo de Avi�es de Transporte" "2. Grupo de Avi�es de Transporte" "3. Grupo de Avi�es de Transporte" "4. Grupo de Avi�es de Transporte" "5. Grupo de Avi�es de Transporte" "6. Grupo de Avi�es de Transporte" "7. Grupo de Avi�es de Transporte" "8. Grupo de Avi�es de Transporte" "9. Grupo de Avi�es de Transporte" "10. Grupo de Avi�es de Transporte" 
	}
	battleship = {
		"NRB Minas Gerais" "NRB S�o Paulo" "NRB Santa Catarina" "NRB Espirito Santo" "NRB Rio de Janeiro" "NRB Maranh�o" "NRB Goi�s" "NRB Rio Grande do Sul" "NRB Par�" "NRB Alagoas" "NRB Pernambuco" 
		"NRB Roraima" "NRB Tocantins" "NRB Santa Catarina" "NRB Rio Grande do Norte" "NRB Paran�" "NRB Acre" "NRB Mato Grosso" "NRB Amazona" "NRB Rondonia" "NRB Amap�" "NRB Piaui" 
		"NRB Cear�" "NRB Sergipe" "NRB Para�ba" "NRB Mato Grosso do Sul" 
	}
	light_cruiser = {
		"NRB Rio Grande do Norte" "NRB Paran�" "NRB Acre" "NRB Mato Grosso" "NRB Amazona" "NRB Rondonia" "NRB Amap�" "NRB Piaui" "NRB Cear�" "NRB Sergipe" "NRB Para�ba" 
		"NRB Mato Grosso do Sul" 
	}
	heavy_cruiser = {
		"NRB Belmonte" "NRB Bahia" "NRB Santa Catarina" "NRB Espirito Santo" "NRB Rio de Janeiro" "NRB Maranh�o" "NRB Goi�s" "NRB Rio Grande do Sul" "NRB Par�" "NRB Alagoas" "NRB Pernambuco" 
		"NRB Roraima" "NRB Tocantins" "NRB Santa Catarina" "NRB Rio Grande do Norte" "NRB Paran�" "NRB Acre" "NRB Mato Grosso" "NRB Amazona" "NRB Rondonia" "NRB Amap�" "NRB Piaui" 
		"NRB Cear�" "NRB Sergipe" "NRB Para�ba" "NRB Mato Grosso do Sul" 
	}
	battlecruiser = {
		"NRB Minas Gerais" "NRB S�o Paulo" "NRB Santa Catarina" "NRB Espirito Santo" "NRB Rio de Janeiro" "NRB Maranh�o" "NRB Goi�s" "NRB Rio Grande do Sul" "NRB Par�" "NRB Alagoas" "NRB Pernambuco" 
		"NRB Roraima" "NRB Tocantins" "NRB Santa Catarina" 
	}
	destroyer = {
		"1. Flotilla de Contratorpedeiros" "2. Flotilla de Contratorpedeiros" "3. Flotilla de Contratorpedeiros" "4. Flotilla de Contratorpedeiros" "5. Flotilla de Contratorpedeiros" "6. Flotilla de Contratorpedeiros" "7. Flotilla de Contratorpedeiros" "8. Flotilla de Contratorpedeiros" "9. Flotilla de Contratorpedeiros" "10. Flotilla de Contratorpedeiros" "11. Flotilla de Contratorpedeiros" 
		"12. Flotilla de Contratorpedeiros" "13. Flotilla de Contratorpedeiros" "14. Flotilla de Contratorpedeiros" "15. Flotilla de Contratorpedeiros" "16. Flotilla de Contratorpedeiros" "17. Flotilla de Contratorpedeiros" "18. Flotilla de Contratorpedeiros" "19. Flotilla de Contratorpedeiros" "20. Flotilla de Contratorpedeiros" 
	}
	carrier = {
		"NRB Santa Catarina" "NRB Espirito Santo" "NRB Rio de Janeiro" "NRB Maranh�o" "NRB Goi�s" "NRB Rio Grande do Sul" "NRB Par�" "NRB Alagoas" "NRB Pernambuco" "NRB Roraima" "NRB Tocantins" 
		"NRB Santa Catarina" 
	}
	escort_carrier = {
		"NRB Minas Gerais" "NRB S�o Paulo" "NRB Santa Catarina" "NRB Espirito Santo" "NRB Rio de Janeiro" "NRB Maranh�o" "NRB Goi�s" "NRB Rio Grande do Sul" "NRB Par�" "NRB Alagoas" "NRB Pernambuco" 
		"NRB Roraima" "NRB Tocantins" 
	}
	submarine = {
		"1. Flotilla de Submarinos" "2. Flotilla de Submarinos" "3. Flotilla de Submarinos" "4. Flotilla de Submarinos" "5. Flotilla de Submarinos" "6. Flotilla de Submarinos" "7. Flotilla de Submarinos" "8. Flotilla de Submarinos" "9. Flotilla de Submarinos" "10. Flotilla de Submarinos" "11. Flotilla de Submarinos" 
		"12. Flotilla de Submarinos" "13. Flotilla de Submarinos" "14. Flotilla de Submarinos" "15. Flotilla de Submarinos" "16. Flotilla de Submarinos" "17. Flotilla de Submarinos" "18. Flotilla de Submarinos" "19. Flotilla de Submarinos" "20. Flotilla de Submarinos" 
	}
	transport_ship = {
		"1. Flotilla de Navios de Transporte" "2. Flotilla de Navios de Transporte" "3. Flotilla de Navios de Transporte" "4. Flotilla de Navios de Transporte" "5. Flotilla de Navios de Transporte" "6. Flotilla de Navios de Transporte" "7. Flotilla de Navios de Transporte" "8. Flotilla de Navios de Transporte" "9. Flotilla de Navios de Transporte" "10. Flotilla de Navios de Transporte" "11. Flotilla de Navios de Transporte" 
		"12. Flotilla de Navios de Transporte" "13. Flotilla de Navios de Transporte" "14. Flotilla de Navios de Transporte" "15. Flotilla de Navios de Transporte" "16. Flotilla de Navios de Transporte" "17. Flotilla de Navios de Transporte" "18. Flotilla de Navios de Transporte" "19. Flotilla de Navios de Transporte" "20. Flotilla de Navios de Transporte" 
	}

}


ministers = {
	
}
