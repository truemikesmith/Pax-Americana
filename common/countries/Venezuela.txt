color = { 252  231  178 }
graphical_culture = Generic
last_election = 2000.7.30
duration = 60

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"1a Div. de Inf. 'Eduardo Blanco'" "2a Divisi�n de Infanter�a" "3a Divisi�n de Infanter�a" "4a Divisi�n de Infanter�a" "5a Divisi�n de Infanter�a" "6a Divisi�n de Infanter�a" "7a Divisi�n de Infanter�a" "8a Divisi�n de Infanter�a" "9a Divisi�n de Infanter�a" "10a Divisi�n de Infanter�a" "11a Divisi�n de Infanter�a" 
		"12a Divisi�n de Infanter�a" "13a Divisi�n de Infanter�a" "14a Divisi�n de Infanter�a" "15a Divisi�n de Infanter�a" "16a Divisi�n de Infanter�a" "17a Divisi�n de Infanter�a" "18a Divisi�n de Infanter�a" "19a Divisi�n de Infanter�a" "20a Divisi�n de Infanter�a" 
	}
	cavalry_brigade = {
		"Los Caballos de Venezuela" "2a Divisi�n de Caballer�a" "3a Divisi�n de Caballer�a" "4a Divisi�n de Caballer�a" "5a Divisi�n de Caballer�a" "6a Divisi�n de Caballer�a" "7a Divisi�n de Caballer�a" "8a Divisi�n de Caballer�a" "9a Divisi�n de Caballer�a" "10a Divisi�n de Caballer�a" 
	}
	motorized_brigade = {
		"1a Divisi�n de Inf. en Cami�n" "2a Divisi�n de Inf. en Cami�n" "3a Divisi�n de Inf. en Cami�n" "4a Divisi�n de Inf. en Cami�n" "5a Divisi�n de Inf. en Cami�n" "6a Divisi�n de Inf. en Cami�n" "7a Divisi�n de Inf. en Cami�n" "8a Divisi�n de Inf. en Cami�n" "9a Divisi�n de Inf. en Cami�n" "10a Divisi�n de Inf. en Cami�n" "11a Divisi�n de Inf. en Cami�n" 
		"12a Divisi�n de Inf. en Cami�n" "13a Divisi�n de Inf. en Cami�n" "14a Divisi�n de Inf. en Cami�n" "15a Divisi�n de Inf. en Cami�n" 
	}
	mechanized_brigade = {
		"1a Divisi�n Mecanizada" "2a Divisi�n Mecanizada" "3a Divisi�n Mecanizada" "4a Divisi�n Mecanizada" "5a Divisi�n Mecanizada" "6a Divisi�n Mecanizada" "7a Divisi�n Mecanizada" "8a Divisi�n Mecanizada" "9a Divisi�n Mecanizada" "10a Divisi�n Mecanizada" "11a Divisi�n Mecanizada" 
		"12a Divisi�n Mecanizada" "13a Divisi�n Mecanizada" "14a Divisi�n Mecanizada" "15a Divisi�n Mecanizada" 
	}
	light_armor_brigade = {
		"1a Divisi�n Blindada de Venezuela" "2a Divisi�n Blindada de Venezuela" "3a Divisi�n Blindada de Venezuela" "4a Divisi�n Blindada de Venezuela" "5a Divisi�n Blindada de Venezuela" "6a Divisi�n Blindada de Venezuela" "7a Divisi�n Blindada de Venezuela" "8a Divisi�n Blindada de Venezuela" "9a Divisi�n Blindada de Venezuela" "10a Divisi�n Blindada de Venezuela" "11a Divisi�n Blindada de Venezuela" 
		"12a Divisi�n Blindada de Venezuela" "13a Divisi�n Blindada de Venezuela" "14a Divisi�n Blindada de Venezuela" "15a Divisi�n Blindada de Venezuela" 
	}
	armor_brigade = {
		"1a Divisi�n Blindada de Venezuela" "2a Divisi�n Blindada de Venezuela" "3a Divisi�n Blindada de Venezuela" "4a Divisi�n Blindada de Venezuela" "5a Divisi�n Blindada de Venezuela" "6a Divisi�n Blindada de Venezuela" "7a Divisi�n Blindada de Venezuela" "8a Divisi�n Blindada de Venezuela" "9a Divisi�n Blindada de Venezuela" "10a Divisi�n Blindada de Venezuela" "11a Divisi�n Blindada de Venezuela" 
		"12a Divisi�n Blindada de Venezuela" "13a Divisi�n Blindada de Venezuela" "14a Divisi�n Blindada de Venezuela" "15a Divisi�n Blindada de Venezuela" 
	}
	paratrooper_brigade = {
		"1a Divisi�n Para. 'Los Immortales'" "2a Divisi�n de Paracaidistas" "3a Divisi�n de Paracaidistas" "4a Divisi�n de Paracaidistas" "5a Divisi�n de Paracaidistas" "6a Divisi�n de Paracaidistas" "7a Divisi�n de Paracaidistas" "8a Divisi�n de Paracaidistas" "9a Divisi�n de Paracaidistas" "10a Divisi�n de Paracaidistas" 
	}
	marine_brigade = {
		"1a Divisi�n de Infanter�a de Marina" "2a Divisi�n de Infanter�a de Marina" "3a Divisi�n de Infanter�a de Marina" "4a Divisi�n de Infanter�a de Marina" "5a Divisi�n de Infanter�a de Marina" "6a Divisi�n de Infanter�a de Marina" "7a Divisi�n de Infanter�a de Marina" "8a Divisi�n de Infanter�a de Marina" "9a Divisi�n de Infanter�a de Marina" "10a Divisi�n de Infanter�a de Marina" 
	}
	bergsjaeger_brigade = {
		"1a Divisi�n Alpina 'Los Salinas'" "2a Divisi�n Alpina" "3a Divisi�n Alpina" "4a Divisi�n Alpina" "5a Divisi�n Alpina" "6a Divisi�n Alpina" "7a Divisi�n Alpina" "8a Divisi�n Alpina" "9a Divisi�n Alpina" "10a Divisi�n Alpina" 
	}
	garrison_brigade = {
		"2a Divisi�n de Infanter�a" "3a Divisi�n de Infanter�a" "4a Divisi�n de Infanter�a" "5a Divisi�n de Infanter�a" "6a Divisi�n de Infanter�a" "7a Divisi�n de Infanter�a" "8a Divisi�n de Infanter�a" "9a Divisi�n de Infanter�a" "10a Divisi�n de Infanter�a" "11a Divisi�n de Infanter�a" "12a Divisi�n de Infanter�a" 
		"13a Divisi�n de Infanter�a" "14a Divisi�n de Infanter�a" "15a Divisi�n de Infanter�a" "16a Divisi�n de Infanter�a" "17a Divisi�n de Infanter�a" "18a Divisi�n de Infanter�a" "19a Divisi�n de Infanter�a" "20a Divisi�n de Infanter�a" 
	}
	hq_brigade = {
		"Cuartel General 1er Ej�rcito" "Cuartel General 2o Ej�rcito" "Cuartel General 3er Ej�rcito" "Cuartel General 4o Ej�rcito" "Cuartel General 5o Ej�rcito" "Cuartel General 6o Ej�rcito" "Cuartel General 7o Ej�rcito" "Cuartel General 8o Ej�rcito" "Cuartel General 9o Ej�rcito" "Cuartel General 10o Ej�rcito" 
	}
	militia_brigade = {
		"1a Divisi�n de la Guardia Naci�nal" "2a Divisi�n de la Guardia Naci�nal" "3a Divisi�n de la Guardia Naci�nal" "4a Divisi�n de la Guardia Naci�nal" "5a Divisi�n de la Guardia Naci�nal" "6a Divisi�n de la Guardia Naci�nal" "7a Divisi�n de la Guardia Naci�nal" "8a Divisi�n de la Guardia Naci�nal" "9a Divisi�n de la Guardia Naci�nal" "10a Divisi�n de la Guardia Naci�nal" "11a Divisi�n de la Guardia Naci�nal" 
		"12a Divisi�n de la Guardia Naci�nal" "13a Divisi�n de la Guardia Naci�nal" "14a Divisi�n de la Guardia Naci�nal" "15a Divisi�n de la Guardia Naci�nal" "16a Divisi�n de la Guardia Naci�nal" "17a Divisi�n de la Guardia Naci�nal" "18a Divisi�n de la Guardia Naci�nal" "19a Divisi�n de la Guardia Naci�nal" "20a Divisi�n de la Guardia Naci�nal" 
	}
	multi_role = {
		"1. Grupo A�reo de Caza" "2. Grupo A�reo de Caza" "3. Grupo A�reo de Caza" "4. Grupo A�reo de Caza" "5. Grupo A�reo de Caza" "6. Grupo A�reo de Caza" "7. Grupo A�reo de Caza" "8. Grupo A�reo de Caza" "9. Grupo A�reo de Caza" "10. Grupo A�reo de Caza" 
	}
	interceptor = {
		"1. Grupo A�reo de Caza" "2. Grupo A�reo de Caza" "3. Grupo A�reo de Caza" "4. Grupo A�reo de Caza" "5. Grupo A�reo de Caza" "6. Grupo A�reo de Caza" "7. Grupo A�reo de Caza" "8. Grupo A�reo de Caza" "9. Grupo A�reo de Caza" "10. Grupo A�reo de Caza" 
	}
	strategic_bomber = {
		"1. Grupo A�reo Estrat�gico" "2. Grupo A�reo Estrat�gico" "3. Grupo A�reo Estrat�gico" "4. Grupo A�reo Estrat�gico" "5. Grupo A�reo Estrat�gico" "6. Grupo A�reo Estrat�gico" "7. Grupo A�reo Estrat�gico" "8. Grupo A�reo Estrat�gico" "9. Grupo A�reo Estrat�gico" "10. Grupo A�reo Estrat�gico" 
	}
	tactical_bomber = {
		"1. Grupo A�reo T�ctico" "2. Grupo A�reo T�ctico" "3. Grupo A�reo T�ctico" "4. Grupo A�reo T�ctico" "5. Grupo A�reo T�ctico" "6. Grupo A�reo T�ctico" "7. Grupo A�reo T�ctico" "8. Grupo A�reo T�ctico" "9. Grupo A�reo T�ctico" "10. Grupo A�reo T�ctico" 
	}
	naval_bomber = {
		"1. Grupo A�reo de la Marina" "2. Grupo A�reo de la Marina" "3. Grupo A�reo de la Marina" "4. Grupo A�reo de la Marina" "5. Grupo A�reo de la Marina" "6. Grupo A�reo de la Marina" "7. Grupo A�reo de la Marina" "8. Grupo A�reo de la Marina" "9. Grupo A�reo de la Marina" "10. Grupo A�reo de la Marina" "1. Grupo A�reo Torpedero" 
		"2. Grupo A�reo Torpedero" "3. Grupo A�reo Torpedero" "4. Grupo A�reo Torpedero" "5. Grupo A�reo Torpedero" "6. Grupo A�reo Torpedero" 
	}
	cas = {
		"1. Grupo A�reo de Asalto" "2. Grupo A�reo de Asalto" "3. Grupo A�reo de Asalto" "4. Grupo A�reo de Asalto" "5. Grupo A�reo de Asalto" "6. Grupo A�reo de Asalto" "7. Grupo A�reo de Asalto" "8. Grupo A�reo de Asalto" "9. Grupo A�reo de Asalto" "10. Grupo A�reo de Asalto" 
	}
	transport_plane = {
		"1. Lineas A�reas de Venezuela" "2. Lineas A�reas de Venezuela" "3. Lineas A�reas de Venezuela" "4. Lineas A�reas de Venezuela" "5. Lineas A�reas de Venezuela" 
	}
	battleship = {
		"ARV Coronel Eduardo Blanco" "ARV Almirante San Pablo" "ARV Caracas" 
	}
	light_cruiser = {
		"ARV Maracay" "ARV Barquisimeto" "ARV Valencia" "ARV Petare Teques" "ARV Puerto de la Cruz" "ARV Cumana" "ARV Maturin" "ARV Sotavento" "ARV Isla de Tortuga" "ARV Pedregal" 
	}
	heavy_cruiser = {
		"ARV Maracay" "ARV Barquisimeto" "ARV Valencia" "ARV Petare Teques" "ARV Puerto de la Cruz" "ARV Cumana" "ARV Maturin" "ARV Sotavento" "ARV Isla de Tortuga" "ARV Pedregal" 
	}
	battlecruiser = {
		"ARV Coronel Eduardo Blanco" "ARV Almirante San Pablo" "ARV Caracas" 
	}
	destroyer = {
		"Flotilla San Fernando" "Flotilla Achaguas" "Flotilla Puerto Ayacucho" "Flotilla Puerto P�ez" "Flotilla Puerto Carre�o" "Flotilla La Esmeralda" "Flotilla San Carlos" "Flotilla Rio Negro" "Flotilla El Dorado" "Flotilla Lumeremo" 
	}
	carrier = {
		"ARV Rep�blica de Venezuela" "ARV Sim�n Bol�var" 
	}
	escort_carrier = {
		"ARV General Urdaneta" "ARV Almirante Bri�n" "ARV General Soublette" "ARV General Sal�m" 
	}
	submarine = {
		"Flotilla de Submarinos S100" "Flotilla de Submarinos S101" "Flotilla de Submarinos S102" "Flotilla de Submarinos S103" "Flotilla de Submarinos S104" "Flotilla de Submarinos S105" "Flotilla de Submarinos S106" "Flotilla de Submarinos S107" "Flotilla de Submarinos S108" "Flotilla de Submarinos S109" 
	}
	transport_ship = {
		"Fl. de Transporte Naval nro. 1" "Fl. de Transporte Naval nro. 2" "Fl. de Transporte Naval nro. 3" "Fl. de Transporte Naval nro. 4" "Fl. de Transporte Naval nro. 5" "Fl. de Transporte Naval nro. 6" "Fl. de Transporte Naval nro. 7" "Fl. de Transporte Naval nro. 8" "Fl. de Transporte Naval nro. 9" "Fl. de Transporte Naval nro. 10" 
	}

}

ministers = {
	
}
