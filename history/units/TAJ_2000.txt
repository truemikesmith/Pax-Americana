theatre = {
	name = "Armed Forces of Tajikistan"
	location = 9054
	regiment = { type = hq_brigade name = "Armed Forces HQ" historical_model = 1 }
	division = {
		is_reserve = yes
		name = "Rapid Reaction Brigade"
		location = 9054
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "11th 'Faizali' Brigade"
		location = 9054
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}	
}
