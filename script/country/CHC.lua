
local P = {}
AI_CHC = P

function P.ForeignMinister_Influence(voForeignMinisterData)
	local laIgnoreWatch -- Ignore this country but monitor them if they are about to join someone else
	local laWatch -- Monitor them and also fi their score is high enough they can be influenced normally
	local laIgnore -- Ignore them completely

	if voForeignMinisterData.FactionName == "comintern" then
	
		laWatch = {
		}
			
		laIgnoreWatch = {
		}
			
		laIgnore = {
			"ISS"} -- Vichy
			
		-- Make a list of countries that are not in Asia and ignore them
		-- for loTCountry in CCurrentGameState.GetCountries() do
			-- if loTCountry:Exists() then
				-- local lsContinent = tostring(loTCountry:GetActingCapitalLocation():GetContinent():GetTag())
				-- If they are not in Asia then ignore them
				-- if lsContinent == "north_america" or lsContinent == "south_america" then
					-- table.insert(laWatch, tostring(loTCountry:GetCountryTag()))
				-- end
			-- end
		-- end				
	end
	
	return laWatch, laIgnoreWatch, laIgnore
end


-- Return:
return AI_CHC

