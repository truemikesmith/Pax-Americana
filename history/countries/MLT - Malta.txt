capital = 5359
government = republic
ideology = market_liberal
alignment = { x = 0 y = -20 }
neutrality = 100
national_unity = 80
join_faction = allies

oob = "MLT_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 52
	market_liberal = 0
	social_liberal = 1
	social_democrat = 47
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 156
	market_liberal = 0
	social_liberal = 3
	social_democrat = 141
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = multiple_laws
civil_laws = open_society
economic_policy = lais_fair
healthcare = public_care
service_length = six_months

infantry_brigade_activation = 1
infantry_small_arms = 2
infantry_heavy_weapons = 2
infantry_anti_tank = 2
infantry_support = 2
truck_engines = 2
engineer_regiment = 1
demolition_equipment = 2
bridging_equipment = 2
military_police_training = 1
security_training = 2
combat_training = 2
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
superior_firepower = 1
static_defence = 1
solid_air_defence = 1
human_wave = 1
firesupport_coordination = 1

1936.1.1 = { decision = allow_research }