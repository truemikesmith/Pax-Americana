30001 = {
	name = "Vozinha"
	country = CVE
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L30001
	add_trait = marine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30002 = {
	name = "Somada"
	country = CVE
	type = land
	skill = 0
	max_skill = 1
	loyalty = 1.00
	picture = L30002
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30003 = {
	name = "Varela"
	country = CVE
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L30003
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30004 = {
	name = "Geg�"
	country = CVE
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L30004
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
30005 = {
	name = "Nivaldo"
	country = CVE
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L30005
	add_trait = defensive_doctrine
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
30006 = {
	name = "Carlitos"
	country = CVE
	type = land
	skill = 0
	max_skill = 1
	loyalty = 1.00
	picture = L30006
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30007 = {
	name = "Babanco"
	country = CVE
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L30007
	add_trait = engineer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30008 = {
	name = "Fortes"
	country = CVE
	type = air
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L30008
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30009 = {
	name = "Platini"
	country = CVE
	type = sea
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L30009
	history = {
		2003.1.1 = { rank = 1 }
	}
}
30010 = {
	name = "Rodrigues"
	country = CVE
	type = sea
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L30010
	add_trait = spotter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
