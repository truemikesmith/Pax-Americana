theatre = {
	name = "Republic of China Armed Forces"
	location = 5737
	regiment = { type = hq_brigade name = "Armed Forces HQ" historical_model = 2 }
	army = {
		name = "Republic of China Army"
		location = 5737
		regiment = { type = hq_brigade name = "Army HQ" historical_model = 2 }
		corps = {
			name = "Aviation and Special Forces Command"
			location = 5737
			regiment = { type = hq_brigade name = "ASFC-HQ" historical_model = 2 }
			division = {
				is_reserve = yes
				name = "601 Air Cavalry Brigade"
				location = 5737
				regiment = { type = air_cavalry_brigade name = "6011 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6012 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6013 Air Cavalry Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "602 Air Cavalry Brigade"
				location = 5737
				regiment = { type = air_cavalry_brigade name = "6021 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6022 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6023 Air Cavalry Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "603 Air Cavalry Brigade"
				location = 5737
				regiment = { type = air_cavalry_brigade name = "6031 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6032 Air Cavalry Battalion" historical_model = 0 }
				regiment = { type = air_cavalry_brigade name = "6033 Air Cavalry Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "862 Special Operation Group"
				location = 5737
				regiment = { type = special_forces_brigade name = "3rd Battalion" historical_model = 2 }
				regiment = { type = special_forces_brigade name = "4th Battalion" historical_model = 2 }
				regiment = { type = special_forces_brigade name = "6th Battalion" historical_model = 2 }
			}
			division = {
				is_reserve = yes
				name = "871 Special Operation Group"
				location = 5737
				regiment = { type = special_forces_brigade name = "1st Battalion" historical_model = 2 }
				regiment = { type = special_forces_brigade name = "2nd Battalion" historical_model = 2 }
				regiment = { type = special_forces_brigade name = "5th Battalion" historical_model = 2 }
			}
		}
		corps = {
			name = "6th Army Corps"
			location = 5747
			regiment = { type = hq_brigade name = "6th Army Corps HQ" historical_model = 2 }
			division = {
				is_reserve = yes
				name = "269 Mechanized Brigade"
				location = 5747
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = mechanized_brigade name = "2693rd Mechanized Battalion" historical_model = 0 }
				regiment = { type = sp_artillery_brigade name = "21 Artillery Command" historical_model = 2 }
			}
			division = {
				is_reserve = yes
				name = "542 Armor Brigade"
				location = 5747
				regiment = { type = armor_brigade name = "5421st Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5422nd Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5423rd Armor Battalion" historical_model = 0 }
				regiment = { type = engineer_brigade name = "53 Engineer Group" historical_model = 2 }
			}
			division = {
				is_reserve = yes
				name = "584 Armor Brigade"
				location = 5747
				regiment = { type = armor_brigade name = "5841st Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5842nd Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5843rd Armor Battalion" historical_model = 0 }
			}
		}
		corps = {
			name = "8th Army Corps"
			location = 5824
			regiment = { type = hq_brigade name = "8th Army Corps HQ" historical_model = 2 }
			division = {
				is_reserve = yes
				name = "298 Mechanized Brigade"
				location = 5824
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = mechanized_brigade name = "2983rd Mechanized Battalion" historical_model = 0 }
				regiment = { type = sp_artillery_brigade name = "43 Artillery Command" historical_model = 2 }
			}
			division = {
				is_reserve = yes
				name = "564 Armor Brigade"
				location = 5824
				regiment = { type = armor_brigade name = "5641st Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5642nd Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5643rd Armor Battalion" historical_model = 0 }
				regiment = { type = engineer_brigade name = "54 Engineer Group" historical_model = 2 }
			}
		}
		corps = {
			name = "10th Army Corps"
			location = 5794
			regiment = { type = hq_brigade name = "10th Army Corps" historical_model = 2 }
			division = {
				is_reserve = yes
				name = "200 Mechanized Brigade"
				location = 5794
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = mechanized_brigade name = "2003rd Mechanized Battalion" historical_model = 0 }
				regiment = { type = sp_artillery_brigade name = "58 Artillery Command" historical_model = 2 }
			}
			division = {
				is_reserve = yes
				name = "586 Armor Brigade"
				location = 5794
				regiment = { type = armor_brigade name = "5861st Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5862nd Armor Battalion" historical_model = 0 }
				regiment = { type = armor_brigade name = "5863rd Armor Battalion" historical_model = 0 }
				regiment = { type = engineer_brigade name = "52 Engineer Group" historical_model = 2 }
			}
		}
	}
	
	air = {
	name = "401st Tactical Wing"
	base = 5769
	location = 5769
	wing = { type = multi_role name = "17th Tactical Fighter Group 'Thor'" historical_model = 0 }
	wing = { type = multi_role name = "26th Tactical Fighter Group 'Whitch'" historical_model = 0 }
	wing = { type = multi_role name = "27th Tactical Fighter Group 'Black Dragon'" historical_model = 0 }
	}
	
	air = {
	name = "455th Tactical Fighter Wing"
	base = 5809
	location = 5809
	wing = { type = multi_role name = "21st Tactical Fighter Group" historical_model = 0 }
	wing = { type = multi_role name = "22nd Tactical Fighter Group" historical_model = 0 }
	}
	
	air = {
	name = "499th Tactical Fighter Wing"
	base = 5769
	location = 5769
	wing = { type = multi_role name = "41st Tactical Fighter Group 'Holy Shield'" historical_model = 0 }
	wing = { type = multi_role name = "42nd Tactical Fighter Group 'Cobra'" historical_model = 0 }
	}
	
	air = {
	name = "427th Tactical Fighter Wing"
	base = 5737
	location = 5737
	wing = { type = multi_role name = "7th Tactical Fighter Group 'Wolf'" historical_model = 0 }
	wing = { type = multi_role name = "28th Tactical Fighter Group 'Baby Dragon'" historical_model = 0 }
	}
	
	air = {
	name = "443rd Tactical Fighter Wing"
	base = 5809
	location = 5809
	wing = { type = multi_role name = "1st Tactical Fighter Group" historical_model = 0 }
	wing = { type = multi_role name = "3rd Tactical Fighter Group" historical_model = 0 }
	}
	
	air = {
	name = "737th Tactical Fighter Wing"
	base = 5809
	location = 5809
	wing = { type = interceptor name = "44th Fighter Squadron" historical_model = 0 }
	}
	
	air = {
	name = "10th Tactical Airlift Group"
	base = 5809
	location = 5809
	wing = { type = transport_plane name = "101st Airlift Squadron" historical_model = 0 }
	wing = { type = transport_plane name = "102nd Airlift Squadron" historical_model = 0 }
	}
	
	navy = {
	name = "Republic of China Navy"
	base = 5737
	location = 5737
	ship = { type = heavy_cruiser name = "ROCS Cheng Kung" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Cheng Ho" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Chi Kuang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Yueh Fei" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Tzu I" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Pan Chao" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Chang Chien" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Chih Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Lan Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Ning Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Hae Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Fong Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Fen Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Hwai Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Ki Yang" historical_model = 0 }
	ship = { type = heavy_cruiser name = "ROCS Kang Ding" historical_model = 1 }
	ship = { type = heavy_cruiser name = "ROCS Si Ning" historical_model = 1 }
	ship = { type = heavy_cruiser name = "ROCS Kun Ming" historical_model = 1 }
	ship = { type = heavy_cruiser name = "ROCS Di Hua" historical_model = 1 }
	ship = { type = heavy_cruiser name = "ROCS Wu Chang" historical_model = 1 }
	ship = { type = heavy_cruiser name = "ROCS Chen De" historical_model = 1 }
	ship = { type = submarine name = "Hai Lung" historical_model = 0 }
	ship = { type = submarine name = "Hai Hu" historical_model = 0 }
	ship = { type = submarine name = "Hai Shih" historical_model = 0 }
	ship = { type = submarine name = "Hai Bao" historical_model = 0 }
	ship = { type = transport_ship name = "Hsuhai" historical_model = 0 }
	ship = { type = transport_ship name = "Chung Cheng" historical_model = 0 }
	}
	
}
	