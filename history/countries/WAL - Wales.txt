capital = 1905
government = republic
ideology = social_democrat
alignment = { x = 0 y = -146 }
neutrality = 100
national_unity = 80

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 28
	social_conservative = 19
	market_liberal = 0
	social_liberal = 14
	social_democrat = 38
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 84
	social_conservative = 57
	market_liberal = 0
	social_liberal = 38
	social_democrat = 114
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

1936.1.1 = { decision = allow_research }