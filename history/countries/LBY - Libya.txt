capital = 5445
government = dictatorship
ideology = leninist
alignment = { x = 100 y = 100 }
neutrality = 100
national_unity = 70
oob = "LBY_1936.txt"
head_of_state = 78513
popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 0
	market_liberal = 0
	social_liberal = 0
	social_democrat = 0
	left_wing_radical = 0
	leninist = 70
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 0
	market_liberal = 0
	social_liberal = 0
	social_democrat = 0
	left_wing_radical = 0
	leninist = 120
	stalinist = 0
}

conscription_policy = military_service
press_laws = limited_censorship
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = restrictions
economic_policy = partial_control
healthcare = public_care
service_length = eighteen_months

#Unit Sizes and brigade size
infantry_increase = 2
special_forces_increase = 2
mobile_increase = 2
armor_increase = 2
recon_increase = 2
artillery_increase = 2
armorsupport_increase = 2
aa_at_increase = 2
engineers_increase = 2
Max_BRG_Size = 2

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
military_police_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
special_forces_regiment = 1
special_small_arms = 1
special_anti_tank = 1
special_heavy_weapons = 1
special_support_weapons = 1
superior_firepower = 1
air_superiority_doctrine = 1
cas_doctrine = 1
green_water_doctrine = 1

1936.1.1 = { decision = allow_research }