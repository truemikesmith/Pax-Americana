###Iran intervene ISIS
country_event = {
	
	id = 14100
	fire_only_once = yes
	title = EVTNAME13101
	desc = EVTDESC13101
	picture = ""
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = PER
		USA = {
			has_country_flag = ME_isolation
			date = 2016.03.01
		}
		PER = {
			has_country_flag = fund_iraq
		}
		IRQ = {war_with = ISS}
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "Intervene"
		PER = {
			relation = { who = IRQ value = 200 }
			create_alliance = IRQ
			war = {
				target = ISS
				attacker_goal = { casus_belli = Defeat_ISS }
				defender_goal = { casus_belli = conquer }
			}			
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}

##Purchase S-300 from russia
country_event = {
	
	id = 14101
	fire_only_once = yes
	title = EVTNAME14101
	desc = EVTDESC14101
	picture = "s300"
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = PER
		USA = {
			has_country_flag = US_Iran_Pivot2
			date = 2016.02.01
		}
		PER = {
			money = 200
		}
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "ok"
		PER = {
			load_oob = "zDD_RUS_Iran_1.txt"
			anti_airs = 300
			anti_air_gun = 300
			anti_air_carriage = 300
			set_country_flag = RUS_S300_1
			money = -50
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}
##re-engineer the S-300 System
country_event = {
	
	id = 14102
	fire_only_once = yes
	title = EVTNAME14102
	desc = EVTDESC14102
	picture = "s300"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = PER
		PER = {
			money = 200
			has_country_flag = RUS_S300_1
		}
		date = 2016.06.01
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "ok"
		PER = {
			anti_airs = 600
			anti_air_gun = 600
			anti_air_carriage = 600
			set_country_flag = RUS_S300_2
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}

#Oil deal With France
country_event = {
	
	id = 14103
	fire_only_once = yes
	title = EVTNAME14103
	desc = EVTDESC14103
	picture = "FRA_PER_hand_shake"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = PER
		USA = {
			has_country_flag = US_Iran_Pivot2
		}
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "ok"
		FRA = {
			country_event = 14104
		}
		ITA = {
			country_event = 14104
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}
#Oil deal With France
country_event = {
	
	id = 14104
	#fire_only_once = yes
	title = EVTNAME14104
	desc = EVTDESC14104
	picture = "FRA_PER_hand_shake"
	is_triggered_only = yes
	major = yes
	
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "ok"
		THIS = {
			crude_oil = 10000
			fuel = 6000
			money = -300
			#relations+
		}
		FROM = {money = 300}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}

#Oil deal With China
country_event = {
	
	id = 14105
	fire_only_once = yes
	title = "Oil deal With China"
	desc = "duh"
	picture = "CHC_PER_hand_shake"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = PER
		USA = {
			has_country_flag = US_Iran_Pivot2
		}
		date = 2015.06.01
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "ok"
		CHC = {
			country_event = 14106
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}
#Oil deal With China
country_event = {
	
	id = 14106
	#fire_only_once = yes
	title = "Oil deal with Iran"
	desc = "duh"
	picture = "CHC_PER_hand_shake"
	is_triggered_only = yes
	major = yes
	
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "ok"
		CHC = {
			crude_oil = 10000
			fuel = 6000
			money = -300
		}
		PER = {money = 300}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}

#SU-35 Deal
country_event = {
	
	id = 14107
	fire_only_once = yes
	title = "SU-35 deal with russia"
	desc = "duh"
	picture = "RUS_PER_hand_shake"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = PER
		USA = {
			has_country_flag = US_Iran_Pivot2
		}
		date = 2016.04.01
	}
	
	mean_time_to_happen =  {
		days = 20
	}
		
	option = {
		name = "ok"
		RUS = {
			country_event = 14108
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}
#SU-35 Deal
country_event = {
	
	id = 14108
	#fire_only_once = yes
	title = "SU-35 deal with Iran"
	desc = "duh"
	picture = "RUS_PER_hand_shake"
	is_triggered_only = yes
	major = yes
	
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "ok"
		RUS = {
			money = 300
		}
		PER = {
			load_oob = "zDD_RUS_Iran_2.txt"
			money = -300
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "Don't"
	}
}
