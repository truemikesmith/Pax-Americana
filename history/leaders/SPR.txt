147001 = {
	name = "Abadiano"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147001
	add_trait = marine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147002 = {
	name = "Abarca"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 0.00
	picture = L147002
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147003 = {
	name = "Acevedo"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147003
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147004 = {
	name = "Acosta"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147004
	add_trait = assaulter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147005 = {
	name = "Aguinaga"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 0.00
	picture = L147005
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147006 = {
	name = "Altamira"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147006
	add_trait = engineer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147007 = {
	name = "Aranda"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 0.00
	picture = L147007
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147008 = {
	name = "Asencio"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147008
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147009 = {
	name = "Azpilicueta"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147009
	add_trait = improvisation
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147010 = {
	name = "Barboza"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147010
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147011 = {
	name = "Becerril"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147011
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147012 = {
	name = "Bola�os"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147012
	add_trait = landing_specialist
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147013 = {
	name = "Bracamontes"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147013
	add_trait = hard_defender
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147014 = {
	name = "Bustamante"
	country = SPR
	type = land
	skill = 0
	max_skill = 5
	loyalty = 1.00
	picture = L147014
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147015 = {
	name = "Caraballo"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147015
	add_trait = learners_will
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147016 = {
	name = "Carpintero"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 0.00
	picture = L147016
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147017 = {
	name = "Carreras"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147017
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147018 = {
	name = "Casta�eda"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147018
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147019 = {
	name = "Cervantes"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147019
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147020 = {
	name = "Chac�n"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147020
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147021 = {
	name = "Col�n"
	country = SPR
	type = land
	skill = 0
	max_skill = 5
	loyalty = 0.00
	picture = L147021
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147022 = {
	name = "Coria"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 0.00
	picture = L147022
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147023 = {
	name = "Crespi"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147023
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147024 = {
	name = "Delgado"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147024
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147025 = {
	name = "Duarte"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147025
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147026 = {
	name = "Esguerra"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147026
	add_trait = engineer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147027 = {
	name = "Espejo"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147027
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147028 = {
	name = "Espinosa"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147028
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147029 = {
	name = "Fajardo"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147029
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147030 = {
	name = "Florencio"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147030
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147031 = {
	name = "Fuenmayor"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147031
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147032 = {
	name = "Gallardo"
	country = SPR
	type = land
	skill = 0
	max_skill = 5
	loyalty = 1.00
	picture = L147032
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147033 = {
	name = "Garbajosa"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147033
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147034 = {
	name = "Godoy"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147034
	add_trait = fortress_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147035 = {
	name = "Guardia"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147035
	add_trait = landing_specialist
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147036 = {
	name = "Guzm�n"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147036
	add_trait = logistics_wizard
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147037 = {
	name = "Hechavarr�a"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147037
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147038 = {
	name = "Herrera"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147038
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147039 = {
	name = "Hurtado"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147039
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147040 = {
	name = "Ib��ez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147040
	add_trait = trainer
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147041 = {
	name = "Jaco"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147041
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147042 = {
	name = "Jim�nez"
	country = SPR
	type = land
	skill = 0
	max_skill = 5
	loyalty = 1.00
	picture = L147042
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147043 = {
	name = "Lebr�n"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147043
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147044 = {
	name = "Libertad"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147044
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147045 = {
	name = "Llorente"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147045
	add_trait = fortress_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147046 = {
	name = "Lucero"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147046
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147047 = {
	name = "Lugo"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147047
	add_trait = engineer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147048 = {
	name = "Madrazo"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147048
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147049 = {
	name = "Marciano"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147049
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147050 = {
	name = "Monreal"
	country = SPR
	type = land
	skill = 1
	max_skill = 5
	loyalty = 0.00
	picture = L147050
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147051 = {
	name = "Morales"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147051
	add_trait = improvisation
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147052 = {
	name = "Aguado"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147052
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147053 = {
	name = "�lvarez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147053
	add_trait = hard_defender
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147054 = {
	name = "Bocanegra"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147054
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147055 = {
	name = "Camarero"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147055
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147056 = {
	name = "Cantero"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147056
	add_trait = landlubber
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147057 = {
	name = "Castro"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147057
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147058 = {
	name = "Chiarini"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147058
	add_trait = landing_specialist
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147059 = {
	name = "Coll"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147059
	add_trait = offensive_doctrine
	add_trait = assaulter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147060 = {
	name = "Coloma"
	country = SPR
	type = land
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147060
	add_trait = old_guard
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147061 = {
	name = "de Gregorio"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147061
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147062 = {
	name = "del Estado"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147062
	add_trait = commando
	add_trait = learners_will
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147063 = {
	name = "Esteban"
	country = SPR
	type = land
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147063
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147064 = {
	name = "Garc�a"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147064
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147065 = {
	name = "Gonzalez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147065
	add_trait = fortress_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147066 = {
	name = "Minguez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147066
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147067 = {
	name = "Monta�o"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147067
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147068 = {
	name = "Navarro"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147068
	add_trait = assaulter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147069 = {
	name = "Paz Figueroa"
	country = SPR
	type = land
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147069
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147070 = {
	name = "Pesadas"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147070
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147071 = {
	name = "Pontijas Deus"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147071
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147072 = {
	name = "Ram�rez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147072
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147073 = {
	name = "S�nchez"
	country = SPR
	type = land
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147073
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147074 = {
	name = "V�zquez"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147074
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147075 = {
	name = "Villalain"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147075
	add_trait = improvisation
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147076 = {
	name = "Bucher"
	country = SPR
	type = land
	skill = 4
	max_skill = 8
	loyalty = 1.00
	picture = L147076
	add_trait = logistics_wizard
	add_trait = engineer
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147077 = {
	name = "Cebri�n"
	country = SPR
	type = land
	skill = 4
	max_skill = 8
	loyalty = 1.00
	picture = L147077
	add_trait = offensive_doctrine
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147078 = {
	name = "del Manzano"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147078
	add_trait = assaulter
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147079 = {
	name = "de Otazu"
	country = SPR
	type = land
	skill = 4
	max_skill = 8
	loyalty = 1.00
	picture = L147079
	add_trait = frontline_soldier
	add_trait = improvisation
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147080 = {
	name = "Medina"
	country = SPR
	type = land
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147080
	add_trait = old_guard
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147081 = {
	name = "Pitarch"
	country = SPR
	type = land
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147081
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147082 = {
	name = "Sintes"
	country = SPR
	type = land
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147082
	add_trait = hard_defender
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147083 = {
	name = "Alejandre"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147083
	add_trait = frontline_soldier
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147084 = {
	name = "�lvarez-Espejo"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147084
	add_trait = trickster
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147085 = {
	name = "Asarta"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147085
	add_trait = landing_specialist
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147086 = {
	name = "Barr�s"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147086
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147087 = {
	name = "Carballo"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147087
	add_trait = panzer_leader
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147088 = {
	name = "de Guevara"
	country = SPR
	type = land
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147088
	add_trait = hard_defender
	add_trait = trickster
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147089 = {
	name = "Dominguez Buj"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147089
	add_trait = offensive_doctrine
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147090 = {
	name = "Garcia"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147090
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147091 = {
	name = "Lastres"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147091
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147092 = {
	name = "Milani"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147092
	add_trait = assaulter
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147093 = {
	name = "Pesse"
	country = SPR
	type = land
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147093
	add_trait = trainer
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147094 = {
	name = "Ramos"
	country = SPR
	type = land
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147094
	add_trait = improvisation
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147095 = {
	name = "Sanju�n"
	country = SPR
	type = land
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147095
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147096 = {
	name = "Palomo"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147096
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147097 = {
	name = "Pazmi�o"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147097
	add_trait = superior_air_tactician
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147098 = {
	name = "Pichardo"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 0.00
	picture = L147098
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147099 = {
	name = "Quesada"
	country = SPR
	type = air
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147099
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147100 = {
	name = "Rafaeli"
	country = SPR
	type = air
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147100
	add_trait = tank_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147101 = {
	name = "Santander"
	country = SPR
	type = air
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147101
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147102 = {
	name = "Arnaiz"
	country = SPR
	type = air
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147102
	add_trait = superior_air_tactician
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147103 = {
	name = "Carrasco"
	country = SPR
	type = air
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147103
	add_trait = fleet_destroyer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147104 = {
	name = "de Bobadilla"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147104
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147105 = {
	name = "G�mez"
	country = SPR
	type = air
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147105
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147106 = {
	name = "Jim�nez"
	country = SPR
	type = air
	skill = 2
	max_skill = 8
	loyalty = 1.00
	picture = L147106
	add_trait = carpet_bomber
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147107 = {
	name = "Latorre"
	country = SPR
	type = air
	skill = 2
	max_skill = 5
	loyalty = 1.00
	picture = L147107
	add_trait = old_guard
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147108 = {
	name = "Palacio"
	country = SPR
	type = air
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147108
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147109 = {
	name = "S�nchez"
	country = SPR
	type = air
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147109
	add_trait = fleet_destroyer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147110 = {
	name = "Sejema"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147110
	add_trait = superior_air_tactician
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147111 = {
	name = "Jim�nez"
	country = SPR
	type = air
	skill = 4
	max_skill = 8
	loyalty = 1.00
	picture = L147111
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147112 = {
	name = "Lens"
	country = SPR
	type = air
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147112
	add_trait = superior_air_tactician
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147113 = {
	name = "Quintana"
	country = SPR
	type = air
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147113
	add_trait = night_flyer
	add_trait = tank_buster
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147114 = {
	name = "Rodriguez"
	country = SPR
	type = air
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147114
	add_trait = fleet_destroyer
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147115 = {
	name = "Abad"
	country = SPR
	type = air
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147115
	add_trait = night_flyer
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147116 = {
	name = "de Los Dolores"
	country = SPR
	type = air
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147116
	add_trait = tank_buster
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147117 = {
	name = "Arruche"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147117
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147118 = {
	name = "de Borb�n"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147118
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147119 = {
	name = "Del Castillo"
	country = SPR
	type = air
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147119
	add_trait = tank_buster
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147120 = {
	name = "Da Pena"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147120
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147121 = {
	name = "Garc�a"
	country = SPR
	type = air
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147121
	add_trait = night_flyer
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147122 = {
	name = "P�rez"
	country = SPR
	type = air
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147122
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147123 = {
	name = "Ramos"
	country = SPR
	type = air
	skill = 2
	max_skill = 6
	loyalty = 0.00
	picture = L147123
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147124 = {
	name = "Saucedo"
	country = SPR
	type = sea
	skill = 1
	max_skill = 6
	loyalty = 1.00
	picture = L147124
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147125 = {
	name = "Suarez"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147125
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147126 = {
	name = "Toledano"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147126
	add_trait = superior_tactician
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147127 = {
	name = "Urqu�a"
	country = SPR
	type = sea
	skill = 1
	max_skill = 5
	loyalty = 0.00
	picture = L147127
	add_trait = seawolf
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147128 = {
	name = "Zapata"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147128
	add_trait = seawolf
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147129 = {
	name = "Bauz�"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147129
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147130 = {
	name = "Beceiro"
	country = SPR
	type = sea
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147130
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147131 = {
	name = "Hertfelder"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147131
	add_trait = blockade_runner
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147132 = {
	name = "Nieto"
	country = SPR
	type = sea
	skill = 1
	max_skill = 5
	loyalty = 1.00
	picture = L147132
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147133 = {
	name = "Pellegrino"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147133
	add_trait = spotter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147134 = {
	name = "S�nchez"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147134
	add_trait = seawolf
	history = {
		2003.1.1 = { rank = 1 }
	}
}
147135 = {
	name = "Pelluz"
	country = SPR
	type = sea
	skill = 4
	max_skill = 8
	loyalty = 1.00
	picture = L147135
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147136 = {
	name = "Pery Paredes"
	country = SPR
	type = sea
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147136
	add_trait = spotter
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147137 = {
	name = "Romero"
	country = SPR
	type = sea
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147137
	add_trait = superior_tactician
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
147138 = {
	name = "Delgado"
	country = SPR
	type = sea
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147138
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147139 = {
	name = "Rodr�guez"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147139
	add_trait = blockade_runner
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
147140 = {
	name = "Bol�bar"
	country = SPR
	type = sea
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147140
	add_trait = spotter
	add_trait = superior_tactician
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147141 = {
	name = "Bueno"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147141
	add_trait = superior_tactician
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147142 = {
	name = "Chicharro"
	country = SPR
	type = sea
	skill = 2
	max_skill = 7
	loyalty = 1.00
	picture = L147142
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147143 = {
	name = "de Espinosa"
	country = SPR
	type = sea
	skill = 3
	max_skill = 7
	loyalty = 1.00
	picture = L147143
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147144 = {
	name = "de la Puente"
	country = SPR
	type = sea
	skill = 3
	max_skill = 8
	loyalty = 1.00
	picture = L147144
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147145 = {
	name = "L�pez"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147145
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
147146 = {
	name = "Manso"
	country = SPR
	type = sea
	skill = 2
	max_skill = 6
	loyalty = 1.00
	picture = L147146
	add_trait = blockade_runner
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
