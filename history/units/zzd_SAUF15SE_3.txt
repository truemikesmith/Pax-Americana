military_construction = {
	country = SAU
	multi_role ={ name = "Saudi F-15SE Wing 5" historical_model = 9 }
	cost = 1.50 #The engine will use practicals to determine the actual IC cost; this is just a placeholder
	progress = 99
	duration = 1
}
military_construction = {
	country = SAU
	multi_role ={ name = "Saudi F-15SE Wing 6" historical_model = 9 }
	cost = 1.50 #The engine will use practicals to determine the actual IC cost; this is just a placeholder
	progress = 99
	duration = 1
}
