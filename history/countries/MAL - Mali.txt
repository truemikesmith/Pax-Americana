capital = 9758
government = republic_president
ideology = social_democrat
alignment = { x = 0 y = 50 }
neutrality = 100
national_unity = 70
oob = "MAL_2000.txt"
head_of_state = 78520
popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 3
	social_conservative = 29
	market_liberal = 8
	social_liberal = 6
	social_democrat = 33
	left_wing_radical = 1
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 9
	social_conservative = 87
	market_liberal = 24
	social_liberal = 18
	social_democrat = 99
	left_wing_radical = 3
	leninist = 0
	stalinist = 0
}

conscription_policy = partial_draft
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
service_length = twofour_months
economic_policy = lais_fair

infantry_brigade_activation = 1
special_forces_regiment = 1
paratrooper_infantry = 1
superior_firepower = 1
air_superiority_doctrine = 1

1936.1.1 = { decision = allow_research }