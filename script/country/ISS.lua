
local P = {}
AI_ISS = P


function P.DiploScore_InviteToFaction(voDiploScoreObj)
	local loAllies = CCurrentGameState.GetFaction("allies")
	local loComi = CCurrentGameState.GetFaction("comintern")
	local loAxis = CCurrentGameState.GetFaction("axis")
	
	-- Only go through these checks if we are being asked to join the Allies
	if (voDiploScoreObj.Faction == loAllies) or (voDiploScoreObj.Faction == loComi) or (voDiploScoreObj.Faction == loAxis) then
		voDiploScoreObj.Score = 0
	end
	-- if voDiploScoreObj.Faction = loComi then
		-- voDiploScoreObj.Score = 0
	-- end
	-- if voDiploScoreObj.Faction = loAxis then
		-- voDiploScoreObj.Score = 0
	-- end
	
	return voDiploScoreObj.Score
end

function P.ForeignMinister_Alignment(...)
	return Support.AlignmentNeutral(...)
end

-- Return:
return AI_ISS

