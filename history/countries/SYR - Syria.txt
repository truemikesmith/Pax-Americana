capital = 7416
government = dictatorship
ideology = fascistic
alignment = { x = 0 y = 50 }
neutrality = 100
national_unity = 70
head_of_state = 78501


oob = "SYR_1936.txt"

popularity = { 
	national_socialist = 0
	fascistic = 30
	paternal_autocrat = 0
	social_conservative = 10
	market_liberal = 0
	social_liberal = 0
	social_democrat = 0
	left_wing_radical = 10
	leninist = 10
	stalinist = 10
}

organization = { 
	national_socialist = 0
	fascistic = 90
	paternal_autocrat = 0
	social_conservative = 30
	market_liberal = 0
	social_liberal = 0
	social_democrat = 0
	left_wing_radical = 30
	leninist = 30
	stalinist = 30
}

####TECHS#######################

air_superiority_doctrine = 1
fighter_pilot_training = 5
fighter_groundcrew_training = 5
#^Offset year is 3
interception_tactics = 6
#^Offset year is 3
fighter_ground_control = 6
#^Offset year is 3
bomber_targerting_focus = 6
#^Offset year is 3
fighter_targerting_focus = 6
#^Offset year is 3
cas_doctrine = 1
cas_pilot_training = 6
#^Offset year is 3
cas_groundcrew_training = 6
#^Offset year is 3
ground_attack_tactics = 6
#^Offset year is 3
forward_air_control = 6
#^Offset year is 3
battlefield_interdiction = 5
#^Offset year is 3
interdiction_tactics = 6
#^Offset year is 3
tactical_air_command = 6
#^Offset year is 3
bomber_focus_doctrine = 1
bomber_pilot_training = 5
#^Offset year is 3
bomber_groundcrew_training = 6
#^Offset year is 3
logistical_strike_tactics = 5
#^Offset year is 3
installation_strike_tactics = 4
#^Offset year is 3
airbase_strike_tactics = 5
#^Offset year is 3
strategic_bombardment_tactics = 4
#^Offset year is 3
airborne_assault_tactics = 3
#^Offset year is 3
strategic_air_command = 5
#^Offset year is 3
naval_aviation_doctrine = 1
nav_pilot_training = 2
#^Offset year is 3
nav_groundcrew_training = 2
#^Offset year is 3
portstrike_tactics = 3
#^Offset year is 3
navalstrike_tactics = 2
#^Offset year is 3
naval_air_targeting = 3
#^Offset year is 3
naval_tactics = 2
#^Offset year is 3


bombers = 1
bomber_bombs = 0
#^Offset year is 2
bomber_engine = 0
#^Offset year is 2
bomber_fuselage = 0
#^Offset year is 2
bomber_fuel_tank = 0
#^Offset year is 2
transport_planes = 0
transport_plane_cargo_hold = 0
#^Offset year is 2
#^Offset year is 2
bomber_countermeasures = 0
#^Offset year is 2
bomber_radar = 0
#^Offset year is 2
srbm = 0
tbm = 0
icbm = 0
missile_propulsion = 0
#^Offset year is 2
missile_warhead = 0
#^Offset year is 4
air_superiority_fighter = 0
air_to_air_weaponry = 0
#^Offset year is 2
Fighter_fuselage = 0
#^Offset year is 2
Fighter_engine = 0
#^Offset year is 2
Fighter_fuel_tank = 0
#^Offset year is 2
close_air_support = 0
air_to_surface_weapons = 0
#^Offset year is 2
jet_datalink = 0
Jet_HUD = 0
Distributed_Aperture_Sys = 0
carrier_air_group = 0
cag_Catapult = 0
CAG_VTOL = 0
cag_electro_Catapult = 0
multi_role_fighter = 0
infared_tracking = 0
AESA_radars = 0
Advanced_BVR = 0
fighter_countermeasures = 0
#^Offset year is 2
fighter_radar = 0
#^Offset year is 2
Vector_thrust = 0
Fly_by_wire = 0
Information_Fusion = 0
Super_cruise = 0
Generational_Upgrade = 0
#^Offset year is 1
#^Offset year is 1
#^Offset year is 1


steel_research = 0
#^Offset year is 2
Uranium_research = 0
#^Offset year is 2
ceramic_research = 0
#^Offset year is 2
Composite_research = 0
#^Offset year is 2
nantostructure_research = 0
#^Offset year is 2
artillery = 1
artillery_carriage = 7
#^Offset year is 2
anti_tank = 0
anti_tank_ordnance = 0
#^Offset year is 2
anti_tank_carriage = 0
#^Offset year is 2
anti_airs = 0
anti_air_gun = 0
#^Offset year is 2
anti_air_carriage = 0
#^Offset year is 2
sam_batteries = 0
#^Offset year is 2
sp_anti_air = 0
sp_anti_air_armament = 0
#^Offset year is 2
sp_anti_air_chassis = 0
#^Offset year is 2


mechanized_infantry_brigade = 0
armor_engines = 0
#^Offset year is 4
High_velocity_gun = 0
#^Offset year is 2
Med_velocity_gun = 0
#^Offset year is 2
Small_arms_armor = 0
#^Offset year is 2
apc_armour = 0
#^Offset year is 4
tank_brigade = 0
tank_armor = 0
#^Offset year is 4
gunships = 0
gunship_armament = 0
#^Offset year is 2
gunship_fuselage = 0
#^Offset year is 2
gunship_engines = 0
#^Offset year is 2
sp_artillery = 0
sp_artillery_chassis = 0
#^Offset year is 2
tank_destroyer = 0
td_chassis = 0
#^Offset year is 2


Allow_air_factory = 0
Allow_shipyard = 0
Allow_smallarms_factory = 0
Allow_tank_factory = 0
infra_building = 0
ic_building = 0
air_base_building = 0
naval_base_building = 0
land_fort_building = 0
coastal_fort_building = 0
nuclear_reactor_building = 0


computers = 4
#^Offset year is 2
radar = 1
Passive_radar = 1
active_elec_radar = 1
Guidance_systems = 1
networking = 1
GPS = 1
ECM = 1
encryption_devices = 6
#^Offset year is 3
decryption_devices = 4
#^Offset year is 3
radio_surveillance = 1
radios = 4
#^Offset year is 5


Max_BRG_Size = 2
#^Offset year is 1
militia_increase = 0
#^Offset year is 1
infantry_increase = 0
#^Offset year is 1
special_forces_increase = 0
#^Offset year is 1
mobile_increase = 0
#^Offset year is 1


modern_blitzkrieg = 1
breakthrough_exploitation = 5
#^Offset year is 3
elastic_defence = 4
#^Offset year is 3
flanking_maneuvers = 3
#^Offset year is 3
shock_and_awe = 4
#^Offset year is 3
armor_tactics = 3
#^Offset year is 3
asymmetrical_warfare = 1
defence_in_depth = 6
#^Offset year is 3
flexible_defence = 6
#^Offset year is 3
guerilla_tactics = 0
#^Offset year is 3
special_forces_doctrine = 3
#^Offset year is 3
reserves_doctrine = 4
#^Offset year is 3
superior_firepower = 1
static_defence = 4
#^Offset year is 3
close_air_support_doctrine = 5
#^Offset year is 3
human_wave = 3
#^Offset year is 3
firesupport_coordination = 6
#^Offset year is 3
solid_air_defence = 5
#^Offset year is 3
organisation_tech = 4
#^Offset year is 3
staff_combat_sections = 5
#^Offset year is 3
unit_cooperation_tech = 5
#^Offset year is 3
unit_cohesion = 6
#^Offset year is 3
logistics_tech = 6
#^Offset year is 3


destroyers = 0
Med_veloc_cannon = 0
#^Offset year is 2
large_turbine_engine = 0
#^Offset year is 2
destroyer_armour = 0
#^Offset year is 4
amphibious_assault_ship = 0
amphibious_assault_ship_armour = 0
#^Offset year is 2
Assult_ship_cag_intergration = 0
#^Offset year is 2
cruiser = 0
cruiser_armour = 0
#^Offset year is 4
carriers = 0
Huge_turbine_engine = 0
#^Offset year is 2
Nuclear_propul_carrier = 0
#^Offset year is 2
carrier_armour = 0
#^Offset year is 4
carrier_hangar = 0
#^Offset year is 2
supercarrier = 0
supercarrier_armour = 0
#^Offset year is 4
supercarrier_hangar = 0
#^Offset year is 2
large_radar = 0
#^Offset year is 2
large_sonar = 0
#^Offset year is 2


reactive_armor = 0
slat_armor = 0
Electric_charged_arm = 0
AT_Heat_rounds = 0
AT_Tandem_rounds = 0
AT_top_attack = 0
CIWS = 0
mod_missile_design = 0
#^Offset year is 2
mod_missile_propulsion = 0
#^Offset year is 2
mod_missile_warhead = 0
#^Offset year is 2
missie_ground_ground = 0
#^Offset year is 2
missie_sea_sea = 0
#^Offset year is 2
missie_ground_air = 0
#^Offset year is 2
railgun_sea = 0
#^Offset year is 2
railgun_land = 0
#^Offset year is 2


blue_water_doctrine = 0
carrier_aircraft_crew_training = 0
#^Offset year is 3
carrier_battlegroups = 0
#^Offset year is 3
carrier_crew_training = 0
#^Offset year is 3
destroyer_tactics = 0
#^Offset year is 3
green_water_doctrine = 0
small_ship_tactics = 0
#^Offset year is 3
corvette_crew_training = 0
#^Offset year is 3
frigate_crew_training = 0
#^Offset year is 3
basing_doctrine = 0
#^Offset year is 3
fire_control_systems = 0
#^Offset year is 3
submarine_warfare_doctrine = 0
anti_submarine_tactics = 0
#^Offset year is 3
submarine_crew_training = 0
#^Offset year is 3
convoy_raiding_tactics = 0
#^Offset year is 3
submarine_combat_tactics = 0
#^Offset year is 3
anti_submarine_patrols = 0
#^Offset year is 3
Amphibious_invasion_doctrine = 0
Exp_Strike_bgrps = 0
#^Offset year is 3
cruiser_tactics = 0
#^Offset year is 3
Assult_ship_tactics = 0
#^Offset year is 3


uav = 0
spydrones = 0
military_drone = 0
#^Offset year is 5
satellite_theory = 0
satellite_design = 0
launch_facilities = 0
first_satellite = 0
communications_satellite = 0
espionage_satellite = 0
military_satellite = 0
stealth_systems = 0
small_craft_stealth = 0
stealth_fighters = 0
#^Offset year is 5
stealth_bombers = 0
#^Offset year is 5
afv_infrared_coverage = 0
adaptive_camouflage = 0
robotics = 0
railgun_research = 0


small_Cannon = 0
#^Offset year is 2
Small_arms_AA = 0
#^Offset year is 2
Torpedos = 0
#^Offset year is 2
Helo_pad_intergration = 0
#^Offset year is 2
Helo_ASW_role = 0
#^Offset year is 2
helo_scout_role = 0
#^Offset year is 2
Small_gas_turbine_engine = 0
#^Offset year is 2
patrol_boats = 0
patrol_boat_armour = 0
#^Offset year is 4
corvettes = 0
corvette_armour = 0
#^Offset year is 4
frigate = 0
frigate_armour = 0
#^Offset year is 4
submarines = 0
submarine_engine = 0
#^Offset year is 2
submarine_hull = 0
#^Offset year is 2
submarine_sonar = 0
#^Offset year is 2
Nuke_sub_propulsion = 0
#^Offset year is 2
landing_craft = 0
landing_tactics = 0
#^Offset year is 2
landing_equipment = 0
#^Offset year is 2
#^Offset year is 2
small_radar = 0
#^Offset year is 2
asw_depth_charges = 0
#^Offset year is 2
asw_mines = 0
#^Offset year is 2
small_sonar = 0
#^Offset year is 2


combat_medicine = 0
#^Offset year is 3
first_aid = 0
#^Offset year is 3
fertilizers = 0
#^Offset year is 2
agriculture_machinery = 0
#^Offset year is 2
industral_production = 0
#^Offset year is 2
industral_efficiency = 0
#^Offset year is 2
military_supplies_production = 0
#^Offset year is 2
education = 0
#^Offset year is 3
energy_production = 0
energy_efficiency = 0
oil_refining = 0
fuel_production = 0
rare_metal_production = 0
steel_production = 0
recycling = 0
synthetic_oil = 0
supply_transportation = 0
supply_organisation = 0
civil_defence = 0


infantry_upgrade = 4
#^Offset year is 2
infantry_brigade_activation = 1
infantry_small_arms = 7
#^Offset year is 2
infantry_heavy_weapons = 7
#^Offset year is 2
truck_engines = 1
engineer_regiment = 1
demolition_equipment = 5
#^Offset year is 2
bridging_equipment = 5
#^Offset year is 2
military_police_training = 1
security_training = 6
#^Offset year is 2
combat_training = 7
#^Offset year is 2
marine_infantry = 1
paratrooper_infantry = 1
special_forces_regiment = 1
mountain_infantry = 1
jungle_infantry = 0
special_small_arms = 8
#^Offset year is 2
special_heavy_weapons = 8
#^Offset year is 2
desert_equipment = 1
winter_equipment = 1
urban_equipment = 1
night_equipment = 1
mountain_equipment = 1
jungle_equipment = 0
forest_equipment = 1
amphibious_equipment = 1
airborne_equipment = 1
helicopter_engine = 1
helicopter_fuselage = 1
helicopter_reliability = 1
helicopter_armament = 1
air_cavalry = 1

1936.1.1 = { decision = allow_research }