theatre = {
	name = "Bulgarian Army"
	location = 4371
	regiment = { type = hq_brigade name = "Bulgarian Army HQ" historical_model = 1 }
	army = {
		name = "Bulgarian Land Forces"
		location = 4371
		regiment = { type = hq_brigade name = "Bulgarian Land Forces HQ" historical_model = 1 }
		division = {
			is_reserve = yes
			name = "Mekhanizirana Brigada na 2-ra Tundzhanska"
			location = 4312
			regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
			regiment = { type = mechanized_brigade name = "3ta Mekhaniziran Batal�on" historical_model = 0 }
		}
		division = {
			is_reserve = yes
			name = "Mekhanizirana Brigada na 2-ra Tundzhanska"
			location = 4372
			regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
			regiment = { type = mechanized_brigade name = "3ta Mekhaniziran Batal�on" historical_model = 0 }
		}
		division = {
			is_reserve = yes
			name = "Nezavisim Mekhaniziran Batal�on"
			location = 4371
			regiment = { type = mechanized_brigade name = "Nezavisim Mekhaniziran Batal�on" historical_model = 0 }
		}
		division = {
			is_reserve = yes
			name = "68mata Spetsialni Operatsii Polk"
			location = 4558
			regiment = { type = special_forces_brigade name = "1-viya Spetsialen Sili Batal�on" historical_model = 1 }
			regiment = { type = mountain_brigade name = "101-viya Alpiiski Batal�on" historical_model = 1 }
		}
		division = {
			is_reserve = yes
			name = "2-ri Spetsialen Sili Batal�on"
			location = 4435
			regiment = { type = paratrooper_brigade name = "2-ri Spetsialen Sili Batal�on" historical_model = 1 }
		}
		division = {
			is_reserve = yes
			name = "5-ti Shipchenski Pekhoten Brigada"
			location = 4371
			regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
			regiment = { type = artillery_brigade name = "4-ti Artileriiski Polk" historical_model = 0 }
			regiment = { type = engineer_brigade name = "55-tata Inzhener Polk" historical_model = 0 }
		}
	}
	
	navy = {
		name = "Voennomorski sili na Republika Balgariya"
		base = 4123
		location = 4123
		ship = { type = heavy_cruiser name = "Drazki" historical_model = 0 }
		ship = { type = heavy_cruiser name = "Verni" historical_model = 0 }
		ship = { type = heavy_cruiser name = "Gordi" historical_model = 0 }
		ship = { type = heavy_cruiser name = "Smeli" historical_model = 0 }
		ship = { type = light_cruiser name = "Reshitelni" historical_model = 0 }
		ship = { type = light_cruiser name = "Bodri" historical_model = 0 }
		ship = { type = light_cruiser name = "Mulniya" historical_model = 0 }
		ship = { type = patrol_boat name = "Osa I" historical_model = 0 }
		ship = { type = patrol_boat name = "Osa II" historical_model = 0 }
		ship = { type = patrol_boat name = "Osa III" historical_model = 0 }
		ship = { type = patrol_boat name = "Briz" historical_model = 0 }
		ship = { type = patrol_boat name = "Shkval" historical_model = 0 }
		ship = { type = patrol_boat name = "Priboi" historical_model = 0 }
		ship = { type = invasion_transport_ship name = "Polnocny I" historical_model = 0 }
	}
	
	air = {
		name = "1/3 & 2/3 Eskadrila"
		base = 4435
		location = 4435
		wing = { type = multi_role name = "1/3 & 2/4 Eskadrila" historical_model = 0 }
	}
	
	air = { 
		name = "1/22 Ataka Eskadrila"
		base = 4248
		location = 4248
		wing = { type = cas name = "1/22 Ataka Eskadrila" historical_model = 0 }
	}
}
	