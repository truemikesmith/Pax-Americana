color = { 64  250  88 }
graphical_culture = Generic
last_election = 2000.12.7
duration = 48

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"al-Mushati al-1i" "al-Mushati al-2i" "al-Mushati al-3i" "al-Mushati al-4i" "al-Mushati al-5i" "al-Mushati al-6i" "al-Mushati al-7i" "al-Mushati al-8i" "al-Mushati al-9i" "al-Mushati al-10i" "al-Mushati al-12r" 
		"al-Mushati al-13r" "al-Mushati al-14r" "al-Mushati al-15r" 
	}
	cavalry_brigade = {
		"Sipahis al-1i" "Sipahis al-2i" "Sipahis al-3i" "Sipahis al-4i" "Sipahis al-5i" "Sipahis al-6i" "Sipahis al-7i" "Sipahis al-8i" "Sipahis al-9i" "Sipahis al-10i" "Sipahis al-11r" 
		"Sipahis al-12r" "Sipahis al-13r" "Sipahis al-14r" "Sipahis al-15r" 
	}
	motorized_brigade = {
		"al-Mushati al-1i" "al-Mushati al-2i" "al-Mushati al-3i" "al-Mushati al-4i" "al-Mushati al-5i" "al-Mushati al-6i" "al-Mushati al-7i" "al-Mushati al-8i" "al-Mushati al-9i" "al-Mushati al-10i" "al-Mushati al-12r" 
		"al-Mushati al-13r" "al-Mushati al-14r" "al-Mushati al-15r" "al-Mushati al-16r" "al-Mushati al-17r" "al-Mushati al-18r" "al-Mushati al-19r" "al-Mushati al-20r" 
	}
	mechanized_brigade = {
		"al-Mushati al-1i" "al-Mushati al-2i" "al-Mushati al-3i" "al-Mushati al-4i" "al-Mushati al-5i" "al-Mushati al-6i" "al-Mushati al-7i" "al-Mushati al-8i" "al-Mushati al-9i" "al-Mushati al-10i" "al-Mushati al-12r" 
		"al-Mushati al-13r" "al-Mushati al-14r" "al-Mushati al-15r" "al-Mushati al-16r" "al-Mushati al-17r" "al-Mushati al-18r" "al-Mushati al-19r" "al-Mushati al-20r" 
	}
	light_armor_brigade = {
		"Sipahis al-1i" "Sipahis al-2i" "Sipahis al-3i" "Sipahis al-4i" "Sipahis al-5i" "Sipahis al-6i" "Sipahis al-7i" "Sipahis al-8i" "Sipahis al-9i" "Sipahis al-10i" 
	}
	armor_brigade = {
		"Sipahis al-1i" "Sipahis al-2i" "Sipahis al-3i" "Sipahis al-4i" "Sipahis al-5i" "Sipahis al-6i" "Sipahis al-7i" "Sipahis al-8i" "Sipahis al-9i" "Sipahis al-10i" 
	}
	paratrooper_brigade = {
		"al-Mushati al-20r" "al-Mushati al-21r" "al-Mushati al-22r" "al-Mushati al-23r" "al-Mushati al-24r" "al-Mushati al-25r" "al-Mushati al-26r" "al-Mushati al-27r" "al-Mushati al-28r" "al-Mushati al-29r" "al-Mushati al-30r" 
			}
	marine_brigade = {
		"al-Mushati al-20r" "al-Mushati al-21r" "al-Mushati al-22r" "al-Mushati al-23r" "al-Mushati al-24r" "al-Mushati al-25r" "al-Mushati al-26r" "al-Mushati al-27r" "al-Mushati al-28r" "al-Mushati al-29r" "al-Mushati al-30r" 
			}
	bergsjaeger_brigade = {
		"al-Mushati al-20r" "al-Mushati al-21r" "al-Mushati al-22r" "al-Mushati al-23r" "al-Mushati al-24r" "al-Mushati al-25r" "al-Mushati al-26r" "al-Mushati al-27r" "al-Mushati al-28r" "al-Mushati al-29r" "al-Mushati al-30r" 
			}
	garrison_brigade = {
		"al-Mushati al-12r" "al-Mushati al-13r" "al-Mushati al-14r" "al-Mushati al-15r" "al-Mushati al-16r" "al-Mushati al-17r" "al-Mushati al-18r" "al-Mushati al-19r" "al-Mushati al-20r" 
	}
	hq_brigade = {
		"1st Egyptian Army" "2nd Egyptian Army" "3rd Egyptian Army" "4th Egyptian Army" "5th Egyptian Army" 
	}
	militia_brigade = {
		"al-Milishiya al-1i" "al-Milishiya al-2i" "al-Milishiya al-3i" "al-Milishiya al-4i" "al-Milishiya al-5i" "al-Milishiya al-6i" "al-Milishiya al-7i" "al-Milishiya al-8i" "al-Milishiya al-9i" "al-Milishiya al-10i" "al-Milishiya al-11r" 
		"al-Milishiya al-12r" "al-Milishiya al-13r" "al-Milishiya al-14r" "al-Milishiya al-15r" "al-Milishiya al-16r" "al-Milishiya al-17r" "al-Milishiya al-18r" "al-Milishiya al-19r" "al-Milishiya al-20r" "al-Milishiya al-21r" "al-Milishiya al-22r" 
		"al-Milishiya al-23r" "al-Milishiya al-24r" "al-Milishiya al-25r" 
	}
	multi_role = {
		"1. REAF Fighter Group" "2. REAF Fighter Group" "3. REAF Fighter Group" "4. REAF Fighter Group" "5. REAF Fighter Group" "6. REAF Fighter Group" "7. REAF Fighter Group" "8. REAF Fighter Group" "9. REAF Fighter Group" "10. REAF Fighter Group" 
	}
	interceptor = {
		"1. REAF Fighter Group" "2. REAF Fighter Group" "3. REAF Fighter Group" "4. REAF Fighter Group" "5. REAF Fighter Group" "6. REAF Fighter Group" "7. REAF Fighter Group" "8. REAF Fighter Group" "9. REAF Fighter Group" "10. REAF Fighter Group" "1. REAF Fighter Group" 
		"2. REAF Fighter Group" "3. REAF Fighter Group" "4. REAF Fighter Group" "5. REAF Fighter Group" "6. REAF Fighter Group" "7. REAF Fighter Group" "8. REAF Fighter Group" "9. REAF Fighter Group" "10. REAF Fighter Group" 
	}
	strategic_bomber = {
		"11. REAF Strategic Bomb Group" "12. REAF Strategic Bomb Group" "13. REAF Strategic Bomb Group" "14. REAF Strategic Bomb Group" "15. REAF Strategic Bomb Group" "16. REAF Strategic Bomb Group" 
	}
	tactical_bomber = {
		"21. REAF Tactical Bomb Group" "22. REAF Tactical Bomb Group" "23. REAF Tactical Bomb Group" "24. REAF Tactical Bomb Group" "25. REAF Tactical Bomb Group" "26. REAF Tactical Bomb Group" 
	}
	naval_bomber = {
		"41. REAF Marine Bomb Group" "42. REAF Marine Bomb Group" "43. REAF Marine Bomb Group" "44. REAF Marine Bomb Group" "45. REAF Marine Bomb Group" "51. REAF Torpedo Group" "52. REAF Torpedo Group" "53. REAF Torpedo Group" "54. REAF Torpedo Group" "55. REAF Torpedo Group" "56. REAF Torpedo Group" 
			}
	cas = {
		"31. REAF Dive Bomb Group" "32. REAF Dive Bomb Group" "33. REAF Dive Bomb Group" "34. REAF Dive Bomb Group" "35. REAF Dive Bomb Group" 
	}
	transport_plane = {
		"61. REAF Airtransport" "62. REAF Airtransport" "63. REAF Airtransport" "64. REAF Airtransport" 
	}
	battleship = {
		"ENS Nasr" "ENS al-Qahira" "ENS Abu Simbel" "ENS Aswan" "ENS Nijmi Shevket" "ENS Assari Shevket" "ENS Lutfi Djelil" "ENS Mehemet Ali" "ENS Ibrahim" "ENS Sakka" 
	}
	light_cruiser = {
		"ENS Bur Safajah" "ENS Al Ghardaqah" "ENS Bur Tawfiq" "ENS Marsa Matruh" "ENS Al Iskandariyah" "ENS Najim al Zafir" "ENS El Aboukir" "ENS El Suez" "ENS Rasheed" "ENS Damyat" "ENS Sharm El-Sheikh" 
		"ENS Toushka" 
	}
	heavy_cruiser = {
		"ENS Bur Safajah" "ENS Al Ghardaqah" "ENS Bur Tawfiq" "ENS Marsa Matruh" "ENS Al Iskandariyah" "ENS Najim al Zafir" "ENS El Aboukir" "ENS El Suez" "ENS Rasheed" "ENS Damyat" "ENS Sharm El-Sheikh" 
		"ENS Toushka" "ENS Nasr" 
	}
	battlecruiser = {
		"ENS Bur Safajah" "ENS Al Ghardaqah" "ENS Bur Tawfiq" "ENS Marsa Matruh" "ENS Al Iskandariyah" "ENS Najim al Zafir" "ENS El Aboukir" "ENS El Suez" "ENS Rasheed" "ENS Damyat" "ENS Sharm El-Sheikh" 
		"ENS Toushka" "ENS Nasr" "ENS al-Qahira" "ENS Abu Simbel" "ENS Aswan" 
	}
	destroyer = {
		"Flotille Al Bi'ra" "Flotille Tabuk" "Flotille Aynunah" "Flotille Dal'at al Akhdar" "Flotille Al Qalibah" "Flotille Ziba" "Flotille Al Wajh" "Flotille Hanak" "Flotille Khuff" "Flotille Umm Lajj" 
	}
	carrier = {
		"ENS Muhammad 'Ali" "ENS Salah al-Din" "ENS Ramses" 
	}
	escort_carrier = {
		"ENS Cairo" "ENS Alexandria" "ENS Port Said" "ENS Suez" "ENS Tanta" 
	}
	submarine = {
		"Ustul al-Ghawwasati al-1i" "Ustul al-Ghawwasati al-2i" "Ustul al-Ghawwasati al-3i" "Ustul al-Ghawwasati al-4i" "Ustul al-Ghawwasati al-5i" "Ustul al-Ghawwasati al-6i" "Ustul al-Ghawwasati al-7i" "Ustul al-Ghawwasati al-8i" "Ustul al-Ghawwasati al-9i" "Ustul al-Ghawwasati al-10i" 
	}
	transport_ship = {
		"Ustul al-Hamli al-1i" "Ustul al-Hamli al-2i" "Ustul al-Hamli al-3i" "Ustul al-Hamli al-4i" "Ustul al-Hamli al-5i" "Ustul al-Hamli al-6i" "Ustul al-Hamli al-7i" "Ustul al-Hamli al-8i" "Ustul al-Hamli al-9i" "Ustul al-Hamli al-10i" 
	}

}


ministers = {
	
}
