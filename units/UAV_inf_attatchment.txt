uav_brigade = {
	type = land
	sprite = Infantry
	active = no
	unit_group = recon_unit_type
	is_mobile = yes

	#Size Definitions
	max_strength = 8
	default_organisation = 30
	default_morale = 0.3
	officers = 10

	#Building Costs
	build_cost_ic = 1.8
	build_cost_manpower = 1.4
	build_time = 52
	repair_cost_multiplier = 0.07

	#Misc Abilities
	maximum_speed = 10
	transport_weight = 3.00
	supply_consumption = 0.1
	fuel_consumption = 0
	radio_strength = 2

	#Defensive Abilities
	defensiveness = 0.1	# was 3.33
	toughness = 2
	softness = 0.5
	air_defence = 2.5
	armor_value = 0.5

	#Offensive Abilities
	suppression = 3
	soft_attack = 0.85
	hard_attack = 0.25
	air_attack = 2.25
	ap_attack = 0.5

	plains = {
		movement = 0.1
	}
    river = {
	}
	amphibious = {
	}
	urban = {
		movement = 0.1
	}
	arctic ={
	}
	desert = {
		movement = 0.1
	}
	woods = {
		movement = 0.3
	}
	forest = {
		movement = 0.3
	}
	jungle = {
		movement = 0.1
	}
	hills = {
		movement = 0.3
	}
	mountain = {
		movement = 0.15
	}
   	marsh = { 			
		movement = 0.15
	}
	night = {
	}
	fort = { }
	
	combat_width = 0.2

	completion_size = 0.2
	on_completion = mobile_practical
	
	priority = 8
}
