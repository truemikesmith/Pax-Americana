154001 = {
	name = "Gwebu"
	country = SWA
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L154001
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154002 = {
	name = "Mabila"
	country = SWA
	type = land
	skill = 0
	max_skill = 1
	loyalty = 0.00
	picture = L154002
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154003 = {
	name = "Ntshalintshali"
	country = SWA
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L154003
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154004 = {
	name = "Maziya"
	country = SWA
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L154004
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154005 = {
	name = "Gamedze"
	country = SWA
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L154005
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154006 = {
	name = "Nhleko"
	country = SWA
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L154006
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
154007 = {
	name = "Mswati III"
	country = SWA
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L154007
	add_trait = logistics_wizard
	add_trait = engineer
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
154008 = {
	name = "Swane"
	country = SWA
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L154008
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
154009 = {
	name = "Tshabalala"
	country = SWA
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L154009
	add_trait = defensive_doctrine
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
154010 = {
	name = "Lukhele"
	country = SWA
	type = air
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L154010
	add_trait = superior_air_tactician
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
