
local P = {}
AI_RUS = P

function P.DiploScore_Alliance(voDiploScoreObj)
	local lsTargetTag = voDiploScoreObj.TargetTag
	if lsTargetTag == 'SYR' then -- we got better plans for you...
			voDiploScoreObj.Score = 200
		end
	return voDiploScoreObj.Score
end

function P.ForeignMinister_Influence(voForeignMinisterData)
	local laIgnoreWatch -- Ignore this country but monitor them if they are about to join someone else
	local laWatch -- Monitor them and also fi their score is high enough they can be influenced normally
	local laIgnore -- Ignore them completely

	if voForeignMinisterData.FactionName == "axis" then
	
		laWatch = {
		}
			
		laIgnoreWatch = {
		}
			
		laIgnore = {
			"ISS"
		} -- Vichy
	end
	
	return laWatch, laIgnoreWatch, laIgnore
end


-- Return:
return AI_RUS

