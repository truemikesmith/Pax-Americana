diplomatic_decisions = {


	ignore_ISS = { #start of game
	
		potential = {
			exists = ISS
			not = {
				has_country_flag = ignore_ISS
			}
			tag = TUR
			ISS = {war_with = SYR}
	   }

		allow = {
			tag = TUR
		}

		effect = {
			TUR = {
				relation = {who = ISS value = 300}
				crude_oil = 7000
				set_country_flag = ignore_ISS
			}
			ISS = {
				money = 100
				supplies = 3000
			}
			
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	Fund_syrian_rebels = { 

		potential = {
			exists = ISS
			not = {
				has_country_flag = fund_XSY
			}
			tag = TUR
	   }

		allow = {
			tag = TUR
		}

		effect = {
			TUR = {
				supplies = -4000
				set_country_flag = fund_XSY
			}
			XSY = {
				supplies = 4000
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	
	
}