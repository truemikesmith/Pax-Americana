color = { 183  148  145 }
graphical_culture = Generic
last_election = 1997.4.27
duration = 72

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"Yemeni al-Mushati al-1i" "Yemeni al-Mushati al-2i" "Yemeni al-Mushati al-3i" "Yemeni al-Mushati al-4i" "Yemeni al-Mushati al-5i" "Yemeni al-Mushati al-6i" "Yemeni al-Mushati al-7i" "Yemeni al-Mushati al-8i" "Yemeni al-Mushati al-9i" "Yemeni al-Mushati al-10i" "Yemeni al-Mushati al-12r" 
		"Yemeni al-Mushati al-13r" "Yemeni al-Mushati al-14r" "Yemeni al-Mushati al-15r" "Yemeni al-Mushati al-16r" "Yemeni al-Mushati al-17r" "Yemeni al-Mushati al-18r" "Yemeni al-Mushati al-19r" "Yemeni al-Mushati al-20r" 
	}
	cavalry_brigade = {
		"Yemeni Sipahis al-1i" "Yemeni Sipahis al-2i" "Yemeni Sipahis al-3i" "Yemeni Sipahis al-4i" "Yemeni Sipahis al-5i" "Yemeni Sipahis al-6i" "Yemeni Sipahis al-7i" "Yemeni Sipahis al-8i" "Yemeni Sipahis al-9i" "Yemeni Sipahis al-10i" "Yemeni Sipahis al-11r" 
		"Yemeni Sipahis al-12r" "Yemeni Sipahis al-13r" "Yemeni Sipahis al-14r" "Yemeni Sipahis al-15r" 
	}
	motorized_brigade = {
		"Yemeni al-Mushati al-1i" "Yemeni al-Mushati al-2i" "Yemeni al-Mushati al-3i" "Yemeni al-Mushati al-4i" "Yemeni al-Mushati al-5i" "Yemeni al-Mushati al-6i" "Yemeni al-Mushati al-7i" "Yemeni al-Mushati al-8i" "Yemeni al-Mushati al-9i" "Yemeni al-Mushati al-10i" "Yemeni al-Mushati al-12r" 
		"Yemeni al-Mushati al-13r" "Yemeni al-Mushati al-14r" "Yemeni al-Mushati al-15r" "Yemeni al-Mushati al-16r" "Yemeni al-Mushati al-17r" "Yemeni al-Mushati al-18r" "Yemeni al-Mushati al-19r" "Yemeni al-Mushati al-20r" 
	}
	mechanized_brigade = {
		"Yemeni al-Mushati al-1i" "Yemeni al-Mushati al-2i" "Yemeni al-Mushati al-3i" "Yemeni al-Mushati al-4i" "Yemeni al-Mushati al-5i" "Yemeni al-Mushati al-6i" "Yemeni al-Mushati al-7i" "Yemeni al-Mushati al-8i" "Yemeni al-Mushati al-9i" "Yemeni al-Mushati al-10i" "Yemeni al-Mushati al-12r" 
		"Yemeni al-Mushati al-13r" "Yemeni al-Mushati al-14r" "Yemeni al-Mushati al-15r" "Yemeni al-Mushati al-16r" "Yemeni al-Mushati al-17r" "Yemeni al-Mushati al-18r" "Yemeni al-Mushati al-19r" "Yemeni al-Mushati al-20r" 
	}
	light_armor_brigade = {
		"Yemeni Sipahis al-1i" "Yemeni Sipahis al-2i" "Yemeni Sipahis al-3i" "Yemeni Sipahis al-4i" "Yemeni Sipahis al-5i" "Yemeni Sipahis al-6i" "Yemeni Sipahis al-7i" "Yemeni Sipahis al-8i" "Yemeni Sipahis al-9i" "Yemeni Sipahis al-10i" 
	}
	armor_brigade = {
		"Yemeni Sipahis al-1i" "Yemeni Sipahis al-2i" "Yemeni Sipahis al-3i" "Yemeni Sipahis al-4i" "Yemeni Sipahis al-5i" "Yemeni Sipahis al-6i" "Yemeni Sipahis al-7i" "Yemeni Sipahis al-8i" "Yemeni Sipahis al-9i" "Yemeni Sipahis al-10i" 
	}
	paratrooper_brigade = {
		"Yemeni al-Mushati al-20r" "Yemeni al-Mushati al-21r" "Yemeni al-Mushati al-22r" "Yemeni al-Mushati al-23r" "Yemeni al-Mushati al-24r" "Yemeni al-Mushati al-25r" "Yemeni al-Mushati al-26r" "Yemeni al-Mushati al-27r" "Yemeni al-Mushati al-28r" "Yemeni al-Mushati al-29r" "Yemeni al-Mushati al-30r" 
			}
	marine_brigade = {
		"Yemeni al-Mushati al-20r" "Yemeni al-Mushati al-21r" "Yemeni al-Mushati al-22r" "Yemeni al-Mushati al-23r" "Yemeni al-Mushati al-24r" "Yemeni al-Mushati al-25r" "Yemeni al-Mushati al-26r" "Yemeni al-Mushati al-27r" "Yemeni al-Mushati al-28r" "Yemeni al-Mushati al-29r" "Yemeni al-Mushati al-30r" 
			}
	bergsjaeger_brigade = {
		"Yemeni al-Mushati al-20r" "Yemeni al-Mushati al-21r" "Yemeni al-Mushati al-22r" "Yemeni al-Mushati al-23r" "Yemeni al-Mushati al-24r" "Yemeni al-Mushati al-25r" "Yemeni al-Mushati al-26r" "Yemeni al-Mushati al-27r" "Yemeni al-Mushati al-28r" "Yemeni al-Mushati al-29r" "Yemeni al-Mushati al-30r" 
			}
	garrison_brigade = {
		"Yemeni al-Mushati al-1i" "Yemeni al-Mushati al-2i" "Yemeni al-Mushati al-3i" "Yemeni al-Mushati al-4i" "Yemeni al-Mushati al-5i" "Yemeni al-Mushati al-6i" "Yemeni al-Mushati al-7i" "Yemeni al-Mushati al-8i" "Yemeni al-Mushati al-9i" "Yemeni al-Mushati al-10i" "Yemeni al-Mushati al-12r" 
		"Yemeni al-Mushati al-13r" "Yemeni al-Mushati al-14r" "Yemeni al-Mushati al-15r" "Yemeni al-Mushati al-16r" "Yemeni al-Mushati al-17r" "Yemeni al-Mushati al-18r" "Yemeni al-Mushati al-19r" "Yemeni al-Mushati al-20r" 
	}
	hq_brigade = {
		"1st Yemeni Army" "2nd Yemeni Army" "3rd Yemeni Army" "4th Yemeni Army" "5th Yemeni Army" 
	}
	militia_brigade = {
		"Yemeni al-Milishiya al-1i" "Yemeni al-Milishiya al-2i" "Yemeni al-Milishiya al-3i" "Yemeni al-Milishiya al-4i" "Yemeni al-Milishiya al-5i" "Yemeni al-Milishiya al-6i" "Yemeni al-Milishiya al-7i" "Yemeni al-Milishiya al-8i" "Yemeni al-Milishiya al-9i" "Yemeni al-Milishiya al-10i" "Yemeni al-Milishiya al-11r" 
		"Yemeni al-Milishiya al-12r" "Yemeni al-Milishiya al-13r" "Yemeni al-Milishiya al-14r" "Yemeni al-Milishiya al-15r" "Yemeni al-Milishiya al-16r" "Yemeni al-Milishiya al-17r" "Yemeni al-Milishiya al-18r" "Yemeni al-Milishiya al-19r" "Yemeni al-Milishiya al-20r" "Yemeni al-Milishiya al-21r" "Yemeni al-Milishiya al-22r" 
		"Yemeni al-Milishiya al-23r" "Yemeni al-Milishiya al-24r" "Yemeni al-Milishiya al-25r" 
	}
	multi_role = {
		"1. RYAF Fighter Group" "2. RYAF Fighter Group" "3. RYAF Fighter Group" "4. RYAF Fighter Group" "5. RYAF Fighter Group" "6. RYAF Fighter Group" "7. RYAF Fighter Group" "8. RYAF Fighter Group" "9. RYAF Fighter Group" "10. RYAF Fighter Group" 
	}
	interceptor = {
		"1. RYAF Fighter Group" "2. RYAF Fighter Group" "3. RYAF Fighter Group" "4. RYAF Fighter Group" "5. RYAF Fighter Group" "6. RYAF Fighter Group" "7. RYAF Fighter Group" "8. RYAF Fighter Group" "9. RYAF Fighter Group" "10. RYAF Fighter Group" "1. RYAF Fighter Group" 
		"2. RYAF Fighter Group" "3. RYAF Fighter Group" "4. RYAF Fighter Group" "5. RYAF Fighter Group" "6. RYAF Fighter Group" "7. RYAF Fighter Group" "8. RYAF Fighter Group" "9. RYAF Fighter Group" "10. RYAF Fighter Group" 
	}
	strategic_bomber = {
		"11. RYAF Strategic Bomb Group" "12. RYAF Strategic Bomb Group" "13. RYAF Strategic Bomb Group" "14. RYAF Strategic Bomb Group" "15. RYAF Strategic Bomb Group" "16. RYAF Strategic Bomb Group" 
	}
	tactical_bomber = {
		"21. RYAF Tactical Bomb Group" "22. RYAF Tactical Bomb Group" "23. RYAF Tactical Bomb Group" "24. RYAF Tactical Bomb Group" "25. RYAF Tactical Bomb Group" "26. RYAF Tactical Bomb Group" 
	}
	naval_bomber = {
		"41. RYAF Marine Bomb Group" "42. RYAF Marine Bomb Group" "43. RYAF Marine Bomb Group" "44. RYAF Marine Bomb Group" "45. RYAF Marine Bomb Group" "51. RYAF Torpedo Group" "52. RYAF Torpedo Group" "53. RYAF Torpedo Group" "54. RYAF Torpedo Group" "55. RYAF Torpedo Group" "56. RYAF Torpedo Group" 
			}
	cas = {
		"31. RYAF Dive Bomb Group" "32. RYAF Dive Bomb Group" "33. RYAF Dive Bomb Group" "34. RYAF Dive Bomb Group" "35. RYAF Dive Bomb Group" 
	}
	transport_plane = {
		"61. RYAF Airtransport" "62. RYAF Airtransport" "63. RYAF Airtransport" "64. RYAF Airtransport" 
	}
	battleship = {
		"RYNS San'a" "RYNS Al Hudaydah" 
	}
	light_cruiser = {
		"RYNS Salalah" "RYNS Raysut" "RYNS Mirbah" "RYNS Sawqirah" 
	}
	heavy_cruiser = {
		"RYNS Al Mukha" "RYNS Zabid" "RYNS Huth" "RYNS Sa'dah" "RYNS Ma'rib" 
	}
	battlecruiser = {
		"RYNS al-Qahira" "RYNS Salah al-Din" "RYNS Mehemet Ali" 
	}
	destroyer = {
		"RYN Torpedo Flotilla no. 1" "RYN Torpedo Flotilla no. 2" "RYN Torpedo Flotilla no. 3" "RYN Torpedo Flotilla no. 4" "RYN Torpedo Flotilla no. 5" "RYN Torpedo Flotilla no. 6" "RYN Torpedo Flotilla no. 7" "RYN Torpedo Flotilla no. 8" "RYN Torpedo Flotilla no. 9" "RYN Torpedo Flotilla no. 10" 
	}
	carrier = {
		"RYNS Al Yaman" "RYNS Ta'izz" 
	}
	submarine = {
		"RYN Ustul al-Ghawwasati al-1i" "RYN Ustul al-Ghawwasati al-2i" "RYN Ustul al-Ghawwasati al-3i" "RYN Ustul al-Ghawwasati al-4i" "RYN Ustul al-Ghawwasati al-5i" "RYN Ustul al-Ghawwasati al-6i" "RYN Ustul al-Ghawwasati al-7i" "RYN Ustul al-Ghawwasati al-8i" "RYN Ustul al-Ghawwasati al-9i" "RYN Ustul al-Ghawwasati al-10i" 
	}
	transport_ship = {
		"RYN Ustul al-Hamli al-1i" "RYN Ustul al-Hamli al-2i" "RYN Ustul al-Hamli al-3i" "RYN Ustul al-Hamli al-4i" "RYN Ustul al-Hamli al-5i" "RYN Ustul al-Hamli al-6i" "RYN Ustul al-Hamli al-7i" "RYN Ustul al-Hamli al-8i" "RYN Ustul al-Hamli al-9i" "RYN Ustul al-Hamli al-10i" 
	}

}

ministers = {
	78503 = {
		name = "Abd Rabbuh Mansur Hadi"
		ideology = paternal_autocrat
		loyalty = 1.00
		picture = M_YEM1
		head_of_state = majestic
		start_date = 2000.1.1
	}
}
