garrison_detachment = {
	type = land
	sprite = Infantry
	#unit_group = infantry_unit_type

	available_trigger = {
	not = { has_country_flag =  colonial_units }
	}
	#Size Definitions
	max_strength = 8
	default_organisation = 30
	default_morale = 0.35
	officers = 15

	#Building Costs
	build_cost_ic = 1
	build_cost_manpower = 1.75
	build_time = 82
	repair_cost_multiplier = 0.03
	
	#Misc Abilities
	maximum_speed = 5
	transport_weight = 1000.00
	supply_consumption = 0.15
	fuel_consumption = 0.00
	radio_strength = 2

	#Defensive Abilities
	defensiveness = 5	# was 5.00
	toughness = 0.4
	softness = 1
	air_defence = 3

	#Offensive Abilities
	suppression = 6.00
	soft_attack = 1
	hard_attack = 0.165
	air_attack = 3.5

	plains = {
		movement = -0.35
	}
	river = {
		attack = -0.1
		defence = 0.2
		movement = -0.35
	}
	amphibious =    { 
		
		defence = 0.2	
	}
	urban = { 
		attack = 0.1
		defence = 0.6
		movement = -0.35
	}
	arctic = {
		attack = -0.05		
		defence = 0.1 
		movement = -0.35
	}
	desert = {
		movement = -0.35
	}
	woods = { 
		attack = 0.15
		defence = 0.2
		movement = -0.35
	}
	forest = { 
		attack = 0.15
		defence = 0.2 
		movement = -0.45
	}
	jungle = { 
		attack = 0.1
		defence = 0.2 
		movement = -0.7
	}
	hills = {
		attack = 0.1
		defence = 0.2
		movement = -0.35
	}
	mountain = { 
		attack = 0.05
		defence = 0.2
		movement = -0.55
	}
	marsh = { 
		attack = -0.05
		defence = -0.05
		movement = -0.9
	}
	fort = {
		attack = 0.05
		defence = 0.8
	}
	
	combat_width = 1

	completion_size = 0.2
	on_completion = militia_practical
	
	priority = 48
}
