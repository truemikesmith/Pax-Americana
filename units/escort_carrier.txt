escort_carrier = {
	type = naval
	sprite = Carrier
	active = no
	
	hull = 0.5
	capital = yes	

	#Size Definitions
	max_strength = 100
	default_organisation = 15  ## was 20
	default_morale = 0.30
	officers = 300

	carrier_size = 0

	#Building Costs
	build_cost_ic = 5
	build_cost_manpower = 1.5
	build_time = 320
	repair_cost_multiplier = 0.15

	#Misc Abilities
	maximum_speed = 15.00
	transport_capability = 0  ## was 0.00
	supply_consumption = 1.60
	fuel_consumption = 1.20
	range = 3000
	distance = 1.00
	radio_strength = 1.0
	positioning = 0.1

	#Detection Abilities
	surface_detection = 2
	air_detection = 2.00
	sub_detection = 5.00
	visibility =50
	
	#Defensive Abilities
	sea_defence = 2.00
	air_defence = 12.00

	#Offensive Abilities
	convoy_attack = 0.00
	sea_attack = 0.00
	sub_attack = 5.00
	air_attack = 0.00
	shore_bombardment = 0.00

	completion_size = 1.5
	on_completion = carrier_practical
	priority = 25
}
