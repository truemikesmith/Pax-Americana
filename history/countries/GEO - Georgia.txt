capital = 3254
government = autocratic_democracy
ideology = social_conservative
alignment = { x = 0 y = 30 }
neutrality = 100
national_unity = 70

oob = "GEO_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 18
	market_liberal = 19
	social_liberal = 7
	social_democrat = 12
	left_wing_radical = 0
	leninist = 21
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 54
	market_liberal = 57
	social_liberal = 21
	social_democrat = 36
	left_wing_radical = 0
	leninist = 63
	stalinist = 0
}

conscription_policy = military_service
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
service_length = eighteen_months
economic_policy = limit_intervention
healthcare = public_care

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
military_police_training = 1
civil_defence = 1
supply_transportation = 1
supply_organisation = 1
infra_building = 1
ic_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
special_forces_regiment = 1
special_small_arms = 1
special_anti_tank = 1
special_support_weapons = 1
special_heavy_weapons = 1
superior_firepower = 1

1936.1.1 = { decision = allow_research }