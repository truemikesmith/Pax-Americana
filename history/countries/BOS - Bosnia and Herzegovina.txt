capital = 4174
government = federation
ideology = social_conservative
alignment = { x = 0 y = -20 }
neutrality = 100
national_unity = 70

oob = "BOS_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 9
	social_conservative = 19
	market_liberal = 1
	social_liberal = 9
	social_democrat = 9
	left_wing_radical = 9
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 27
	social_conservative = 57
	market_liberal = 3
	social_liberal = 27
	social_democrat = 27
	left_wing_radical = 27
	leninist = 0
	stalinist = 0
}

conscription_policy = military_service
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = multiple_laws
civil_laws = open_society
service_length = six_months
economic_policy = lais_fair
healthcare = public_care

infantry_brigade_activation = 1
engineer_regiment = 1
military_police_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
air_base_building = 1
land_fort_building = 1
organisation_tech = 1
superior_firepower = 1
naval_base_building = 1
coastal_fort_building = 1

1936.1.1 = { decision = allow_research }