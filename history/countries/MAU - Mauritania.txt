capital = 9701
government = autocratic_democracy
ideology = social_conservative
alignment = { x = 0 y = 50 }
neutrality = 100
national_unity = 70
oob = "MAU_2000.txt"
head_of_state = 78512
popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 51
	market_liberal = 0
	social_liberal = 8
	social_democrat = 0
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 150
	market_liberal = 0
	social_liberal = 24
	social_democrat = 0
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

conscription_policy = military_service
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = restrictions
service_length = twofour_months
economic_policy = lais_fair

infantry_brigade_activation = 1
superior_firepower = 1

1936.1.1 = { decision = allow_research }