pack_artillery_brigade = {
	type = land
	sprite = Infantry
	active = no
	unit_group = artillery_unit_type

	#Size Definitions
	max_strength = 2
	default_organisation = 30
	default_morale = 0.30
	officers = 60

	#Building Costs
	build_cost_ic = 1.5
	build_cost_manpower = 1.1
	build_time = 105
	repair_cost_multiplier = 0.05

	#Misc Abilities
	maximum_speed = 5 ##donkeys and mules
	transport_weight = 10.00
	supply_consumption = 0.25
	fuel_consumption = 0.00
	radio_strength = 1

	#Defensive Abilities
	defensiveness = 1.2
	toughness = 0.2
	softness = 0.90
	air_defence = 7

	#Offensive Abilities
	suppression = 0.00
	soft_attack = 1.5
	hard_attack = 0.3
	air_attack = 0.1
	ap_attack = 2

	plains = { 
		defence = 0.3
		attack = 0.2
		movement = -0.2
	}
	river = { 
		defence = 0.3
		attack = 0.2
		movement = -0.2
	}
	amphibious = { 
		attack = -0.05 ##no where to set up for supporting fire
	}
	urban = {
		attack = 0.2
		movement = -0.2
	}
	arctic = {
		attack = 0.1
		defence = 0.2
		movement = -0.2
	}
	desert = {
		attack = 0.2
		defence = 0.4
		movement = -0.2
	}
	woods = { 
		attack = 0.3
		defence = 0.4
		movement = -0.2
	}
	forest = { 
		attack = 0.3
		defence = 0.4
		movement = -0.25
	}
	jungle = { 
		attack = 0.3
		defence = 0.4
		movement = -0.3
	}
	hills = { 
		attack = 0.4
		defence = 0.5
		movement = -0.2
	}
	mountain = { 
		attack = 0.4
		defence = 0.5
		movement = -0.3
	}
	marsh = { 
		attack = 0.3
		defence = 0.4
		movement = -0.3
	}
	night = {
	}
	fort = { 
		attack = 0.1
		defence = 0.1
	}
	
	combat_width = 0.1

	completion_size = 0.1
	on_completion = artillery_practical
	
	priority = 17
}
