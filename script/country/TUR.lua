
local P = {}
AI_TUR = P

function P.ForeignMinister_ProposeWar(voForeignMinisterData)
 -- Utils.LUA_DEBUGOUT("TUR looking for war")
  local laWarTags = {}
  local laPeaceTags = {}
  local liTotalNeighborWars = 0
  
  local loISSTag = CCountryDataBase.GetTag("ISS")
  local loTURISSrelation = voForeignMinisterData.ministerCountry:GetRelation(loISSTag)
  
  --If not preparing for war continue
  if not(voForeignMinisterData.Strategy:IsPreparingWar()) then
  --If not at war with ISS Continue
	if not(loTURISSrelation:HasWar()) then	
		--If have isis war flag continue
		if (voForeignMinisterData.ministerCountry:GetFlags():IsFlagSet("Helping_MidEast1")) then 
			--Utils.LUA_DEBUGOUT("TUR lets war isis")
			--Execute ISIS War Check Function
			--P.ISSWarCheck(voForeignMinisterData) -- THIS IS WHAT STARTS THE WAR!~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		end	
	  end
	end
end


function P.ISSWarCheck(voForeignMinisterData)
	
	--Utils.LUA_DEBUGOUT("TUR in isis war check")
	
	--If date is between Jan and December continue
	if (voForeignMinisterData.Month >= 1 and voForeignMinisterData.Month <= 12) or vbOveride then
	
		local loISSTag = CCountryDataBase.GetTag("ISS")
		local loISSCountry = loISSTag:GetCountry()
		local lbDOW = Support.GoodToWarCheck(loISSTag, loISSCountry, voForeignMinisterData, true)
		
		--Utils.LUA_DEBUGOUT("TUR date 1-12")
		
		if lbDOW then	
		--	Utils.LUA_DEBUGOUT("TUR prep war isis")
			voForeignMinisterData.Strategy:PrepareLimitedWar(loISSTag, 100)
		end
		
		
		return lbDOW

	end
	--Utils.LUA_DEBUGOUT("TUR ISS war check false")
	return false
end
-- Return:
return AI_TUR

