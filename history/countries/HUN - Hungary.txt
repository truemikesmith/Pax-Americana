capital = 3164
government = republic
ideology = left_wing_radical
alignment = { x = 120 y = 113.5 }
neutrality = 100
national_unity = 70
join_faction = allies

oob = "HUN_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 5
	social_conservative = 39
	market_liberal = 3
	social_liberal = 7
	social_democrat = 0
	left_wing_radical = 41
	leninist = 11
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 15
	social_conservative = 117
	market_liberal = 9
	social_liberal = 21
	social_democrat = 0
	left_wing_radical = 123
	leninist = 33
	stalinist = 0
}

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = multiple_laws
civil_laws = open_society
service_length = six_months
economic_policy = lais_fair
healthcare = public_care

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
demolition_equipment = 1
bridging_equipment = 1
military_police_training = 1
security_training = 1
combat_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
nuclear_reactor_building = 1
air_base_building = 1
land_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
special_forces_regiment = 1
paratrooper_infantry = 1
special_small_arms = 1
special_heavy_weapons = 1
special_anti_tank = 1
special_support_weapons = 1
modern_blitzkrieg = 1
flanking_maneuvers = 1
shock_and_awe = 1
breakthrough_exploitation = 1
armor_tactics = 1
elastic_defence = 1
air_superiority_doctrine = 1
fighter_pilot_training = 1
fighter_groundcrew_training = 1
fighter_ground_control = 1
interception_tactics = 1

1936.1.1 = { decision = allow_research }