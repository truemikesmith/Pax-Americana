###Gulf states funding ISIS
country_event = {
	
	id = 34500
	#fire_only_once = yes
	title = EVTNAME34500
	desc = EVTDESC34500
	picture = "SAU_fund_isis"
	is_triggered_only = yes
	#major = yes
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "When will it end?"
		ai_chance = { factor = 100 }
	}
}

#Effect when Saudi Arabia Spreads wahabism to dictator
country_event = {
	
	id = 34501
	#fire_only_once = yes
	title = EVTNAME34501
	desc = EVTDESC34501
	picture = "SAU_fund_isis"
	is_triggered_only = yes
	#major = yes
	
	mean_time_to_happen =  {
		days = 10
	}
		
	option = {
		name = "Darn it"
		ai_chance = { factor = 100 }
		this = {
			popularity = -5
			national_unity = -1
			dissent = -1
			manpower = -10
		}
		
		ISS = {
			manpower = 50
			money = 50
		}
		
	}
}

#Effect when Saudi Arabia Spreads wahabism event for majors
country_event = {
	
	id = 34502
	#fire_only_once = yes
	title = EVTNAME34502
	desc = EVTDESC34502
	picture = "SAU_fund_isis"
	is_triggered_only = yes
	#major = yes
	
	mean_time_to_happen =  {
		days = 10
	}
		
	option = {
		name = "Darn it"
		this = {
			relation = { who = SAU value = -10 } 
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Clerics call for Jihad against Iran & Russia - october 2015 ISS +20k troops
country_event = {
	
	id = 34503
	fire_only_once = yes
	title = EVTNAME34503
	desc = EVTDESC34503
	picture = "SAU_fund_isis"
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = SAU
		USA = {
			has_country_flag = US_Agreed_pivot_iran
			#need more
		}
		date = 2015.10.1
	}
	
	mean_time_to_happen =  {
		days = 30
	}
		
	option = {
		name = "My enemies, enemy is my friend"
		ISS = {
			random = { 
				chance = 80 
				load_oob = zDD_ISS_3MonthRecruits.txt
			}
			supplies = 7000
			manpower = 200
		}
		SAU = {
			set_country_flag = SAU_Iran_war_1
		}
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = "No, we shouldn't."
	}
}

#Saudi Arabia executes Shia Cleric
country_event = {
	
	id = 34504
	fire_only_once = yes
	title = EVTNAME34504
	desc = EVTDESC34504
	picture = "SAU_fund_isis"
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = SAU
		USA = {
			has_country_flag = US_Agreed_pivot_iran
			has_country_flag = US_Iran_Pivot2
		}
		has_country_flag = SAU_Iran_war_1
	}
	
	mean_time_to_happen =  {
		days = 30
	}
		
	option = {
		name = "Execute him"
		PER = {
			dissent = 10
			relation = { who = SAU value = -50 } 
		}
		SAU = {
			set_country_flag = SAU_Iran_war_2
			national_unity = 6
			
			9556 = {add_province_modifier = {name = SAU_Execute_cleric duration = -1}}
			9488 = {add_province_modifier = {name = SAU_Execute_cleric duration = -1}}
			9614 = {add_province_modifier = {name = SAU_Execute_cleric duration = -1}}
			9538 = {add_province_modifier = {name = SAU_Execute_cleric duration = -1}}
			9575 = {add_province_modifier = {name = SAU_Execute_cleric duration = -1}}
			country_event = 34512
			
		}
		ai_chance = { factor = 100 }
		
	}
	
	option = {
		name = "No, we shouldn't."
	}
}

#Saudi Arabia F15SE upgrade - stage 1, need to - money
country_event = {
	
	id = 34505
	fire_only_once = yes
	title = EVTNAME34505
	desc = EVTDESC34505
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F15SE_start
		not = {has_country_flag = SAU_F25SE_stage1 }
		brigade_exists = "Saudi F-15S Wing 1"
		brigade_exists = "Saudi F-15S Wing 2"
		not = {has_country_modifier = SAU_f15se_break}
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			set_country_flag = SAU_F15SE_stage1
			remove_brigade = "Saudi F-15S Wing 1"
			remove_brigade = "Saudi F-15S Wing 2"
			load_oob = "zzd_SAUF15SE_1.txt"
			add_country_modifier = { name = SAU_f15se_break duration = 140}
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE upgrade - stage 2, need to - money
country_event = {
	
	id = 34506
	fire_only_once = yes
	title = EVTNAME34506
	desc = EVTDESC34506
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F25SE_stage1
		not = {has_country_flag = SAU_F25SE_stage2 }
		not = {has_country_modifier = SAU_f15se_break}
		brigade_exists = "Saudi F-15S Wing 3"
		brigade_exists = "Saudi F-15S Wing 4"
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			set_country_flag = SAU_F15SE_stage2
			remove_brigade = "Saudi F-15S Wing 3"
			remove_brigade = "Saudi F-15S Wing 4"
			load_oob = "zzd_SAUF15SE_2.txt"
			add_country_modifier = { name = SAU_f15se_break duration = 140}
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE upgrade - stage 3, need to - money
country_event = {
	
	id = 34507
	fire_only_once = yes
	title = EVTNAME34507
	desc = EVTDESC34507
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F25SE_stage2
		not = {has_country_flag = SAU_F25SE_stage3 }
		not = {has_country_modifier = SAU_f15se_break}
		brigade_exists = "Saudi F-15S Wing 5"
		brigade_exists = "Saudi F-15S Wing 6"
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			set_country_flag = SAU_F15SE_stage3
			remove_brigade = "Saudi F-15S Wing 5"
			remove_brigade = "Saudi F-15S Wing 6"
			load_oob = "zzd_SAUF15SE_3.txt"
			add_country_modifier = { name = SAU_f15se_break duration = 140}
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE purchase - stage 1, need to - money
country_event = {
	
	id = 34508
	fire_only_once = yes
	title = EVTNAME34508
	desc = EVTDESC34508
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F15SE_start
		date = 2016.01.01
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			load_oob = "zzd_SAUF15SE_purchase1.txt"
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE purchase - stage 2, need to - money
country_event = {
	
	id = 34509
	fire_only_once = yes
	title = EVTNAME34509
	desc = EVTDESC34509
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F15SE_start
		date = 2017.01.01
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			load_oob = "zzd_SAUF15SE_purchase2.txt"
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE purchase - stage 3, need to - money
country_event = {
	
	id = 34510
	fire_only_once = yes
	title = EVTNAME34510
	desc = EVTDESC34510
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F15SE_start
		date = 2018.01.01
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			load_oob = "zzd_SAUF15SE_purchase3.txt"
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Arabia F15SE purchase - stage 4, need to - money
country_event = {

	id = 34511
	fire_only_once = yes
	title = EVTNAME34511
	desc = EVTDESC34511
	picture = "SAU_f15SE"
	#is_triggered_only = yes
	#major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_F15SE_start
		date = 2019.01.01
		ai = no
	}
	
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Great"
		this = {
			load_oob = "zzd_SAUF15SE_purchase4.txt"
		}
		ai_chance = { factor = 100 }
		
	}
}

#Saudi Revolt after killing cleric
country_event = {

	id = 34512
	#fire_only_once = yes
	title = "test"
	desc = "test"
	picture = ""
	#is_triggered_only = yes
	major = yes
	
	trigger = {
		tag = SAU
		has_country_flag = SAU_Iran_war_2
		not = { has_country_modifier = SAU_Shia_revolts }
		#date = 2016.01.01
		#ai = no
	}
	mean_time_to_happen =  {
		days = 1
	}
		
	option = {
		name = "Uh oh"
		SAU = {
			add_country_modifier = {name = SAU_Shia_revolts duration = 120}
			random_owned = {
				limit = {has_province_modifier = SAU_Execute_cleric}
				create_revolt = 1
			}
			random_owned = {
				limit = {has_province_modifier = SAU_Execute_cleric}
				create_revolt = 1
			}
			random_owned = {
				limit = {has_province_modifier = SAU_Execute_cleric}
				create_revolt = 1
			}
			
		}
		ai_chance = { factor = 100 }
		
	}
}
