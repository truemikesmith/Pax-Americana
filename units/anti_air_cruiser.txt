anti_air_cruiser = {
	type = naval
	sprite = LightCruiser
	active = no
	hull = 1.00  ## was 1

	#Size Definitions
	max_strength = 100
	default_organisation = 30
	default_morale = 0.30
	officers = 180

	#Building Costs
	build_cost_ic = 4.00
	build_cost_manpower = 1
	build_time = 240 ## was 240
	repair_cost_multiplier = 0.15

	#Misc Abilities
	maximum_speed = 29  
	transport_capability = 0.00
	supply_consumption = 0.30
	fuel_consumption = 0.43
	range = 1500.00  ## was 2000.00
	distance = 0.15
	radio_strength = 1
	positioning = 0.15

	#Detection Abilities
	surface_detection = 0.5
	air_detection = 20.00
	sub_detection = 0.00
	visibility = 45.00

	#Defensive Abilities
	sea_defence = 1.25
	air_defence = 20

	#Offensive Abilities
	convoy_attack = 1
	sea_attack = 1.75
	sub_attack = 0.5
	air_attack = 9.00
	shore_bombardment = 0.5

	completion_size = 1.5
	on_completion = cruiser_practical
	priority = 26
}
