capital = 6433
government = commonwealth
ideology = social_liberal
alignment = { x = 0 y = 50 }
neutrality = 100
national_unity = 70
oob = "BAA_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 0
	market_liberal = 41
	social_liberal = 52
	social_democrat = 0
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 0
	market_liberal = 0
	social_liberal = 123
	social_democrat = 150
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
economic_policy = lais_fair
service_length = six_months

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
demolition_equipment = 1
bridging_equipment = 1
military_police_training = 1
security_training = 1
combat_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
asymmetrical_warfare = 1

1936.1.1 = { decision = allow_research }