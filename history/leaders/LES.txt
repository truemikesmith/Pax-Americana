91001 = {
	name = "Koenane"
	country = LES
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L91001
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91002 = {
	name = "Kholuoe"
	country = LES
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L91002
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91003 = {
	name = "Mapheelle"
	country = LES
	type = land
	skill = 0
	max_skill = 1
	loyalty = 1.00
	picture = L91003
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91004 = {
	name = "Ntobo"
	country = LES
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L91004
	add_trait = fortress_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91005 = {
	name = "Maile"
	country = LES
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L91005
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91006 = {
	name = "Lerotholi"
	country = LES
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L91006
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91007 = {
	name = "Mokhahlane"
	country = LES
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L91007
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91008 = {
	name = "Mofolo"
	country = LES
	type = land
	skill = 0
	max_skill = 1
	loyalty = 0.00
	picture = L91008
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91009 = {
	name = "Khoachele"
	country = LES
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L91009
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91010 = {
	name = "Letsie"
	country = LES
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L91010
	add_trait = improvisation
	add_trait = logistics_wizard
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
91011 = {
	name = "Houngnikpo"
	country = LES
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L91011
	add_trait = defensive_doctrine
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
91012 = {
	name = "Kebane"
	country = LES
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L91012
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
91013 = {
	name = "Janefeke"
	country = LES
	type = air
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L91013
	history = {
		2003.1.1 = { rank = 1 }
	}
}
91014 = {
	name = "Moletsane"
	country = LES
	type = air
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L91014
	add_trait = night_flyer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
