color = { 100  213  29 }
graphical_culture = Generic
last_election = 2000.10.15
duration = 48


default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
	generic_armoured = {
		armor_brigade
		motorized_brigade
		motorized_brigade
	}
	generic_cavalry = {
		cavalry_brigade
		cavalry_brigade
	}
}

unit_names = {
	infantry_brigade = {
		"Gardijska Pesadijska Divizija" "1. Cerska Pesadijska Divizija" "3. Dunavska Pesadijska Divizija" "5. Sumadijska Pesadijska Divizija" "7. Potiska Pesadijska Divizija" "8. Krajinska Pesadijska Divizija" "9. Timocka Pesadijska Divizija" "10. Bosanska Pesadijska Divizija" "12. Jadranska Pesadijska Divizija" "13. Hercegovacka Pesadijska Divizija" "15. Zetska Pesadijska Divizija" 
		"17. Vrbaska Pesadijska Divizija" "20. Bregalnicka Pesadijska Divizija" "22. Ibarska Pesadijska Divizija" "25. Vardarska Pesadijska Divizija" "27. Savska Pesadijska Divizija" "30. Osjecka Pesadijska Divizija" "31. Kosovska Pesadijska Divizija" "32. Triglavska Pesadijska Divizija" "33. Licka Pesadijska Divizija" "34. Toplicka Pesadijska Divizija" "38. Dravska Pesadijska Divizija" 
		"40. Slavonska Pesadijska Divizija" "42. Murska Pesadijska Divizija" "44. Unska Pesadijska Divizija" "46. Moravska Pesadijska Divizija" "47. Dinarska Pesadijska Divizija" "49. Sremska Pesadijska Divizija" "50. Drinska Pesadijska Divizija" "51. Pesadijska Divizija" "52. Pesadijska Divizija" "53. Pesadijska Divizija" "54. Pesadijska Divizija" 
		"55. Pesadijska Divizija" "56. Pesadijska Divizija" "57. Pesadijska Divizija" "58. Pesadijska Divizija" "59. Pesadijska Divizija" "60. Pesadijska Divizija" "61. Pesadijska Divizija" "62. Pesadijska Divizija" "63. Pesadijska Divizija" "64. Pesadijska Divizija" "65. Pesadijska Divizija" 
		"66. Pesadijska Divizija" "67. Pesadijska Divizija" "68. Pesadijska Divizija" "69. Pesadijska Divizija" "70. Pesadijska Divizija" 
	}
	cavalry_brigade = {
		"1. Konjicka Divizija" "2. Konjicka Divizija" "3. Konjicka Divizija" "4. Konjicka Divizija" "5. Konjicka Divizija" "6. Konjicka Divizija" "7. Konjicka Divizija" "8. Konjicka Divizija" "9. Konjicka Divizija" "10. Konjicka Divizija" "11. Konjicka Divizija" 
		"12. Konjicka Divizija" 
	}
	motorized_brigade = {
		"1. Motorizovana Divizija" "2. Motorizovana Divizija" "3. Motorizovana Divizija" "4. Motorizovana Divizija" "5. Motorizovana Divizija" "5. Motorizovana Divizija" "6. Motorizovana Divizija" "7. Motorizovana Divizija" "8. Motorizovana Divizija" "9. Motorizovana Divizija" "10. Motorizovana Divizija" 
		"11. Motorizovana Divizija" 
	}
	mechanized_brigade = {
		"1. Mehanizovana Divizija" "2. Mehanizovana Divizija" "3. Mehanizovana Divizija" "4. Mehanizovana Divizija" "5. Mehanizovana Divizija" "6. Mehanizovana Divizija" "7. Mehanizovana Divizija" "8. Mehanizovana Divizija" "9. Mehanizovana Divizija" "10. Mehanizovana Divizija" "11. Mehanizovana Divizija" 
		"12. Mehanizovana Divizija" "13. Mehanizovana Divizija" 
	}
	light_armor_brigade = {
		"1. Oklopna Divizija" "2. Oklopna Divizija" "3. Oklopna Divizija" "4. Oklopna Divizija" "5. Oklopna Divizija" "6. Oklopna Divizija" "7. Oklopna Divizija" "8. Oklopna Divizija" "9. Oklopna Divizija" "10. Oklopna Divizija" "11. Oklopna Divizija" 
		"12. Oklopna Divizija" "13. Oklopna Divizija" "14. Oklopna Divizija" 
	}
	armor_brigade = {
		"1. Oklopna Divizija" "2. Oklopna Divizija" "3. Oklopna Divizija" "4. Oklopna Divizija" "5. Oklopna Divizija" "6. Oklopna Divizija" "7. Oklopna Divizija" "8. Oklopna Divizija" "9. Oklopna Divizija" "10. Oklopna Divizija" "11. Oklopna Divizija" 
		"12. Oklopna Divizija" "13. Oklopna Divizija" "14. Oklopna Divizija" 
	}
	paratrooper_brigade = {
		"1. Padobranska Lovacka Divizija" "2. Padobranska Lovacka Divizija" "3. Padobranska Lovacka Divizija" "4. Padobranska Lovacka Divizija" "5. Padobranska Lovacka Divizija" "6. Padobranska Lovacka Divizija" "7. Padobranska Lovacka Divizija" "8. Padobranska Lovacka Divizija" "9. Padobranska Lovacka Divizija" 
	}
	marine_brigade = {
		"1. Mornaricka Divizija" "2. Mornaricka Divizija" "3. Mornaricka Divizija" "4. Mornaricka Divizija" "5. Mornaricka Divizija" "6. Mornaricka Divizija" "7. Mornaricka Divizija" "8. Mornaricka Divizija" "9. Mornaricka Divizija" "10. Mornaricka Divizija" 
	}
	bergsjaeger_brigade = {
		"Triglavski Odred" "Risnjacki Odred" "Planinski Odred" "Banatski Odred" "Branichevski Odred" "Kalnski Odred" "Komski Odred" "Licki Odred" "Ormoski Odred" "Pozarevacki Odred" "Savski Odred" 
		"Sencanski Odred" "Smederevski Odred" "Somborski Odred" "Strumicki Odred" "Vlasinski Odred" "Zabaljski Odred" 
	}
	garrison_brigade = {
		"25. Vardarska Pesadijska Divizija" "27. Savska Pesadijska Divizija" "30. Osjecka Pesadijska Divizija" "31. Kosovska Pesadijska Divizija" "32. Triglavska Pesadijska Divizija" "33. Licka Pesadijska Divizija" "34. Toplicka Pesadijska Divizija" "38. Dravska Pesadijska Divizija" "40. Slavonska Pesadijska Divizija" "42. Murska Pesadijska Divizija" "44. Unska Pesadijska Divizija" 
		"46. Moravska Pesadijska Divizija" "47. Dinarska Pesadijska Divizija" "49. Sremska Pesadijska Divizija" "50. Drinska Pesadijska Divizija" "51. Pesadijska Divizija" "52. Pesadijska Divizija" "53. Pesadijska Divizija" "54. Pesadijska Divizija" "55. Pesadijska Divizija" "56. Pesadijska Divizija" "57. Pesadijska Divizija" 
		"58. Pesadijska Divizija" "59. Pesadijska Divizija" "60. Pesadijska Divizija" "61. Pesadijska Divizija" "62. Pesadijska Divizija" "63. Pesadijska Divizija" "64. Pesadijska Divizija" "65. Pesadijska Divizija" "66. Pesadijska Divizija" "67. Pesadijska Divizija" "68. Pesadijska Divizija" 
		"69. Pesadijska Divizija" "70. Pesadijska Divizija" 
	}
	hq_brigade = {
		"1. armija" "2. armija" "3. armija" "4. armija" "5. armija" "6. armija" "7. armija" "8. armija" "9. armija" "10. armija" "15. armija" 
			}
	militia_brigade = {
		"1. Milicija Divisi�n" "2. Milicija Divisi�n" "3. Milicija Divisi�n" "4. Milicija Divisi�n" "5. Milicija Divisi�n" "6. Milicija Divisi�n" "7. Milicija Divisi�n" "8. Milicija Divisi�n" "9. Milicija Divisi�n" "10. Milicija Divisi�n" "11. Milicija Divisi�n" 
		"12. Milicija Divisi�n" "13. Milicija Divisi�n" "14. Milicija Divisi�n" "15. Milicija Divisi�n" "16. Milicija Divisi�n" "17. Milicija Divisi�n" "18. Milicija Divisi�n" "19. Milicija Divisi�n" "20. Milicija Divisi�n" 
	}
	multi_role = {
		"2. Vazduhoplovni Lovacki Puk" "4. Vazduhoplovni Lovacki Puk" "5. Vazduhoplovni Lovacki Puk" "6. Vazduhoplovni Lovacki Puk" "9. Vazduhoplovni Lovacki Puk" "10. Vazduhoplovni Lovacki Puk" "15. Vazduhoplovni Lovacki Puk" "16. Vazduhoplovni Lovacki Puk" "17. Vazduhoplovni Lovacki Puk" "18. Vazduhoplovni Lovacki Puk" 
	}
	interceptor = {
		"2. Vazduhoplovni Lovacki Puk" "4. Vazduhoplovni Lovacki Puk" "5. Vazduhoplovni Lovacki Puk" "6. Vazduhoplovni Lovacki Puk" "9. Vazduhoplovni Lovacki Puk" "10. Vazduhoplovni Lovacki Puk" "15. Vazduhoplovni Lovacki Puk" "16. Vazduhoplovni Lovacki Puk" "17. Vazduhoplovni Lovacki Puk" "18. Vazduhoplovni Lovacki Puk" 
	}
	strategic_bomber = {
		"1. Strateski Bombarderski Puk" "3. Strateski Bombarderski Puk" "7. Strateski Bombarderski Puk" "8. Strateski Bombarderski Puk" "11. Strateski Bombarderski Puk" "12. Strateski Bombarderski Puk" "13. Strateski Bombarderski Puk" "14. Strateski Bombarderski Puk" "15. Strateski Bombarderski Puk" "16. Strateski Bombarderski Puk" 
	}
	tactical_bomber = {
		"26. Takticki Bombarderski Puk" "17. Takticki Bombarderski Puk" "18. Takticki Bombarderski Puk" "19. Takticki Bombarderski Puk" "28. Takticki Bombarderski Puk" "38. Takticki Bombarderski Puk" "48. Takticki Bombarderski Puk" "58. Takticki Bombarderski Puk" "68. Takticki Bombarderski Puk" "78. Takticki Bombarderski Puk" 
	}
	naval_bomber = {
		"1. Hidroplanska Komanda" "2. Hidroplanska Komanda" "3. Hidroplanska Komanda" "4. Hidroplanska Komanda" "5. Hidroplanska Komanda" "6. Hidroplanska Komanda" "7. Hidroplanska Komanda" "8. Hidroplanska Komanda" "9. Hidroplanska Komanda" "10. Hidroplanska Komanda" "13. Hidroplanska Torpedo Komanda" 
		"14. Hidroplanska Torpedo Komanda" "15. Hidroplanska Torpedo Komanda" "16. Hidroplanska Torpedo Komanda" "17. Hidroplanska Torpedo Komanda" "18. Hidroplanska Torpedo Komanda" "19. Hidroplanska Torpedo Komanda" "20. Hidroplanska Torpedo Komanda" "24. Hidroplanska Torpedo Komanda" "26. Hidroplanska Torpedo Komanda" 
	}
	cas = {
		"19. Obrasavanje Avio Bombarderski Puk" "20. Obrasavanje Avio Bombarderski Puk" "21. Obrasavanje Avio Bombarderski Puk" "22. Obrasavanje Avio Bombarderski Puk" "23. Obrasavanje Avio Bombarderski Puk" "24. Obrasavanje Avio Bombarderski Puk" "25. Obrasavanje Avio Bombarderski Puk" "27. Obrasavanje Avio Bombarderski Puk" "29. Obrasavanje Avio Bombarderski Puk" "30. Obrasavanje Avio Bombarderski Puk" 
	}
	transport_plane = {
		"1. Transportna Eskadrila" "2. Transportna Eskadrila" "3. Transportna Eskadrila" "4. Transportna Eskadrila" "5. Transportna Eskadrila" 
	}
	battleship = {
		"CJMN Belgrade" "CJMN Zagreb" "CJMN Novi Sad" "CJMN Budva" "CJMN Cetinje" "CJMN Dubrovnik" "CJMN Pec" "CJMN Pizren" "CJMN Pristina" "CJMN Sarajevo" 
	}
	light_cruiser = {
		"CJMN Dalmacija" "CJMN Sarajevo" "CJMN Brus" "CJMN Nis" "CJMN Smederovo" "CJMN Manasija" "CJMN Vrsac" "CJMN Ljubljana" 
	}
	heavy_cruiser = {
		"CJMN Belgrade" "CJMN Zagreb" "CJMN Novi Sad" "CJMN Budva" "CJMN Cetinje" "CJMN Dubrovnik" "CJMN Pec" "CJMN Pizren" "CJMN Pristina" 
	}
	battlecruiser = {
		"CJMN Brus" "CJMN Nis" 
	}
	destroyer = {
		"1. Razarac Flotila" "2. Razarac Flotila" "3. Razarac Flotila" "4. Razarac Flotila" "5. Razarac Flotila" "6. Razarac Flotila" "7. Razarac Flotila" "8. Razarac Flotila" "9. Razarac Flotila" "10. Razarac Flotila" "11. Razarac Flotila" 
		"12. Razarac Flotila" "13. Razarac Flotila" "14. Razarac Flotila" "15. Razarac Flotila" "16. Razarac Flotila" "17. Razarac Flotila" "18. Razarac Flotila" "19. Razarac Flotila" "20. Razarac Flotila" 
	}
	carrier = {
		"CJMN Smederovo" "CJMN Manasija" "CJMN Vrsac" "CJMN Ljubljana" "CJMN Brus" "CJMN Ljubljana" "CJMN Belgrade" "CJMN Zagreb" "CJMN Novi Sad" "CJMN Budva" 
	}
	submarine = {
		"1. Podmorski Flotila" "2. Podmorski Flotila" "3. Podmorski Flotila" "4. Podmorski Flotila" "5. Podmorski Flotila" "6. Podmorski Flotila" "7. Podmorski Flotila" "8. Podmorski Flotila" "9. Podmorski Flotila" "10. Podmorski Flotila" "11. Podmorski Flotila" 
		"12. Podmorski Flotila" "13. Podmorski Flotila" "14. Podmorski Flotila" "15. Podmorski Flotila" "16. Podmorski Flotila" "17. Podmorski Flotila" "18. Podmorski Flotila" "19. Podmorski Flotila" "20. Podmorski Flotila" 
	}
	transport_ship = {
		"1. Pomorski Saobrac Flotila" "2. Pomorski Saobrac Flotila" "3. Pomorski Saobrac Flotila" "4. Pomorski Saobrac Flotila" "5. Pomorski Saobrac Flotila" "6. Pomorski Saobrac Flotila" "7. Pomorski Saobrac Flotila" "8. Pomorski Saobrac Flotila" "9. Pomorski Saobrac Flotila" "10. Pomorski Saobrac Flotila" "11. Pomorski Saobrac Flotila" 
		"12. Pomorski Saobrac Flotila" "13. Pomorski Saobrac Flotila" "14. Pomorski Saobrac Flotila" "15. Pomorski Saobrac Flotila" "16. Pomorski Saobrac Flotila" "17. Pomorski Saobrac Flotila" "18. Pomorski Saobrac Flotila" "19. Pomorski Saobrac Flotila" "20. Pomorski Saobrac Flotila" 
	}

}

ministers = {

		90163 = {
		name = "Borut Pahor"
		ideology = 
		loyalty = 1.00
		picture = M90163
		head_of_state = school_of_psychology
		start_date = 2013.1.1
	}
	
		90164 = {
		name = "Miro Cerar"
		ideology = social_liberal
		loyalty = 1.00
		picture = M90164
		head_of_government = school_of_psychology
		start_date = 2013.1.1
	}
	
			90165 = {
		name = "Karl Erjavec"
		ideology = left_wing_radical
		loyalty = 1.00
		picture = M90165
		foreign_minister = fanatical_academic
		start_date = 2013.1.1
	}
	
				90166 = {
		name = "Vesna Gy�rk�s �nidar"
		ideology = social_liberal
		loyalty = 1.00
		picture = M90166
		minister_of_security = silent_lawyer
		start_date = 2013.1.1
	}
	
					90167 = {
		name = "Andreja Katic"
		ideology = social_democrat
		loyalty = 1.00
		picture = M90167
		minister_of_security = happy_amateur
		start_date = 2013.1.1
	}
	
						90168 = {
		name = "Damir Crncec"
		ideology = 
		loyalty = 1.00
		picture = M90168
		minister_of_intelligence = talented_and_concealed
		start_date = 2013.1.1
	}
	
				90169 = {
		name = "Andrej Osterman"
		ideology = 
		loyalty = 1.00
		picture = M90169
		chief_of_staff = excelling_solder
		chief_of_army = school_of_defence
		chief_of_navy =  open_seas_doctrine
		start_date = 2013.1.1
	}
	
					90170 = {
		name = "Igor Strojin"
		ideology = 
		loyalty = 1.00
		picture = M90170
		chief_of_air = air_to_ground_proponent
		start_date = 2013.1.1
	}

}

