capital = 9613
government = absolute_monarchy
ideology = paternal_autocrat
alignment = { x = -10 y = 10 }
neutrality = 100
national_unity = 75

oob = "SAU_1936.txt"
head_of_state = 67501
head_of_government = 67501
foreign_minister = 67503
minister_of_security = 67504
armament_minister = 67507
minister_of_intelligence = 67510
chief_of_staff = 67514
chief_of_army = 67515
chief_of_air = 67517
chief_of_navy = 67520



popularity = { 
	national_socialist = 10
	fascistic = 0
	paternal_autocrat = 70
	social_conservative = 10
	market_liberal = 0
	social_liberal = 4
	social_democrat = 4
	left_wing_radical = 2
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 10
	fascistic = 0
	paternal_autocrat = 150
	social_conservative = 30
	market_liberal = 0
	social_liberal = 12
	social_democrat = 12
	left_wing_radical = 6
	leninist = 0
	stalinist = 0
}

#Unit Sizes and brigade size
infantry_increase = 2
special_forces_increase = 2
mobile_increase = 2
armor_increase = 2
recon_increase = 2
artillery_increase = 2
armorsupport_increase = 2
aa_at_increase = 2
engineers_increase = 2
Max_BRG_Size = 2

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
service_length = six_months
economic_policy = partial_control
healthcare = public_care

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
truck_engines = 1
engineer_regiment = 1
demolition_equipment = 1
bridging_equipment = 1
military_police_training = 1
security_training = 1
combat_training = 1
military_supplies_production = 0
combat_medicine = 0
first_aid = 0
computers = 1
encryption_devices = 1
decryption_devices = 1
radio_surveillance = 1
radios = 1
radar = 1
civil_defence = 0
supply_transportation = 0
supply_organisation = 0
infra_building = 1
ic_building = 1
air_base_building = 1
naval_base_building = 1
land_fort_building = 1
coastal_fort_building = 1
organisation_tech = 1
staff_combat_sections = 1
unit_cooperation_tech = 1
unit_cohesion = 1
logistics_tech = 1
mechanized_infantry_brigade = 1
apc_armament = 2
apc_armour = 2
apc_engines = 2
apc_reliability = 2
paratrooper_infantry = 1
special_forces_regiment = 1
special_small_arms = 1
special_anti_tank = 1
special_support_weapons = 1
special_heavy_weapons = 1
modern_blitzkrieg = 1
air_superiority_doctrine = 1
green_water_doctrine = 1
submarine_warfare_doctrine = 1

1936.1.1 = { 
	decision = allow_research
	decision = join_gcc

}