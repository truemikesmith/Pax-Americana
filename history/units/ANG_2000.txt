theatre = {
	name = "For�as Armadas Angolanas"
	location = 10147
	regiment = { type = hq_brigade name = "For�as Armadas Angolanas" historical_model = 0 }
	army = {
		name = "Angolan Army"
		location = 10147
		regiment = { type = hq_brigade name = "Army HQ" historical_model = 0 }
		corps = {
			name = "Northern Military Region"
			location = 10147
			regiment = { type = hq_brigade name = "Northern HQ" historical_model = 0 }
			division = {
				is_reserve = yes
				name = "Cabinda Garrison"
				location = 10085
				regiment = { type = garrison_brigade name = "1st Cabinda Battalion" historical_model = 0 }
				regiment = { type = garrison_brigade name = "2nd Cabinda Battalion" historical_model = 0 }
				regiment = { type = garrison_brigade name = "3rd Cabinda Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "1st Mechanized Brigade"
				location = 10147
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = armor_brigade name = "13th Armoured Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "2nd Infantry Brigade"
				location = 10147
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "3rd Infantry Brigade"
				location = 10127
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "4th Infantry Brigade"
				location = 10139
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "5th Infantry Brigade"
				location = 10160
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
		}
		corps = {
			name = "Central Military Region"
			location = 10194
			regiment = { type = hq_brigade name = "Central HQ" historical_model = 0 }
			division = {
				is_reserve = yes
				name = "6th Mechanized Brigade"
				location = 10194
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = armor_brigade name = "63rd Armoured Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "7th Infantry Brigade"
				location = 10171
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "8th Infantry Brigade"
				location = 10183
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "9th Infantry Brigade"
				location = 7985
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "10th Infantry Brigade"
				location = 7977
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
		}
		corps = {
			name = "Southern Military Region"
			location = 10257
			regiment = { type = hq_brigade name = "Southern HQ" historical_model = 0 }
			division = {
				is_reserve = yes
				name = "11th Mechanized Brigade"
				location = 10257
				regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
				regiment = { type = armor_brigade name = "113th Armoured Battalion" historical_model = 0 }
			}
			division = {
				is_reserve = yes
				name = "12th Infantry Brigade"
				location = 10227
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "13th Infantry Brigade"
				location = 10284
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "14th Infantry Brigade"
				location = 7984
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
			division = {
				is_reserve = yes
				name = "15th Infantry Brigade"
				location = 8041
				regiment = { type = infantry_brigade name = "151st Infantry Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Anti-Tank Battalion" historical_model = 1 }
					regiment = { type = pack_artillery_brigade name = "153rd Artillery Battalion" historical_model = 1 }
					regiment = { type = horse_transport name = "153rd Transport Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd HQ Battalion" historical_model = 1 }
			}
		}
	}
		
	air = {
	name = "Quattro De Fevero AFB"
	location = 10147
	base = 10147
	wing = { type = interceptor name = "1st Fighter Wing" historical_model = 0 }
	wing = { type = interceptor name = "2nd Fighter Wing" historical_model = 0 }
	}
		
	air = {
	name = "Lobito AFB"
	location = 10194
	base = 10194
	wing = { type = cas name = "3rd Attack Wing" historical_model = 0 }
	}
	
	air = {
	name = "Namibe AFB"
	location = 10257
	base = 10257
	wing = { type = transport_plane name = "4th Transport Wing" historical_model = 0 }
	}
	
	navy = {
	name = "Angolan Navy"
	location = 10147
	base = 10147
	ship = { type = patrol_boat name = "Osa I" historical_model = 0 }
	ship = { type = patrol_boat name = "Osa II" historical_model = 0 }
	ship = { type = patrol_boat name = "Osa III" historical_model = 0 }
	ship = { type = patrol_boat name = "Osa IV" historical_model = 0 }
	ship = { type = patrol_boat name = "Osa V" historical_model = 0 }
	ship = { type = patrol_boat name = "Osa VI" historical_model = 0 }
	}
}