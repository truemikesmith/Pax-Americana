color = { 149  203  167 }
graphical_culture = Generic
last_election = 2001.3.2
duration = 60

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
}




ministers = {
	66501 = {
		name = "Tanumafili II"
		ideology = social_liberal
		loyalty = 1.00
		picture = M66501
		head_of_state = majestic
		start_date = 2000.1.1
	}
	66502 = {
		name = "Tufuga Efi"
		ideology = social_liberal
		loyalty = 1.00
		picture = M66502
		head_of_state = sympathetic
		start_date = 2000.1.1
	}
	66503 = {
		name = "Tuilaepa Aiono Sailele Malielegaoi"
		ideology = social_liberal
		loyalty = 1.00
		picture = M66503
		head_of_government = intellectualist
		foreign_minister = advertising_figure
		start_date = 2000.1.1
	}
	66504 = {
		name = "Eti Alesana Tofilau"
		ideology = social_liberal
		loyalty = 1.00
		picture = M66504
		minister_of_security = happy_amateur
		armament_minister = school_of_psychology
		start_date = 2000.1.1
	}
	66505 = {
		name = "Le Mamea Ropati"
		ideology = market_liberal
		loyalty = 1.00
		picture = M66505
		head_of_state = corporate_suit
		start_date = 2000.1.1
	}
	66506 = {
		name = "Asiata Sale'imoa Va'ai"
		ideology = market_liberal
		loyalty = 1.00
		picture = M66506
		head_of_government = planning_representative
		foreign_minister = weary_stiff_neck
		start_date = 2000.1.1
	}
	66507 = {
		name = "Sililoto Tolo Tuaifaiva"
		ideology = market_liberal
		loyalty = 1.00
		picture = M66507
		armament_minister = talented_recruiter
		minister_of_security = talented_liar
		start_date = 2000.1.1
	}
	66508 = {
		name = "Tuala Falenaoti Tiresa Malietoa"
		ideology = social_conservative
		loyalty = 1.00
		picture = M66508
		head_of_state = compassionate_gentleman
		start_date = 2000.1.1
	}
	66509 = {
		name = "Billy Asbell"
		ideology = paternal_autocrat
		loyalty = 1.00
		picture = M66509
		head_of_state = fanatical_academic
		start_date = 2000.1.1
	}
	66510 = {
		name = "Sua Rimoni Ah Chong"
		ideology = paternal_autocrat
		loyalty = 1.00
		picture = M66510
		head_of_government = theoretical_scientist
		foreign_minister = indecisive_politician
		start_date = 2000.1.1
	}
	66511 = {
		name = "Leota Itu'au Ale"
		ideology = social_conservative
		loyalty = 1.00
		picture = M66511
		head_of_government = good_connections
		foreign_minister = tempered_speaker
		start_date = 2000.1.1
	}
	66512 = {
		name = "Lorenese Neru"
		ideology = social_liberal
		loyalty = 1.00
		picture = M66512
		minister_of_intelligence = technical_specialist
		chief_of_staff = air_superiority_doctrine
		chief_of_army = decisive_battle_doctrine
		chief_of_air = air_superiority_proponent
		chief_of_navy = indirect_approach_doctrine
		start_date = 2000.1.1
	}
}
