Flying_boat = {
	type = air
	sprite = Naval
	is_bomber = no
	active = no
	

	#Size Definitions
	max_strength = 100
	default_organisation = 30
	default_morale = 0.30
	officers = 80

	#Building Costs
	build_cost_ic = 7.5
	build_cost_manpower = 0.18 ##2-3+ crew
	build_time = 165
	repair_cost_multiplier = 0.2

	#Misc Abilities
	maximum_speed = 130.00
	transport_capability = 0.00
	supply_consumption = 1.33
	fuel_consumption = 3.75
	range = 500.00
	radio_strength = 1

	#Detection Abilities
	surface_detection = 10.00
	air_detection = 1.5
	sub_detection = 0.5

	#Defensive Abilities
	surface_defence = 1.00
	air_defence = 0.75 ## weak against fighters

	#Offensive Abilities
	soft_attack = 0.50
	hard_attack = 0.75
	sea_attack = 1.50 ##
	air_attack = 0.4 ##
	strategic_attack = 0.10
	sub_attack = 4.00 ## cant be too easy to kill subs early on


	completion_size = 2.50
	on_completion = twin_engine_aircraft_practical
	
	priority = 4
}
