theatre = {
	name = "Armed Forces of Guatemala"
	location = 9713
	regiment = { type = hq_brigade name = "Armed Forces HQ" historical_model = 1 }
	division = {
		is_reserve = yes
		name = "1st Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "2nd Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "3rd Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "4th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "5th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "6th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "7th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "8th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
	division = {
		is_reserve = yes
		name = "9th Brigade"
		location = 9713
		regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
	}
}
