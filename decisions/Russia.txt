diplomatic_decisions = {


	Upgrade_Latakia = { #start of game

		potential = {
			tag = RUS	
			controls = 5082
			not = {has_country_flag = upgraded_Latakia}
			date = 2015.1.2 
	   }

		allow = {
			tag = RUS
		}

		effect = {
			
			RUS = {
				5082 = {
					naval_base = 5
					air_base = 5
					garrison_brigade = current
					armor_bat = current
					sp_artillery_brigade = current
					
				}
				add_country_modifier = {  name = Support_assad_small duration = 90 }
				set_country_flag = upgraded_Latakia
			}
			
		}
	}
	
	RUS_intervene_syria = { #Iraq at war with ISIS

		potential = {

			tag = RUS
			has_country_flag = ME_intervene
			SYR = {war_with = ISS}
			not = {has_country_flag = event_306}
	   }

		allow = {
			tag = RUS
		}

		effect = {
			
			this = {
				country_event = 306
				set_country_flag = event_306
			}
			
		}
	}
	
	RUS_Pull_out = { #Iraq at war with ISIS

		potential = {

			tag = RUS
			SYR = {has_country_flag = SYR_XSY_temp_peace}
			not = {
				has_country_modifier = Russian_pullout_timmer
				has_country_flag = Rus_pullout_temp
			}
	   }

		allow = {
			tag = RUS
		}

		effect = {
			
			RUS = {
				remove_brigade = "Russian 1st Syrian Expeditionary Wing"
				remove_brigade = "Russian 2nd Syrian Expeditionary Wing" 
				remove_brigade = "Russian 3rd Syrian Expeditionary Wing"
			}
			SYR = {
				remove_brigade = "Russian 1st Syrian Expeditionary Wing"
				remove_brigade = "Russian 2nd Syrian Expeditionary Wing" 
				remove_brigade = "Russian 3rd Syrian Expeditionary Wing"
			}
			this = {
				#remove_country_modifier = 
				set_country_flag = Rus_pullout_temp
			}
			
		}
	}
	
	#####################################RUSSIA NEEDS TO PULL JETS OUT OF SYRIA AFTER PEACE WITH REBELSx
}