capital = 6070
government = dictatorship
ideology = fascistic
alignment = { x = -30 y = 90 }
neutrality = 100
national_unity = 70
oob = "BUR_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 60
	paternal_autocrat = 0
	social_conservative = 10
	market_liberal = 10
	social_liberal = 10
	social_democrat = 10
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 120
	paternal_autocrat = 0
	social_conservative = 30
	market_liberal = 30
	social_liberal = 30
	social_democrat = 30
	left_wing_radical = 0
	leninist = 0
	stalinist = 0
}

conscription_policy = universal_military_service
press_laws = censorship
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = totalitarianism
economic_policy = state_control
service_length = twofour_months

jungle_infantry = 1
jungle_equipment = 1
infantry_brigade_activation = 1
air_superiority_doctrine = 1
cas_doctrine = 1
submarine_warfare_doctrine = 1
asymmetrical_warfare = 1

1936.1.1 = { decision = allow_research }