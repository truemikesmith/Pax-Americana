marine_brigade = {
	type = land
	sprite = Infantry
	active = no
	unit_group = special_forces_unit_type 

	#Size Definitions
	max_strength = 30
	default_organisation = 30
	default_morale = 0.40
	officers = 120

	#Building Costs
	build_cost_ic = 5
	build_cost_manpower = 5.00
	build_time = 180
	repair_cost_multiplier = 0.08

	#Misc Abilities
	maximum_speed = 4.5
	transport_weight = 10
	supply_consumption = 0.3
	fuel_consumption = 0.3
	radio_strength = 1

	#Defensive Abilities
	defensiveness = 8
	toughness = 4.2 
	softness = 0.80
	air_defence = 5.5

	#Offensive Abilities
	suppression = 1
	soft_attack = 2 ##upgrades to 2.65
	hard_attack = 0.335 ## upgrades to 1.20
	air_attack = 2.50
	ap_attack = 1
	
	plains = {
		attack = -0.2
		defence = 0.2
		movement = -0.1
	}
	river = {
		attack = 0.2
		defence = 0.3
		movement = -0.15
	}
	amphibious = { 
		attack = 0.3
		defence = 0.3
	}
	urban = { 
		attack = 0.5
		defence = 0.5		
		movement = -0.1
	}
	arctic = {
		attack = 0.05		
		defence = 0.1 		
		movement = -0.1	
	}
	desert = {
		attack = -0.15
		defence = 0.2
		movement = -0.1
	}
	woods = { 
		attack = 0.15
		defence = 0.3
		movement = -0.1
	}
	forest = { 
		attack = 0.2
		defence = 0.3
		movement = -0.15
	}
	jungle = { 
		attack = 0.4
		defence = 0.5
		movement = -0.2
	}
	hills = {
		attack = 0.15
		defence = 0.3
		movement = -0.1
	}
	mountain = { 
		attack = 0.15
		defence = 0.3
		movement = -0.2
	}
	marsh = { 
		attack = 0.3
		defence = 0.4
		movement = -0.3
	}
	night = {
	}	
	
	fort = {
		attack = 0.5
		defence = 0.5
	}
	
	combat_width = 1

	completion_size = 1.8
	on_completion = infantry_practical
	
	priority = 68
}
