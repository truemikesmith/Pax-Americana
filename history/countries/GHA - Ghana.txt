capital = 9969
government = republic
ideology = social_conservative
alignment = { x = -40 y = 80 }
neutrality = 100
national_unity = 70
oob = "GHA_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 45
	market_liberal = 0
	social_liberal = 1
	social_democrat = 41
	left_wing_radical = 3
	leninist = 1
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 135
	market_liberal = 0
	social_liberal = 3
	social_democrat = 123
	left_wing_radical = 9
	leninist = 3
	stalinist = 0
}

conscription_policy = volunteer_army
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
economic_policy = lais_fair
healthcare = public_care
service_length = six_months

infantry_brigade_activation = 1
infantry_small_arms = 1
infantry_heavy_weapons = 1
infantry_anti_tank = 1
infantry_support = 1
paratrooper_infantry = 1
special_small_arms = 1
special_anti_tank = 1
special_heavy_weapons = 1
special_support_weapons = 1
superior_firepower = 1

1936.1.1 = { decision = allow_research }