theatre = {
	name = "For�as Armadas Portuguesas"
	location = 4644
	regiment = { type = hq_brigade name = "For�as Armadas Portuguesas" historical_model = 2 }
	corps = {
		name = "Ex�rcito Portugu�s"
		location = 4644
		regiment = { type = hq_brigade name = "Ex�rcito Portugu�s" historical_model = 2 }
		division = {
			is_reserve = yes
			name = "Brigada Mecanizada"
			location = 4645
			regiment = { type = armor_brigade name = "Grupo de Carros de Combate" historical_model = 0 }
			regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
		}
		division = {
			is_reserve = yes
			name = "Brigada de Interven��o"
			location = 4405
			regiment = { type = armor_brigade name = "Grupo de Auto-Metralhadoras" historical_model = 0 }
			regiment = { type = mechanized_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
		}
		division = {
			is_reserve = yes
			name = "Brigada de Rea��o R�pida"
			location = 4645
			regiment = { type = special_forces_brigade name = "For�a de Opera��es Especiais" historical_model = 2 }
			regiment = { type = paratrooper_brigade name = "1� Batalh�o de Infantaria Paraquedista" historical_model = 2 }
			regiment = { type = paratrooper_brigade name = "2� Batalh�o de Infantaria Paraquedista" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Zona Militar dos A�ores"
			location = 8970
			regiment = { type = garrison_brigade name = "Regimento de Guarni��o n� 1" historical_model = 2 }
			regiment = { type = garrison_brigade name = "Regimento de Guarni��o n� 2" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Zona Militar da Madeira"
			location = 11384
			regiment = { type = garrison_brigade name = "Regimento de Guarni��o n� 3" historical_model = 2 }
		}
	}
	
	navy = {
	name = "Marinha Portuguesa"
	base = 4644
	location = 4644
	ship = { type = heavy_cruiser name = "NRP Vasco da Gama" historical_model = 1 }
	ship = { type = heavy_cruiser name = "NRP �lvares Cabral" historical_model = 1 }
	ship = { type = heavy_cruiser name = "NRP Corte-Real" historical_model = 1 }
	ship = { type = light_cruiser name = "NRP Baptista de Andrade" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP Jo�o Roby" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP Afonso Cerqueira" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP Ant�nio Enes" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP Jo�o Coutinho" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP Jacinto C�ndido" historical_model = 0 }
	ship = { type = light_cruiser name = "NRP General Pereira D'E�a" historical_model = 0 }
	ship = { type = invasion_transport_ship name = "NRP Bombarda" historical_model = 0 }
	}
	
	air = {
	name = "Air Base No. 6"
	base = 4750
	location = 4750
	wing = { type = transport_plane name = "Esquadra 501 'Bisontes'" historical_model = 0 }
	}
	
	air = {
	name = "Air Base No. 5"
	base = 4406
	location = 4406
	wing = { type = multi_role name = "Esquadra 201 'Falc�es'" historical_model = 0 }
	wing = { type = multi_role name = "Esquadra 301 'Jaguares'" historical_model = 0 }
	}
}
	
	