color = { 76  157  195 }
graphical_culture = Generic
last_election = 1999.3.7
duration = 48

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
}

unit_names = {
	air_cavalry_brigade = {
		"1. Öhkratsavägibrigaad" "2. Öhkratsavägibrigaad" "3. Öhkratsavägibrigaad" "4. Öhkratsavägibrigaad" "5. Öhkratsavägibrigaad"
		"6. Öhkratsavägibrigaad" "7. Öhkratsavägibrigaad" "8. Öhkratsavägibrigaad" "9. Öhkratsavägibrigaad" "10. Öhkratsavägibrigaad"
	}
	armor_brigade = {
		"1. Soomustatud Brigaad" "2. Soomustatud Brigaad" "3. Soomustatud Brigaad" "4. Soomustatud Brigaad" "5. Soomustatud Brigaad"
		"6. Soomustatud Brigaad" "7. Soomustatud Brigaad" "8. Soomustatud Brigaad" "9. Soomustatud Brigaad" "10. Soomustatud Brigaad"
	}
	garrison_brigade = {
		"Alutaguse Maleva" "Harju Maleva" "Jõgeva Maleva" "Järva Maleva" "Lääne Maleva" "Põlva Maleva" "Pärnumaa Maleva" "Rapla Maleva" 
		"Saaremaa Maleva" "Sakala Maleva" "Tallinna Maleva" "Tartu Maleva" "Valgamaa Maleva" "Viru Maleva" "Võrumaa Maleva"
	}
	infantry_brigade = {
		"1. Jalaväebrigaad" "2. Jalaväebrigaad" "3. Jalaväebrigaad" "4. Jalaväebrigaad" "5. Jalaväebrigaad" "6. Jalaväebrigaad"
		"7. Jalaväebrigaad" "8. Jalaväebrigaad" "9. Jalaväebrigaad" "10. Jalaväebrigaad" "11. Jalaväebrigaad" "12. Jalaväebrigaad"
	}
	jungle_brigade = {
		"1. Dzungelbrigaad" "2. Dzungelbrigaad" "3. Dzungelbrigaad" "4. Dzungelbrigaad" "5. Dzungelbrigaad"
	}
	marine_brigade = {
		"1. Merebrigaad" "2. Merebrigaad" "3. Merebrigaad" "5. Merebrigaad" "6. Merebrigaad"
	}
	mechanized_brigade = {
		"1. Mehhaniseeritud Jalaväebrigaad" "2. Mehhaniseeritud Jalaväebrigaad" "3. Mehhaniseeritud Jalaväebrigaad" "4. Mehhaniseeritud Jalaväebrigaad" "5. Mehhaniseeritud Jalaväebrigaad"
		"6. Mehhaniseeritud Jalaväebrigaad" "7. Mehhaniseeritud Jalaväebrigaad" "8. Mehhaniseeritud Jalaväebrigaad" "9. Mehhaniseeritud Jalaväebrigaad" "10. Mehhaniseeritud Jalaväebrigaad"
	}
	militia_brigade = {
		"1. Maakaitsevägi" "2. Maakaitsevägi" "3. Maakaitsevägi" "4. Maakaitsevägi" "5. Maakaitsevägi"
		"6. Maakaitsevägi" "7. Maakaitsevägi" "8. Maakaitsevägi" "9. Maakaitsevägi" "10. Maakaitsevägi"
	}
	mountain_brigade = {
		"1. Mägibrigaad" "2. Mägibrigaad" "3. Mägibrigaad" "4. Mägibrigaad" "5. Mägibrigaad"
	}
	paratrooper_brigade = {
		"1. Langevarjurbrigaad" "2. Langevarjurbrigaad" "3. Langevarjurbrigaad" "4. Langevarjurbrigaad" "5. Langevarjurbrigaad" 
	}
	special_forces_brigade = {
		"1. Erivägedebrigaad" "2. Erivägedebrigaad" "3. Erivägedebrigaad" "4. Erivägedebrigaad" "5. Erivägedebrigaad"
	} 
	
	assault_ship = {
		"EML Lembit"  "EML Lood"
	}
	battlecruiser = {
		"EML Admiral Cowan" "EML Grif"
	}
	battleship = {
		"EML Sakala" "EML Laine"
	}
	carrier = {
		"EML Ugandi" "EML Sulev"
	}
	destroyer = {
		"EML Suurop" "EML Lennuk" "EML Vambola" 
	}
	escort_carrier = {
		"EML Ristna" "EML Kompass"
	}
	heavy_cruiser = {
		"EML Tasuja" "EML Wambola"
	}
	invasion_transport_ship = {
		"1. Maabumisekslaeveskadron" "2. Maabumisekslaeveskadron" "3. Maabumisekslaeveskadron" "4. Maabumisekslaeveskadron" "5. Maabumisekslaeveskadron"
	}
	light_cruiser = {
		"EML Meeme" "EML Tahkona" "EML Lehtma"
	}
	patrol_boat = {
		"EML Mardus" "EML Keri" "EML Vaindlo"
	}
	submarine = {
		"EML Kalev" "EML Lembit"
	}
	transport_ship = {
		"1. Transpordiettevõtteid" "2. Transpordiettevõtteid" "3. Transpordiettevõtteid" "4. Transpordiettevõtteid" "5. Transpordiettevõtteid"
	}
	
	ballistic_missile = {
		"1. Ballistiliste Rakettidejagu" "2. Ballistiliste Rakettidejagu" "3. Ballistiliste Rakettidejagu" "4. Ballistiliste Rakettidejagu" "5. Ballistiliste Rakettidejagu"
	}
	cag = {
		"1. Lennuettevõtjalennuväegrupp" "2. Lennuettevõtjalennuväegrupp" "3. Lennuettevõtjalennuväegrupp" "4. Lennuettevõtjalennuväegrupp" "5. Lennuettevõtjalennuväegrupp"
	}
	cas = {
		"1. Maapealsedtiib" "2. Maapealsedtiib" "3. Maapealsedtiib" "4. Maapealsedtiib" "5. Maapealsedtiib"
	}
	icbm = {
		"1. Mandritevahelise Ballistilise Raketi" "2. Mandritevahelise Ballistilise Raketi" "3. Mandritevahelise Ballistilise Raketi" "4. Mandritevahelise Ballistilise Raketi" "5. Mandritevahelise Ballistilise Raketi"
	}
	interceptor = {
		"1. Võitlejatiiva" "2. Võitlejatiiva" "3. Võitlejatiiva" "4. Võitlejatiiva" "5. Võitlejatiiva"
	}
	multi_role = {
		"1. Öhktiiva" "2. Öhktiiva" "3. Öhktiiva" "4. Öhktiiva" "5. Öhktiiva"
	}
	naval_bomber = {
		"1. Mereväe Pommitajatiiva" "2. Mereväe Pommitajatiiva" "3. Mereväe Pommitajatiiva" "4. Mereväe Pommitajatiiva" "5. Mereväe Pommitajatiiva" 
	}
	strategic_bomber = {
		"1. Pommitajatiiva" "2. Pommitajatiiva" "3. Pommitajatiiva" "4. Pommitajatiiva" "5. Pommitajatiiva" 
	}
	tbm = {
		"1. Teater Ballistiliste Rakettidejagu" "2. Teater Ballistiliste Rakettidejagu" "3. Teater Ballistiliste Rakettidejagu" "4. Teater Ballistiliste Rakettidejagu" "5. Teater Ballistiliste Rakettidejagu"
	}
	transport_plane = {
		"1. Transporditiib" "2. Transporditiib" "3. Transporditiib" "4. Transporditiib" "5. Transporditiib"
	}
	
}


ministers = {
	26501 = {
		name = "Arnold Rüütel"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26501
		head_of_state = hazardous_clean
		start_date = 2000.1.1
	}
	26502 = {
		name = "Toomas Savi"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26502
		head_of_state = industrial_specialist
		start_date = 2000.1.1
	}
	26503 = {
		name = "Ene Ergma"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26503
		head_of_state = theoretical_scientist
		start_date = 2003.1.1
	}
	26504 = {
		name = "Toomas Hendrik Ilves"
		ideology = social_democrat
		loyalty = 1.00
		picture = M26504
		head_of_state = indecisive_politician
		start_date = 2000.1.1
	}
	26505 = {
		name = "Indrek Tarand"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26505
		head_of_state = backroom_backstabber
		start_date = 2000.1.1
	}
	26506 = {
		name = "Siim Kallas"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26506
		head_of_government = logistics_specialist
		foreign_minister = talented_and_concealed
		start_date = 2000.1.1
	}
	26507 = {
		name = "Juhan Parts"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26507
		head_of_government = corporate_suit
		start_date = 2000.1.1
	}
	26508 = {
		name = "Andrus Ansip"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26508
		head_of_government = apologetic_clerk
		start_date = 2004.1.1
	}
	26509 = {
		name = "Kristiina Ojuland"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26509
		foreign_minister = good_connections
		start_date = 2000.1.1
	}
	26510 = {
		name = "Rein Lang"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26510
		foreign_minister = prince_of_terror
		start_date = 2005.1.1
	}
	26511 = {
		name = "Urmas Paet"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26511
		foreign_minister = biased_intellectual
		start_date = 2005.1.1
	}
	26512 = {
		name = "Sven Mikser"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26512
		armament_minister = autocratic_charmer
		start_date = 2000.1.1
	}
	26513 = {
		name = "Margus Hanson"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26513
		armament_minister = service_enforcer
		start_date = 2000.1.1
	}
	26514 = {
		name = "Jaak Jõerüüt"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26514
		armament_minister = silent_workhorse
		start_date = 2004.1.1
	}
	26515 = {
		name = "Jürgen Ligi"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26515
		armament_minister = efficient_sociopath
		start_date = 2005.1.1
	}
	26516 = {
		name = "Jaak Aaviksoo"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26516
		armament_minister = service_enforcer
		start_date = 2007.1.1
	}
	26517 = {
		name = "Mart Laar"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26517
		armament_minister = elastic_defence_doctrine
		start_date = 2000.1.1
	}
	26518 = {
		name = "Urmas Reinsalu"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26518
		armament_minister = man_of_the_people
		start_date = 2012.1.1
	}
	26519 = {
		name = "Ain Seppik"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26519
		minister_of_security = technical_specialist
		start_date = 2000.1.1
	}
	26520 = {
		name = "Toomas Varek"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26520
		minister_of_security = weary_stiff_neck
		start_date = 2003.1.1
	}
	26521 = {
		name = "Margus Leivo"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26521
		minister_of_security = political_protege
		start_date = 2000.1.1
	}
	26522 = {
		name = "Kalle Laanet"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26522
		minister_of_security = laissez_faires_capitalist
		start_date = 2005.1.1
	}
	26523 = {
		name = "Jüri Pihl"
		ideology = social_democrat
		loyalty = 1.00
		picture = M26523
		minister_of_security = backroom_backstabber
		minister_of_intelligence = biased_intellectual
		start_date = 2000.1.1
	}
	26524 = {
		name = "Marko Pomerants"
		ideology = market_liberal
		loyalty = 1.00
		picture = M26524
		minister_of_security = prince_of_terror
		start_date = 2000.1.1
	}
	26525 = {
		name = "Ken-Marti Vaher"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26525
		minister_of_security = political_specialist
		start_date = 2011.1.1
	}
	26526 = {
		name = "Jüri Ratas"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26526
		head_of_government = tempered_speaker
		start_date = 2000.1.1
	}
	26527 = {
		name = "Edgar Savisaar"
		ideology = social_liberal
		loyalty = 1.00
		picture = M26527
		foreign_minister = undistinguished_suit
		start_date = 2000.1.1
	}
	26528 = {
		name = "Tunne Kelam"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26528
		foreign_minister = man_of_the_people
		start_date = 2000.1.1
	}
	26529 = {
		name = "Ivari Padar"
		ideology = social_democrat
		loyalty = 1.00
		picture = M26529
		head_of_government = prince_of_terror
		start_date = 2000.1.1
	}
	26530 = {
		name = "Peeter Kreitzberg"
		ideology = social_democrat 
		loyalty = 1.00
		picture = M26530
		armament_minister = flamboyant_tough_guy
		start_date = 2000.1.1
	}
	26531 = {
		name = "Mark Soosaar"
		ideology = social_democrat
		loyalty = 1.00
		picture = M26531
		foreign_minister = industry_servant
		start_date = 2000.1.1
	}
	26532 = {
		name = "Tiit Toomsalu"
		ideology = leninist
		loyalty = 1.0
		picture = M26532
		head_of_state = research_specialist
		start_date = 2000.1.1
	}
	26533 = {
		name = "Sirje Kingsepp"
		ideology = leninist
		loyalty = 1.00
		picture = M26533
		head_of_government = iron_fisted_brute
		start_date = 2000.1.1
	}
	26534 = {
		name = "Sergei Jürgens"
		ideology = leninist
		loyalty = 1.00
		picture = M26534
		foreign_minister = flamboyant_tough_guy
		start_date = 2000.1.1
	}
	26535 = {
		name = "Heino Rüütel"
		ideology = leninist
		loyalty = 1.00
		picture = M26535
		armament_minister = army_aviation_doctrine
		start_date = 2000.1.1
	}
	26536 = {
		name = "Vello Leito"
		ideology = fascistic
		loyalty = 1.00
		picture = M26536
		head_of_state = silent_workhorse
		start_date = 2000.1.1
	}
	26537 = {
		name = "Tauno Rahnu"
		ideology = fascistic
		loyalty = 1.00
		picture = M26537
		head_of_government = unexperienced_diplomat
		start_date = 2000.1.1
	}
	26538 = {
		name = "Risto Teinonen"
		ideology = fascistic
		loyalty = 1.00
		picture = M26538
		minister_of_security = staunch_capitalist
		start_date = 2000.1.1
	}
	26539 = {
		name = "Stanislav Tšerepanov"
		ideology = paternal_autocrat
		loyalty = 1.00
		picture = M26539
		head_of_government = fanatical_academic
		start_date = 2000.1.1
	}
	26540 = {
		name = "Aldis Alus"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26540
		minister_of_intelligence = ruthless_powermonger
		start_date = 2003.1.1
	}
	26541 = {
		name = "Raivo Aeg"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26541
		minister_of_intelligence = popular_figurehead
		start_date = 2008.1.1
	}
	26542 = {
		name = "Arnold Sinisalu"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26542
		minister_of_intelligence = benevolent_gentleman
		start_date = 2013.1.1
	}
	26543 = {
		name = "Tarmo Kõuts"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26543
		chief_of_staff = patriot
		start_date = 2000.1.1
	}
	26544 = {
		name = "Ants Laaneots"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26544
		chief_of_staff = logistics_specialist
		start_date = 2006.1.1
	}
	26545 = {
		name = "Riho Terras"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26545
		chief_of_staff = service_enforcer
		start_date = 2011.1.1
	}
	26546 = {
		name = "Indrek Sirel"
		ideology = social_conservative 
		loyalty = 1.00
		picture = M26546
		chief_of_army = school_of_mass_combat
		start_date = 2000.1.1
	}
	26547 = {
		name = "Artur Tiganik"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26547
		chief_or_army = school_of_defence
		start_date = 2008.1.1
	}
	26548 = {
		name = "Teo Krüüner"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26548
		chief_of_air = vertical_envelopment_doctrine
		start_date = 2000.1.1
	}
	26549 = {
		name = "Mart Vendla"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26549
		chief_of_air = strategic_air_proponent
		start_date = 2004.1.1
	}
	26550 = {
		name = "Valeri Saar"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26550
		chief_of_air = air_superiority_proponent
		start_date = 2005.1.1
	}
	26551 = {
		name = "Rauno Sirk"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26551
		chief_of_air = talented_recruiter
		start_date = 2006.1.1
	}
	26552 = {
		name = "Jaak Tarien"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26552
		chief_of_air = guns_and_butter_doctrine
		start_date = 2012.1.1
	}
	26553 = {
		name = "Igor Schvede"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26553
		chief_of_navy = battle_fleet_proponent
		start_date = 2000.1.1
	}
	26554 = {
		name = "Sten Sepper"
		ideology = social_conservative
		loyalty = 1.00
		picture = M26554
		chief_of_navy = naval_aviation_doctrine
		start_date = 2013.1.1
	}
}

