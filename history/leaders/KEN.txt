84001 = {
	name = "Ochieng"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84001
	add_trait = marine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84002 = {
	name = "Kasaya"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84002
	add_trait = improvisation
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84003 = {
	name = "Situma"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84003
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84004 = {
	name = "Atudo"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84004
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84005 = {
	name = "Owino"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84005
	add_trait = assaulter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84006 = {
	name = "Mohammed"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 0.00
	picture = L84006
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84007 = {
	name = "Meja"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84007
	add_trait = hard_defender
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84008 = {
	name = "Mariga"
	country = KEN
	type = land
	skill = 1
	max_skill = 2
	loyalty = 1.00
	picture = L84008
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84009 = {
	name = "Wanyama"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84009
	add_trait = landing_specialist
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84010 = {
	name = "Opiyo"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84010
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84011 = {
	name = "Kahata"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84011
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84012 = {
	name = "Were"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84012
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84013 = {
	name = "Miheso"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 0.00
	picture = L84013
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84014 = {
	name = "Akumu"
	country = KEN
	type = land
	skill = 0
	max_skill = 1
	loyalty = 0.00
	picture = L84014
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84015 = {
	name = "Omolo"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84015
	add_trait = fortress_buster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84016 = {
	name = "Oliech"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84016
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84017 = {
	name = "Lavatsa"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84017
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84018 = {
	name = "Keli"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 0.00
	picture = L84018
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
84019 = {
	name = "Masika"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84019
	add_trait = frontline_soldier
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84020 = {
	name = "Onyango"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84020
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84021 = {
	name = "Origi"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84021
	add_trait = commando
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84022 = {
	name = "Ndeto"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84022
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84023 = {
	name = "Mandela"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84023
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84024 = {
	name = "Juma"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84024
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84025 = {
	name = "Hassan"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84025
	add_trait = hard_defender
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84026 = {
	name = "Wafula"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84026
	add_trait = logistics_wizard
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84027 = {
	name = "Gateri"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84027
	add_trait = panzer_leader
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84028 = {
	name = "Okoth"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84028
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84029 = {
	name = "Bwamy"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84029
	add_trait = landing_specialist
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84030 = {
	name = "Kiongera"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84030
	add_trait = defensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84031 = {
	name = "Wendo"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 0.00
	picture = L84031
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84032 = {
	name = "Waruru"
	country = KEN
	type = land
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84032
	add_trait = landlubber
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84033 = {
	name = "Aswani"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84033
	add_trait = trickster
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84034 = {
	name = "Salim"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84034
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84035 = {
	name = "Oguna"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84035
	add_trait = offensive_doctrine
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84036 = {
	name = "J. W. Kianga"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84036
	add_trait = hard_defender
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
84037 = {
	name = "Njoroge"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84037
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
84038 = {
	name = "Kasaon"
	country = KEN
	type = land
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84038
	add_trait = assaulter
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
84039 = {
	name = "Ngondi"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84039
	add_trait = defensive_doctrine
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
84040 = {
	name = "Tuwei"
	country = KEN
	type = land
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84040
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
84041 = {
	name = "Ochieng"
	country = KEN
	type = air
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84041
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84042 = {
	name = "Keli"
	country = KEN
	type = air
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84042
	add_trait = night_flyer
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84043 = {
	name = "Karangi"
	country = KEN
	type = air
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84043
	add_trait = carpet_bomber
	history = {
		1995.1.1 = { rank = 1 }
		1998.1.1 = { rank = 2 }
		2001.1.1 = { rank = 3 }
		2003.1.1 = { rank = 4 }
	}
}
84044 = {
	name = "Otieno"
	country = KEN
	type = air
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84044
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
84045 = {
	name = "Miheso"
	country = KEN
	type = sea
	skill = 0
	max_skill = 2
	loyalty = 1.00
	picture = L84045
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84046 = {
	name = "Opiyo"
	country = KEN
	type = sea
	skill = 0
	max_skill = 2
	loyalty = 0.00
	picture = L84046
	add_trait = spotter
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84047 = {
	name = "Osiako"
	country = KEN
	type = sea
	skill = 0
	max_skill = 3
	loyalty = 1.00
	picture = L84047
	history = {
		2003.1.1 = { rank = 1 }
	}
}
84048 = {
	name = "Mwathethe"
	country = KEN
	type = sea
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84048
	history = {
		1998.1.1 = { rank = 1 }
		2001.1.1 = { rank = 2 }
		2003.1.1 = { rank = 3 }
	}
}
84049 = {
	name = "Awitta"
	country = KEN
	type = sea
	skill = 1
	max_skill = 3
	loyalty = 1.00
	picture = L84049
	add_trait = old_guard
	history = {
		2000.1.1 = { rank = 1 }
		2003.1.1 = { rank = 2 }
	}
}
