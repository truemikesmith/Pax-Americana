icbm = {
	type = air
	active = no
	sprite = Rocket

	is_bomber = yes
	is_rocket = yes

	#Size Definitions
	max_strength = 100
	default_organisation = 100
	default_morale = 1.00

	#Building Costs
	build_cost_ic = 4.50
	build_cost_manpower = 0.05
	build_time = 100
	officers = 0
	repair_cost_multiplier = 0.05

	#Misc Abilities
	maximum_speed = 1000.00
	transport_capability = 0.00
	supply_consumption = 0.25
	fuel_consumption = 3.45
	range = 6000.00
	radio_strength = 1

	#Detection Abilities
	surface_detection = 0.00
	air_detection = 0.00

	#Defensive Abilities
	surface_defence = 100.00
	air_defence = 100.00

	#Offensive Abilities
	soft_attack = 0.00
	hard_attack = 0.00
	sea_attack = 0.00
	air_attack = 0.00
	strategic_attack = 150.00
	sub_attack = 0.00

	completion_size = 0.1
	on_completion = rocket_practical
	
	priority = 3

}
