color = { 99  120  119 }
graphical_culture = Generic
last_election = 1998.1.18
duration = 60

default_templates = {
	generic_infantry = {
		infantry_brigade
		infantry_brigade
		infantry_brigade
	}
	generic_milita = {
		militia_brigade
		militia_brigade
		militia_brigade
	}
}




ministers = {
	32001 = {
		name = "Keith Mitchell"
		ideology = social_conservative
		loyalty = 1.00
		picture = M32001
		head_of_government = die_hard_reformer
		armament_minister = service_enforcer
		start_date = 2000.1.1
	}
	32002 = {
		name = "Tillman Thomas"
		ideology = social_liberal
		loyalty = 1.00
		picture = M32002
		head_of_government = crime_fighter
		armament_minister = technical_specialist
		start_date = 2000.1.1
	}
	32003 = {
		name = "Mark Isaac"
		ideology = social_conservative
		loyalty = 1.00
		picture = M32003
		foreign_minister = ideological_crusader
		start_date = 2000.1.1
	}
	32004 = {
		name = "Elvin Nimrod"
		ideology = social_conservative
		loyalty = 1.00
		picture = M32004
		minister_of_security = autocratic_charmer
		start_date = 2000.1.1
	}
	32005 = {
		name = "Alleyne Walker"
		ideology = social_liberal
		loyalty = 1.00
		picture = M32005
		head_of_state = undistinguished_suit
		start_date = 2000.1.1
	}
	32006 = {
		name = "Randolph Harrison Fleary"
		ideology = social_liberal
		loyalty = 1.00
		picture = M32006
		foreign_minister = political_protege
		start_date = 2000.1.1
	}
	32007 = {
		name = "Terry Hillaire"
		ideology = social_liberal
		loyalty = 1.00
		picture = M32007
		minister_of_security = planning_representative
		start_date = 2000.1.1
	}
	32008 = {
		name = "Roland Bhola"
		ideology = social_conservative
		loyalty = 1.00
		picture = M32008
		head_of_state = pig_headed_isolationist
		start_date = 2000.1.1
	}
	32009 = {
		name = "Gloria Payne Banfield"
		ideology = social_democrat
		loyalty = 1.00
		picture = M32009
		head_of_government = fanatical_academic
		start_date = 2000.1.1
	}
	32010 = {
		name = "Willan Thompson"
		ideology = social_conservative
		loyalty = 1.00
		picture = M32010
		minister_of_intelligence = efficient_sociopath
		chief_of_staff = elastic_defence_doctrine
		chief_of_army = tank_proponent
		chief_of_navy = indirect_approach_doctrine
		chief_of_air = air_to_sea_proponent
		start_date = 2000.1.1
	}
}
