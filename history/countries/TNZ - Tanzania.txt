capital = 10129
government = republic
ideology = leninist
alignment = { x = -30 y = 80 }
neutrality = 100
national_unity = 70
oob = "TNZ_2000.txt"

popularity = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 4
	market_liberal = 4
	social_liberal = 13
	social_democrat = 0
	left_wing_radical = 9
	leninist = 65
	stalinist = 0
}

organization = { 
	national_socialist = 0
	fascistic = 0
	paternal_autocrat = 0
	social_conservative = 12
	market_liberal = 12
	social_liberal = 39
	social_democrat = 0
	left_wing_radical = 27
	leninist = 150
	stalinist = 0
}

conscription_policy = military_service
press_laws = free_press
mobilisation_level = peace_time
environmental_policy = some_laws
civil_laws = open_society
service_length = twofour_months
economic_policy = lais_fair
healthcare = aid_groups

infantry_brigade_activation = 1
superior_firepower = 1

1936.1.1 = { decision = allow_research }