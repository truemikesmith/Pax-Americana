XSY_unit = {
	type = land
	sprite = Infantry
	active = no
	unit_group = infantry_unit_type

	#Size Definitions
	max_strength = 8
	default_organisation = 30
	default_morale = 0.4
	officers = 10

	#Building Costs
	build_cost_ic = 1.00
	build_cost_manpower = 1.5
	build_time = 30

	#Misc Abilities
	maximum_speed = 10.00
	transport_weight = 1.00
	supply_consumption = 0.1
	fuel_consumption = 0.1
	radio_strength = 1
	repair_cost_multiplier = 0

	#Defensive Abilities
	defensiveness = 40	# was 7
	toughness = 0
	softness = 20.0
	air_defence = 5.30
	armor_value = 0

	#Offensive Abilities
	suppression = 0.00
	soft_attack = 2.75
	hard_attack = 2.75
	air_attack = 0.165
	ap_attack = 26

		plains = { 
		defence = 0.3
		
		movement = -0.3
	}
	river = { 
		defence = 0.4
		
		movement = -0.3
	}
	amphibious = { 
		
	}
	urban = {
		
		movement = -0.3
	}
	arctic = {
		
		defence = 0.05
		movement = -0.4
	}
	desert = {
		
		defence = 0.3
		movement = -0.3
	}
	woods = { 
		
		movement = -0.45
	}
	forest = { 
		
		movement = -0.5
	}
	jungle = { 
		
		movement = -0.6
	}
	hills = { 
		
		movement = -0.45
	}
	mountain = { 
		
		movement = -0.5
	}
	marsh = { 
		
		movement = -0.6
	}
	night = {
	}
	fort = { 
		
	}
	combat_width = 0.1

   
	
	combat_width = 0.2

	completion_size = 2.6
	on_completion = infantry_practical
	
	priority = 32

}
