
local P = {}
AI_SYR = P

function P.DiploScore_Alliance(voDiploScoreObj)
	local lsTargetTag = voDiploScoreObj.TargetTag
	
	if lsTargetTag == 'RUS' then -- we got better plans for you...
			voDiploScoreObj.Score = 200
	end
		
	return voDiploScoreObj.Score
end

function P.ForeignMinister_CallAlly(voForeignMinisterData)

	local loISSTag = CCountryDataBase.GetTag("ISS")
	local loSYRISSRelation = voForeignMinisterData.ministerCountry:GetRelation(loISSTag)
	--Utils.LUA_DEBUGOUT("SYR looking to call ally")
	-- Get a list of all your allies
		local laAllies = {}
		for loAllyTag in voForeignMinisterData.ministerCountry:GetAllies() do
			local loAllyCountry = loAllyTag:GetCountry()

		-- Exclude Puppets from this list
			if not(loAllyCountry:IsPuppet()) then
				local loAlly = {
					AllyTag = loAllyTag,
					AllyCountry = loAllyCountry,
					Continent = tostring(loAllyCountry:GetActingCapitalLocation():GetContinent():GetTag()),
				}
				--Utils.LUA_DEBUGOUT("SYR getting list of allies")
				laAllies[tostring(loAllyTag)] = loAlly
			end
		end
	
	if (loSYRISSRelation:HasWar()) then
		-- Go through our Wars
		for loDiploStatus in voForeignMinisterData.ministerCountry:GetDiplomacy() do
			local loTargetTag = loDiploStatus:GetTarget()
			
			--Utils.LUA_DEBUGOUT("SYR go threw wars")
			if loTargetTag:IsValid() and loDiploStatus:HasWar() then
				local loWar = loDiploStatus:GetWar()
			
				if loWar:IsLimited() then
					local lsTargetTag = tostring(loTargetTag)
					local liWarMonths = loWar:GetCurrentRunningTimeInMonths()
					local loTargetCountry = loTargetTag:GetCountry()
				
				-- Call in all potential allies
					for k, v in pairs(laAllies) do
						--Utils.LUA_DEBUGOUT("SYR valling allies")
						if not(v.AllyCountry:GetRelation(loTargetTag):HasWar()) then
							if (loSYRISSRelation:HasWar()) then
								if k == "RUS" then
								--Utils.LUA_DEBUGOUT("SYR calling russia")
									--Support.ExecuteCallAlly(voForeignMinisterData.ministerAI, voForeignMinisterData.ministerTag, v, loTargetTag)
								end
							end
						end
					end
					
				end
			end
		end
	end
	return false
end



-- Return:
return AI_SYR

