theatre = {
	name = "Suomen Puolusvoimat"
	location = 739
	regiment = { type = hq_brigade name = "Pääesikunta" historical_model = 2 }
	army = {
	name = "Maavoimat"
	location = 739
	regiment = { type = hq_brigade name = "Maavoimien Esikunta" historical_model = 2 }
	corps = {
		name = "Etelä-Suomen Sotilaslääni"
		location = 739
		regiment = { type = hq_brigade name = "Etelä-Suomen Sotilasläänin Esikunta" historical_model = 2 }
		division = {
			is_reserve = yes
			name = "Kaartin Jääkärirykmentti"
			location = 739
			regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
		}
	}
	corps = {
		name = "Pohjois-Suomen Sotilaslääni"
		location = 234
		regiment = { type = hq_brigade name = "Pohjois-Suomen Sotilasläänin Esikunta" historical_model = 2 }
		division = {
			is_reserve = yes
			name = "Jääkäriprikaati"
			location = 105
			regiment = { type = infantry_brigade name = "151st Mechanized Battalion" historical_model = 1 }
					regiment = { type = anti_tank_brigade name = "152nd Mechanized Battalion" historical_model = 1 }
					regiment = { type = artillery_brigade name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = light_transport name = "153rd Mechanized Battalion" historical_model = 1 }
					regiment = { type = division_hq_standard name = "153rd Mechanized Battalion" historical_model = 1 }
		}
		division = {
			is_reserve = yes
			name = "Kainuun Prikaati"
			location = 283
			regiment = { type = infantry_brigade name = "Kainuun Jääkäripataljoona" historical_model = 2 }
			regiment = { type = anti_air_brigade name = "Lapin Ilmatorjuntarykmentti" historical_model = 2 }
		}
	}
	corps = {
		name = "Länsi-Suomen Sotilaslääni"
		location = 654
		regiment = { type = hq_brigade name = "Länsi-Suomen Sotilasläänin Esikunta" historical_model = 2 }
		division = {
			is_reserve = yes
			name = "Panssariprikaati"
			location = 654
			regiment = { type = mechanized_brigade name = "Hämeen Jääkäripataljoona" historical_model = 2 }
			regiment = { type = armor_brigade name = "Panssaripataljoona" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Porin Prikaati"
			location = 653
			regiment = { type = mechanized_brigade name = "Satakunnan Jääkäripataljoona" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Tykistöprikaati"
			location = 653
			regiment = { type = infantry_brigade name = "Jääkäripataljoona" historical_model = 2 }
			regiment = { type = artillery_brigade name = "Satakunnan Tykistörykmentti" historical_model = 2 }
		}
	}
	corps = {
		name = "Itä-Suomen Sotilaslääni"
		location = 618
		regiment = { type = hq_brigade name = "Itä-Suomen Sotilasläänin Esikunta" historical_model = 2 }
		division = {
			is_reserve = yes
			name = "Karjalan Prikaati"
			location = 618
			regiment = { type = mechanized_brigade name = "Kymen Jääkäripataljoona" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Pohjois-Karjalan Prikaati"
			location = 402
			regiment = { type = infantry_brigade name = "Karjalan Jääkäripataljoona" historical_model = 2 }
		}
		division = {
			is_reserve = yes
			name = "Utin Jääkärirykmentti"
			location = 618
			regiment = { type = special_forces_brigade name = "Erikoisjääkäripataljoona" historical_model = 2 }
			regiment = { type = air_cavalry_brigade name = "Helikopteripataljoona" historical_model = 2 }
		}
	}
}
division = {
	is_reserve = yes
	name = "Uudenmaan Prikaati"
	location = 779
	regiment = { type = marine_brigade name = "Vaasan Rannikkojääkäripataljoona" historical_model = 2 }
}

navy = {
	name = "5. Miinalaivue"
	base = 739
	location = 739
	ship = { type = light_cruiser name = "Pohjanmaa" historical_model = 0 }
}

navy = {
	name = "4. Miinalaivue"
	base = 736
	location = 736
	ship = { type = light_cruiser name = "Hämeenmaa" historical_model = 0 }
	ship = { type = light_cruiser name = "Uusimaa" historical_model = 0 }
}

navy = {
	name = "7. Ohjuslaivue"
	base = 739
	location = 739
	ship = { type = patrol_boat name = "Hamina" historical_model = 0 }
}

navy = {
	name = "6. Ohjuslaivue"
	base = 736
	location = 736
	ship = { type = patrol_boat name = "Rauma" historical_model = 0 }
	ship = { type = patrol_boat name = "Raahe" historical_model = 0 }
	ship = { type = patrol_boat name = "Porvoo" historical_model = 0 }
	ship = { type = patrol_boat name = "Naantali" historical_model = 0 }
}

navy = {
	name = "Kuljetuslaivue"
	base = 739
	location = 739
	ship = { type = invasion_transport_ship name = "1. Kuljetusviirikkö" historical_model = 0 }
	ship = { type = invasion_transport_ship name = "2. Kuljetusviirikkö" historical_model = 0 }
}

air = {
	name = "Satakunnan Lennosto"
	base = 616
	location = 616
	wing = { type = interceptor name = "Hävittäjälentolaivue 21" historical_model = 0 }
}

air = {
	name = "Karjalan Lennosto"
	base = 400
	location = 400
	wing = { type = interceptor name = "Hävittäjälentolaivue 31" historical_model = 0 }
}
}	