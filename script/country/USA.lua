
local P = {}
AI_USA = P

-- #######################################

-- Tech weights
--   1.0 = 100% the total needs to equal 1.0
function P.TechWeights(voTechnologyData)
	local laTechWeights = {
		0.1,	-- _RESEARCH_LAND_
		0.1,	-- _RESEARCH_LAND_DOC_
		0.125,	-- _RESEARCH_AIR_
		0.125,	-- _RESEARCH_AIR_DOC_
		0.125,	-- _RESEARCH_NAVAL_
		0.125,	-- _RESEARCH_NAVAL_DOC_
		0.1,	-- _RESEARCH_INDUSTRIAL_
		0.150,	-- _RESEARCH_SECRET_
		0.05	-- _RESEARCH_UNKNOWN_
		};
	
	return laTechWeights
end
-- Techs that are used in the main file to be ignored
--   techname|level (level must be 1-9 a 0 means ignore all levels
--   use as the first tech name the word "all" and it will cause the AI to ignore all the techs
function P.LandTechs(voTechnologyData)
	local lbArmor = voTechnologyData.TechStatus:IsUnitAvailable(CSubUnitDataBase.GetSubUnit("armor_brigade"))
	local ignoreTech
	

		ignoreTech = {
			{"military_police_training", 0}, 
			{"security_training", 0}, 
			{"combat_training", 0},
		};
	
		
	local preferTech = {
		-- Armor & APCs
		"tank_gun",
		"tank_armor",
		"tank_engine",
		"tank_reliability",
		"apc_armament",
		"apc_armour",
		"apc_engines",
		"apc_reliability",
		"gunship_armament",
		"gunship_fuselage",
		"gunship_engines",
		"gunship_reliability",
		"artillery_gun",
		"artillery_carriage",
		"anti_tank_ordnance",
		"anti_tank_carriage",
		"anti_air_gun",
		"anti_air_carriage",
		"sam_batteries",
		"sp_artillery_gun",
		"sp_artillery_chassis",
		"sp_anti_air_armament",
		"sp_anti_air_chassis",
		-- Infantry
		"infantry_small_arms",
		"infantry_heavy_weapons",
		"infantry_anti_tank",
		"infantry_support",
		"demolition_equipment",
		"bridging_equipment",
		"special_small_arms",
		"special_heavy_weapons",
		"special_anti_tank",
		"special_support_weapons"
	};
	
	return ignoreTech, preferTech
end

function P.LandDoctrinesTechs(voTechnologyData)
	local ignoreTech = {
		{"unit_cohesion", 0},
		{"logistics_tech", 0},
		{"staff_combat_sections", 0},
		{"defence_in_depth", 0},
		{"guerilla_tactics", 0},
		{"reserves_doctrine", 0}};
		
	local preferTech = {
		"modern_blitzkrieg",
		"breakthrough_exploitation",
		"elastic_defence",
		"flanking_maneuvers",
		"shock_and_awe",
		"armor_tactics",
		"asymmetrical_warfare",
		"flexible_defence",
		"special_forces_doctrine",
		"superior_firepower",
		"static_defence",
		"close_air_support_doctrine",
		"human_wave",
		"firesupport_coordination",
		"solid_air_defence",
		"organisation_tech",
		"unit_cooperation_tech"
	};
		
	return ignoreTech, preferTech
end

function P.AirTechs(voTechnologyData)
	local ignoreTech = {
		{"missile_warhead", 0},
		{"missile_propulsion", 0}
	};
	local preferTech = {
		"air_to_air_weaponry",
		"asf_fuselage",
		"asf_engine",
		"asf_fuel_tank",
		"air_to_surface_weapons",
		"cas_fuselage",
		"cas_engine",
		"cas_fuel_tanks",
		"cag_weapons",
		"cag_fuselage",
		"cag_engine",
		"cag_fuel_tanks",
		"multi_role_engine",
		"multi_role_fuselage",
		"multi_role_fuel_tank",
		"fighter_countermeasures",
		"fighter_radar",
		"bomber_bombs",
		"bomber_engine",
		"bomber_fuselage",
		"bomber_fuel_tank",
		"air_to_sea_weapons",
		"naval_bomber_engine",
		"naval_bomber_fuselage",
		"naval_bomber_fuel_tanks",
		"bomber_countermeasures",
		"bomber_radar"	
	};
		
	return ignoreTech, preferTech
end

function P.AirDoctrineTechs(voTechnologyData)
	local ignoreTech = {};

	local preferTech = {
		"air_superiority_doctrine",
		"fighter_pilot_training",
		"interception_tactics",
		"fighter_ground_control",
		"ground_attack_tactics",
		"interdiction_tactics",
		"tactical_air_command",
		"logistical_strike_tactics",
		"installation_strike_tactics",
		"airbase_strike_tactics",
		"portstrike_tactics",
		"navalstrike_tactics"
	};	
		
	return ignoreTech, preferTech
end
		
function P.NavalTechs(voTechnologyData)
	local ignoreTech = {};

	local preferTech = {
		"submarine_engine",
		"submarine_hull",
		"submarine_sonar",
		"submarine_torpedo",
		"submarine_aa",
		"landing_tactics",
		"landing_equipment",
		"destroyer_armament",
		"destroyer_engine",
		"destroyer_armour",
		"anti_air_cruiser_armament",
		"anti_air_cruiser_aa",
		"anti_air_cruiser_engine",
		"anti_air_cruiser_armour",
		"supercarrier_engine",
		"supercarrier_armour",
		"supercarrier_hangar",
		"large_radar"
	};	
		
	return ignoreTech, preferTech
end
		
function P.NavalDoctrineTechs(voTechnologyData)
	local ignoreTech = {};

	local preferTech = {
		"carrier_aircraft_crew_training",
		"carrier_battlegroups",
		"carrier_crew_training",
		"cruiser_tactics",
		"destroyer_tactics",
		"basing_doctrine",
		"fire_control_systems",
		"anti_submarine_tactics",
		"submarine_crew_training",
		"convoy_raiding_tactics",
		"submarine_combat_tactics"
	};	
		
	return ignoreTech, preferTech
end

function P.IndustrialTechs(voTechnologyData)
	local ignoreTech = {};

	local preferTech = {
		-- High tech advance:
		"computers",
		"radios",
		"radar",
		"education",
		-- Industrial advance:
		"energy_production",
		"energy_efficiency",
		"industral_production",
		"industral_efficiency",
		"first_aid"
	};
		
	return ignoreTech, preferTech
end
		
function P.SecretWeaponTechs(voTechnologyData)
	local ignoreTech = {}
	local preferTech = {
		"stealth_fighters",
		"stealth_bombers",
		"military_drone"
	};
	return ignoreTech, preferTech
end

function P.OtherTechs(voTechnologyData)
	local ignoreTech = {};

	local preferTech = {};			

	return ignoreTech, preferTech
end

-- END OF TECH RESEARCH OVERIDES
-- #######################################

-- #######################################
-- Production Overides the main LUA with country specific ones

-- Production Weights
--   1.0 = 100% the total needs to equal 1.0
function P.ProductionWeights(voProductionData)

	-- Set the default in the array incase no condition is met
	local laArray = {
		0.30,	-- Land
		0.39,	-- Air
		0.30,	-- Sea
		0.01
	};	-- Other         
		
	if not(voProductionData.IsAtWar) then
		laArray = {
			0.10, -- Land 
			0.39, -- Air
			0.50, -- Sea
			0.01
		}; -- Other
	else			
		laArray = {
			0.40, -- Land 
			0.35, -- Air
			0.25, -- Sea
			0
		}; -- Other
	end
	
	return laArray
end

-- Land ratio distribution
function P.LandRatio(voProductionData)
	local laArray
	
	--iif voProductionData.Year < 1941 or not(voProductionData.IsAtWar) then
		laArray = {
			garrison_brigade = 1,
			mechanized_brigade = 6,
			armor_brigade = 4,
			--marine_brigade = 3,
			motorized_brigade = 3,
			light_infantry_brigade = 3,
			infantry_brigade = 3};
	--else
		--laArray = {
		--	garrison_brigade = 1,
		--	mechanized_brigade = 6,
		--	armor_brigade = 4,
		--	infantry_brigade = 4};
	--end
	
	return laArray
end

-- Special Forces ratio distribution
function P.SpecialForcesRatio(voProductionData)
	
	local laRatio = {
		10, -- Land
		1	-- Special Force Unit
	};
	local laUnits = {
		marine_brigade = 10,
		paratrooper_brigade = 2,
		mountain_brigade = 5,
		special_forces_brigade = 1
	};
	
	return laRatio, laUnits	
end



-- Which units should get 1 more Support unit with Superior Firepower tech
function P.FirePower(voProductionData)
	local laArray = {
		"mechanized_brigade",
		"armor_brigade",
		"infantry_brigade",
		"marine_brigade",
		"mechanized_brigade",
		"motorized_brigade",
		"light_infantry_brigade"
		};
	return laArray
end

-- Air ratio distribution
function P.AirRatio(voProductionData)
	local laArray = {
		interceptor = 5,
		cag = 3,
		multi_role = 9,
		cas = 2,
		naval_bomber = 1,
		strategic_bomber = 3
	};
	return laArray
end
-- Naval ratio distribution
function P.NavalRatio(voProductionData)
	local laArray = {
		destroyer = 10,
		submarine = 3,
		--anti_air_cruiser = 7,
		--super_carrier = 4,
		light_cruiser = 12
	};
	return laArray
end

-- Transport to Land unit distribution
function P.TransportLandRatio(voProductionData)
	local laArray = {
		14, -- Land
		4,  -- transport
		4}  -- invasion craft
  
	return laArray
end

-- Convoy Ratio control
--- NOTE: If goverment is in Exile these parms are ignored
function P.ConvoyRatio(voProductionData)
	local laArray = {
		10, -- Percentage extra (adds to 100 percent so if you put 10 it will make it 110% of needed amount)
		150, -- If Percentage extra is less than this it will force it up to the amount entered
		250, -- If Percentage extra is greater than this it will force it down to this
		5} -- Escort to Convoy Ratio (Number indicates how many convoys needed to build 1 escort)
  
	return laArray
end

-- Garrison builds - GAR+(ART|HVYART)+SUPPORTSx2 ("Garrison" Support Group)

function P.Build_garrison_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)
	
	
		voType.first = "artillery_brigade"
		
		voType.Support = 0
	

	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end

function P.Build_motorized_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)
		voType.TransportMain = "truck_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "anti_tank_brigade"
		voType.second = "artillery_brigade"
		voType.third = "armored_car_brigade"
		voType.SecondaryMain = "engineer_brigade"
		voType.Support = 0

	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end

function P.Build_mechanized_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)	
		voType.TransportMain = "hftrack_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "heavy_anti_tank_brigade"
		voType.second = "sp_artillery_brigade"
		voType.third = "engineer_brigade"
		voType.SecondaryMain = "sp_anti_air_brigade"
		voType.Support = 0

	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end

function P.Build_armor_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)

		voType.TransportMain = "hftrack_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "mechanized_infantry_bat"
		voType.second = "engineer_brigade"
		voType.third = "tank_destroyer_brigade"
		voType.SecondaryMain = "sp_anti_air_brigade"
		voType.Support = 0

	
	
	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end

function P.Build_heavy_armor_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)

		voType.SecondaryMain = "semi_motorized_brigade"
		voType.TransportMain = "hftrack_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "sp_artillery_brigade"
		voType.second = "sp_anti_air_brigade"
		voType.third = "engineer_brigade"
		voType.Support = 0

	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end

function P.Build_light_armor_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)

		
		voType.SecondaryMain = "engineer_brigade"
		voType.TransportMain = "light_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "motorized_infantry_bat"
		voType.second = "motorized_artillery_brigade"
		voType.forth = "engineer_brigade"


		voType.Support = 0

	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end


function P.Build_infantry_brigade(vIC, viManpowerTotal, voType, voProductionData, viUnitQuantity)
		
	--if (voProductionData.Year <= 1940) then
		
		voType.TransportMain = "truck_transport"
		voType.TertiaryMain = "division_hq_standard"
		voType.first = "anti_tank_brigade"
		voType.second = "artillery_brigade"
		voType.third = "Recon_cavalry_brigade"
		voType.SecondaryMain = "engineer_brigade"
		voType.Support = 0
		
		--else
		
		
		--voType.TransportMain = "truck_transport"
		--voType.TertiaryMain = "division_hq_standard"
		--voType.first = "medium_tank_destroyer_brigade"
		--voType.second = "motorized_artillery_brigade"
		--voType.third = "Recon_cavalry_brigade"
		--voType.SecondaryMain = "engineer_brigade"
		--voType.Support = 0
	--end
		
	return Support.CreateUnit(voType, vIC, viUnitQuantity, voProductionData, laSupportUnit)
end




function P.Build_CoastalFort(ic, voProductionData)


	--if voProductionData.Year < 1943 then
	--	return ic, false
	--end
	
	return ic, false
end

function P.Build_AirBase(ic, voProductionData)
	--ic = Support.Build_AirBase(ic, voProductionData, 10669, 10) --Midway
	--ic = Support.Build_AirBase(ic, voProductionData, 5825, 10) --Honolulu
	--ic = Support.Build_AirBase(ic, voProductionData, 5712, 10) -- AmamiOshima 
	--ic = Support.Build_AirBase(ic, voProductionData, 5720, 10) -- TokunoShima
	--ic = Support.Build_AirBase(ic, voProductionData, 5748, 10) -- Nago
	--ic = Support.Build_AirBase(ic, voProductionData, 5759, 10) -- Naha
	--ic = Support.Build_AirBase(ic, voProductionData, 10642, 10) -- Iwo Jima
	--ic = Support.Build_AirBase(ic, voProductionData, 14129, 10) -- Bonin Islands
	--ic = Support.Build_AirBase(ic, voProductionData, 10664, 10) --Wake island

	--if voProductionData.Year < 1942 then
	--	return ic, false
	--end
	
	return ic, false
end
function P.Build_Industry(vIC, voProductionData)
	return vIC, false
end

function P.Build_NavalBase(ic, voProductionData)
	--ic = Support.Build_NavalBase(ic, voProductionData, 10669, 10) --Midway
	--ic = Support.Build_NavalBase(ic, voProductionData, 5825, 10) --Honolulu
	--ic = Support.Build_NavalBase(ic, voProductionData, 5712, 6) -- AmamiOshima
	--ic = Support.Build_NavalBase(ic, voProductionData, 5720, 6) -- TokunoShima
	--ic = Support.Build_NavalBase(ic, voProductionData, 5748, 6) -- Nago
	--ic = Support.Build_NavalBase(ic, voProductionData, 5759, 6) -- Naha
	--ic = Support.Build_NavalBase(ic, voProductionData, 10642, 6) -- Iwo Jima
	--ic = Support.Build_NavalBase(ic, voProductionData, 14129, 6) -- Bonin Islands
	--ic = Support.Build_NavalBase(ic, voProductionData, 10664, 10) --Wake island

	-- Ports in Spain in case Germany takes them over
	--ic = Support.Build_NavalBase(ic, voProductionData, 3884, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3814, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3676, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3877, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3679, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3610, 10) 
	--ic = Support.Build_NavalBase(ic, voProductionData, 3675, 10) 
		
	--if voProductionData.Year < 1942 then
	--	return ic, false
	--end
	
	return ic, false
end

function P.Build_Radar(ic, voProductionData)
	-- ic = Support.Build_Radar(ic, voProductionData, 10669, 10) --Midway
	-- ic = Support.Build_Radar(ic, voProductionData, 5825, 10) --Honolulu
	-- ic = Support.Build_Radar(ic, voProductionData, 5712, 10) -- AmamiOshima
	-- ic = Support.Build_Radar(ic, voProductionData, 5720, 10) -- TokunoShima
	-- ic = Support.Build_Radar(ic, voProductionData, 5748, 10) -- Nago
	-- ic = Support.Build_Radar(ic, voProductionData, 5759, 10) -- Naha
	-- ic = Support.Build_Radar(ic, voProductionData, 10642, 10) -- Iwo Jima
	-- ic = Support.Build_Radar(ic, voProductionData, 14129, 10) -- Bonin Islands
	-- ic = Support.Build_Radar(ic, voProductionData, 10664, 10) --Wake island

	-- if voProductionData.Year < 1942 then
		-- return ic, false
	-- end
	
	return ic, false
end

function P.Build_AntiAir(ic, voProductionData)
	return ic, false
end

function P.Build_Infrastructure(ic, voProductionData)
	-- if voProductionData.Year <= 1942 then
		-- return ic, false
	-- end

	return ic, false
end

function P.Build_Fort(ic, voProductionData)
 	-- if voProductionData.Year < 1944 then
		-- return ic, false
	-- end
	
	return ic, false
end
-- END OF PRODUTION OVERIDES
-- #######################################

function P.ForeignMinister_Alignment(...)
	return Support.AlignmentPush("allies", ...)
end

-- function P.DiploScore_Embargo(voDiploScoreObj)
	-- if voDiploScoreObj.EmbargoHasFaction then
		-- local loAllyFaction = CCurrentGameState.GetFaction("allies")

		--If USA is leaning toward the allies and UK then embargo their enemies
		-- if Support.IsFriend(voDiploScoreObj.ministerAI, loAllyFaction, voDiploScoreObj.ministerCountry) then
			-- local allyTag = loAllyFaction:GetFactionLeader()
			-- local loAllyCountry = allyTag:GetCountry()
			
			-- if loAllyCountry:GetRelation(voDiploScoreObj.EmbargoTag):HasWar() then
				-- voDiploScoreObj.Score = voDiploScoreObj.Score + 100
			-- end
			
			--Push Japan to the top of the que if they are in the Axis
			-- if tostring(voDiploScoreObj.EmbargoTag) == "JAP" then
				-- local loAxisFaction = CCurrentGameState.GetFaction("axis")
				-- local chiTag = CCountryDataBase.GetTag("CHI")
				
				-- if voDiploScoreObj.EmbargoCountry:GetFaction() == loAxisFaction
				-- or voDiploScoreObj.EmbargoCountry:GetRelation(chiTag):HasWar() then
					-- voDiploScoreObj.Score = voDiploScoreObj.Score + 100
				-- end
			-- end
		-- end
	-- end
	
	-- return voDiploScoreObj.Score
-- end

-- function P.DiploScore_Debt(voDiploScoreObj)
	-- local loAllyFaction = CCurrentGameState.GetFaction("allies")
	
	--If the requesting country is part of the Allies then
	-- if voDiploScoreObj.ToCountry:GetFaction() == loAllyFaction then
		--Make sure the USA is not part of a faction already
		-- if not(voDiploScoreObj.FromCountry:HasFaction()) then
			-- if Support.IsFriend(voDiploScoreObj.ministerAI, loAllyFaction, voDiploScoreObj.FromCountry) then
				--Check to see if they are at war
				-- if voDiploScoreObj.ToCountry:IsAtWar() then
					--Calculate the score based on USA neutrality the lower it is the more likely they will allow the debt
					-- local liNeutrality = voDiploScoreObj.FromCountry:GetEffectiveNeutrality():Get()
					-- voDiploScoreObj.Score = 110 - liNeutrality
				-- end
			-- end
		-- end
	-- else
		-- local lsToTag = tostring(voDiploScoreObj.ToTag)
		
		--If it is China do a special check
		-- if lsToTag == "CHI" then
			--If we are friendly to the Allied faction
			-- if Support.IsFriend(voDiploScoreObj.ministerAI, loAllyFaction, voDiploScoreObj.FromCountry) then
				-- local japTag = CCountryDataBase.GetTag("JAP")
				
				--If China and Japan are at war then let China be allowed debt even if not in the Allies
				-- if voDiploScoreObj.ToCountry:GetRelation(japTag):HasWar() then
					-- voDiploScoreObj.Score = 100
				-- end
			-- end
		-- end
	-- end
	
	-- return voDiploScoreObj.Score
-- end
function P.DiploScore_InfluenceNation(voDiploScoreObj)
	local loInfluences = {
		JAP = {Score = 450},
		KOR = {Score = 500},
		ISR = {Score = 500},
		AST = {Score = 250},
		NZL = {Score = 250},
		CHI = {Score = 400},
		JOR = {Score = 100},
		MIC = {Score = 50},
		SOL = {Score = 50},
		PHI = {Score = 150},
		IND = {Score = 125}
	}
	-- Are they on our list
	if loInfluences[voDiploScoreObj.TargetName] then
		return (voDiploScoreObj.Score + loInfluences[voDiploScoreObj.TargetName].Score)
	end
	return voDiploScoreObj.Score
end

function P.DiploScore_OfferTrade(voDiploScoreObj)
	local laTrade = {
		-- Loves:
		ENG = {Score = 200},
		GER = {Score = 200},
		IND = {Score = 100},
		ITA = {Score = 75},
		SPA = {Score = 75},
		POR = {Score = 75},
		JAP = {Score = 200},
		KOR = {Score = 200},
		ISR = {Score = 200},
		CHI = {Score = 200},
		CAN = {Score = 200},
		FRA = {Score = 190},
		AST = {Score = 100},
		NZL = {Score = 100},
		SIN = {Score = 100},
		PHI = {Score = 200},
		MEX = {Score = 100},
		IRQ = {Score = 150},
		TUR = {Score = 150},
		ISR = {Score = 200},
		SAU = {Score = 200},
		BAH = {Score = 100},
		AFG = {Score = 100},
		SIA = {Score = 100},
		CHC = {Score = 200},
		-- Hates:
		--BRA = {Score = -50},
		RUS = {Score = -200},
		--CHC = {Score = -50},
		--PER = {Score = -200},
		SYR = {Score = -200},
		EGY = {Score = -100},
		PAK = {Score = -50},
		PRK = {Score = -200},
		ISS = {Score = -200}
	}
	if laTrade[voDiploScoreObj.TagName] then
		return voDiploScoreObj.Score + laTrade[voDiploScoreObj.TagName].Score
	end
	return voDiploScoreObj.Score
end

function P.DiploScore_Alliance(voDiploScoreObj)
	local lsTargetTag = voDiploScoreObj.TargetTag
	if lsTargetTag == 'ISR' or
	lsTargetTag == 'KOR' or
	lsTargetTag == 'SIN' or
	lsTargetTag == 'JAP' or
	lsTargetTag == 'CHI' or
	lsTargetTag == 'PHI' or
	lsTargetTag == 'AST' or
	lsTargetTag == 'NZL' or
	lsTargetTag == 'ENG' or
	lsTargetTag == 'FRA' or
	lsTargetTag == 'GER' or
	lsTargetTag == 'CAN' or
	lsTargetTag == 'ITA' or
	lsTargetTag == 'CYP' or
	lsTargetTag == 'ISR' or
	lsTargetTag == 'SAU' or
	lsTargetTag == 'PHI' or
	lsTargetTag == 'IRQ' or
	lsTargetTag == 'SIA' or
	lsTargetTag == 'MLT' then
		voDiploScoreObj.Score = 200
	elseif lsTargetTag == 'PER' or
	--lsTargetTag == 'SYR' or
	lsTargetTag == 'EGY' or
	lsTargetTag == 'RUS' or
	lsTargetTag == 'MEX' or
	lsTargetTag == 'SAU' or
	lsTargetTag == 'CHC' or
	lsTargetTag == 'PKR' then
		return 0
	end
	return voDiploScoreObj.Score
end

function P.ForeignMinister_CallAlly(voForeignMinisterData)

	local loISSTag = CCountryDataBase.GetTag("ISS")
	local loUSISSRelation = voForeignMinisterData.ministerCountry:GetRelation(loISSTag)
	--Utils.LUA_DEBUGOUT("USA looking to call ally")
	-- Get a list of all your allies
		local laAllies = {}
		for loAllyTag in voForeignMinisterData.ministerCountry:GetAllies() do
			local loAllyCountry = loAllyTag:GetCountry()

		-- Exclude Puppets from this list
			if not(loAllyCountry:IsPuppet()) then
				local loAlly = {
					AllyTag = loAllyTag,
					AllyCountry = loAllyCountry,
					Continent = tostring(loAllyCountry:GetActingCapitalLocation():GetContinent():GetTag()),
				}
				--Utils.LUA_DEBUGOUT("USA getting list of allies")
				laAllies[tostring(loAllyTag)] = loAlly
			end
		end
	
	
	if (loUSISSRelation:HasWar()) then
		-- Go through our Wars
		for loDiploStatus in voForeignMinisterData.ministerCountry:GetDiplomacy() do
			local loTargetTag = loDiploStatus:GetTarget()
			
			--Utils.LUA_DEBUGOUT("USA go threw wars")
			if loTargetTag:IsValid() and loDiploStatus:HasWar() then
				local loWar = loDiploStatus:GetWar()
			
				if loWar:IsLimited() then
					local lsTargetTag = tostring(loTargetTag)
					local liWarMonths = loWar:GetCurrentRunningTimeInMonths()
					local loTargetCountry = loTargetTag:GetCountry()
				
				-- Call in all potential allies
					for k, v in pairs(laAllies) do
						--Utils.LUA_DEBUGOUT("USA valling allies")
						if not(v.AllyCountry:GetRelation(loTargetTag):HasWar()) then
							if (loUSISSRelation:HasWar()) then
								--if k == "USA" then
								--Utils.LUA_DEBUGOUT("USA calling USA")
								--	Support.ExecuteCallAlly(voForeignMinisterData.ministerAI, voForeignMinisterData.ministerTag, v, loTargetTag)
								--end
							end
						end
					end
					
				end
			end
		end
	end
	return false
end



--deleted function P.DiploScore_InviteToFaction(voDiploScoreObj) from Black ICE
-- deleted DiploScore_Guarantee from Black ICE


--##########################
-- Foreign Minister Hooks
function P.ForeignMinister_Influence(voForeignMinisterData)
	local laIgnoreWatch -- Ignore this country but monitor them if they are about to join someone else
	local laWatch -- Monitor them and also fi their score is high enough they can be influenced normally
	local laIgnore -- Ignore them completely
	laWatch = {
		"ISR",
		"KOR",
		"JAP",
		"NZL",
		"AST",
		"SIN",
		"CHI",
		"PHI",
		"FRA",
		"ITA",
		"SPA",
		"GER",
		"ENG",
		"NOR"
	};
	laIgnoreWatch = {
		"MLT",
		"IND",
		"MEX",
		"IRE",
		"CYP",
		"SWE",
		"FIN",
		"POR",
		"SCH",
		"AUS",
		"BOS",
		"SER",
		"SLV",
		"SLO",
		"CZE"
	};
	laIgnore = {
		"PER",
		"IRQ",
		"SYR",
		"EGY",
		"CHC",
		"RUS",
		"PKR",
		"SAU"
	};
	return laWatch, laIgnoreWatch, laIgnore
end
function P.DiploScore_RequestLendLease( liScore, voAI, voActorTag )
	if tostring(voActorTag) == "JAP" or
	tostring(voActorTag) == "KOR" or
	tostring(voActorTag) == "ISR" or
	tostring(voActorTag) == "CHI" or
	tostring(voActorTag) == "ENG" or
	tostring(voActorTag) == "GER" or
	tostring(voActorTag) == "FRA" or
	tostring(voActorTag) == "CYP" or
	tostring(voActorTag) == "SIN" or
	tostring(voActorTag) == "IND" or
	tostring(voActorTag) == "AST" or
	tostring(voActorTag) == "SPR" or
	tostring(voActorTag) == "GRE" or
	tostring(voActorTag) == "CAN" or
	tostring(voActorTag) == "ITA" or
	tostring(voActorTag) == "NZL" then
		return 200
	end
	return liScore
end


function P.ForeignMinister_ProposeWar(voForeignMinisterData)
  --Utils.LUA_DEBUGOUT("looking for war")
  local laWarTags = {}
  local laPeaceTags = {}
  local liTotalNeighborWars = 0
  
  
  --if not(voForeignMinisterData.Strategy:IsPreparingWar()) then
  --Utils.LUA_DEBUGOUT("lets war isis")
  --P.ISSWarCheck(voForeignMinisterData)
  --end	
  
end


function P.ISSWarCheck(voForeignMinisterData)
	
	--Utils.LUA_DEBUGOUT("in isis war check")

	if (voForeignMinisterData.Month >= 1 and voForeignMinisterData.Month <= 12) or vbOveride then
	
		local loISSTag = CCountryDataBase.GetTag("ISS")
		local loISSCountry = loISSTag:GetCountry()
		local lbDOW = Support.GoodToWarCheck(loISSTag, loISSCountry, voForeignMinisterData, true)
		
		--Utils.LUA_DEBUGOUT("date 1-12")
		
		if lbDOW then	
			--Utils.LUA_DEBUGOUT("usa prep war isis")
			voForeignMinisterData.Strategy:PrepareLimitedWar(loISSTag, 100)
		end
		
		
		return lbDOW

	end
	--Utils.LUA_DEBUGOUT("ISS war check false")
	return false
end
-- Return:
return AI_USA

