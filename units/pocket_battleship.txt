pocket_battleship = {
	type = naval
	sprite = HeavyCruiser
	active = no
	capital = yes
	can_be_pride = yes
	hull = 1.75  ## was 1.4
	available_trigger = {
		not = { has_country_flag =  deutschland_class_ca }
	}
	
	#Size Definitions
	max_strength = 100
	default_organisation = 30
	default_morale = 0.30
	officers = 300  ## was 20

	#Building Costs
	build_cost_ic = 7 ## was 8.00
	build_cost_manpower = 1.80  # was 1.20
	build_time = 500 ## was 310
	repair_cost_multiplier = 0.15

	#Misc Abilities
	maximum_speed = 28  ## was 28.00
	transport_capability = 0.00  ## was 0.00
	supply_consumption = 1.00
	fuel_consumption = 1.75
	range = 3200.00  ## was 4300.00
	distance = 0.18  ## was 0.34
	radio_strength = 1
	positioning = 0.20

	#Detection Abilities
	surface_detection = 0.5
	air_detection = 1
	sub_detection = 0.50
	visibility = 70.00

	#Defensive Abilities
	sea_defence = 4.5
	air_defence = 16 ## was 2.00

	#Offensive Abilities
	convoy_attack = 8
	sea_attack = 9.5
	sub_attack = 0.25
	air_attack = 0.0
	shore_bombardment = 2

	completion_size = 3.3
	on_completion = cruiser_practical
	priority = 31
}
